.use_context: &use_context
  - kubectl config use-context "$KUBE_CONTEXT"

.deploy_dynamic_environment:
  rules:
    - if: $KUBECONFIG
  image: alpine
  variables:
    DOMAIN: $CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  before_script:
    # This creates a new file with values provided by the pipeline dynamically.
    # These values are required to deploy new environments during the pipeline execution (e.g. UAT).
    - |
      cat >"$CI_PROJECT_DIR/.gitlab/assets/common/dynamic.overwrite.values.yaml" <<EOF
      deploymentAnnotations: &deploymentAnnotations
        annotations:
          app.gitlab.com/app: "$CI_PROJECT_PATH_SLUG"
          app.gitlab.com/env: "$CI_ENVIRONMENT_SLUG"
        podAnnotations:
          app.gitlab.com/app: "$CI_PROJECT_PATH_SLUG"
          app.gitlab.com/env: "$CI_ENVIRONMENT_SLUG"
      bbbatscale:
        <<: *deploymentAnnotations
        domains:
          - "$DOMAIN"
        image: "$BBBATSCALE_IMAGE"
        imageTag: "$CI_COMMIT_SHA"
      nginx:
        <<: *deploymentAnnotations
        image: "$NGINX_IMAGE"
        imageTag: "$CI_COMMIT_SHA"
      ingress:
        annotations:
          kubernetes.io/tls-acme: "true"
        tls:
          - secretName: bbbatscale-tls-certificate
            hosts:
              - "$DOMAIN"
      EOF
    # TODO remove --repository as soon as kubectl and helm are available in the main repository
    - apk add --repository https://dl-cdn.alpinelinux.org/alpine/edge/testing/ kubectl helm
    - *use_context
    - kubectl delete --ignore-not-found=true namespace "$CI_ENVIRONMENT_SLUG"
  script:
    # This deploys an environment with fake data dynamically.
    - |
      cat >"$CI_PROJECT_DIR/charts/bbbatscale/templates/hooks/bbbatscale-fakedata.yaml" <<EOF
      apiVersion: batch/v1
      kind: Job
      metadata:
        name: bbbatscale-fake-data-job
        labels:
          app.kubernetes.io/name: bbbatscale-fake-data
          {{- include "bbbatscale.commonLabels" . | nindent 4 }}
        annotations:
          helm.sh/hook: post-install,post-upgrade
          helm.sh/hook-weight: "30"
      spec:
        template:
          metadata:
            labels:
              app.kubernetes.io/name: bbbatscale-fake-data
              {{- include "bbbatscale.commonLabels" . | nindent 8 }}
          spec:
            restartPolicy: Never
            volumes:
              {{- include "bbbatscale.configVolumes" . | nindent 8 }}
            {{- with .Values.bbbatscale.imagePullSecrets }}
            imagePullSecrets:
              {{- range . }}
              - name: {{ . }}
              {{- end }}
            {{- end }}
            containers:
              - name: bbbatscale-fake-data-container
                image: "{{ .Values.bbbatscale.image }}:{{ .Values.bbbatscale.imageTag | default .Chart.AppVersion }}"
                imagePullPolicy: {{ .Values.bbbatscale.imagePullPolicy }}
                command:
                  - sh
                  - -c
                  - pipenv install --system --deploy --dev
                    && bbbatscale fake-data --create --homerooms --personalrooms --admin "$DOMAIN"
                {{- /* TODO: Use ConfigMap envFrom as soon as this feature is live: https://github.com/kubernetes/kubernetes/issues/22368 */}}
                env:
                  {{- include "bbbatscale.env" . | nindent 12 }}
                volumeMounts:
                  {{- include "bbbatscale.configVolumeMounts" . | nindent 12 }}
      EOF
    - helm install --dependency-update --wait
      --create-namespace -n "$CI_ENVIRONMENT_SLUG"
      -f .gitlab/assets/common/overwrite.values.yaml
      -f .gitlab/assets/common/dynamic.overwrite.values.yaml
      bbbatscale charts/bbbatscale/
  needs:
    - job: lint_commit_messages
    - job: build_bbbatscale
    - job: build_nginx

.stop_dynamic_environment:
  rules:
    - if: $KUBECONFIG
  image: alpine
  before_script:
    # TODO remove --repository as soon as kubectl is available in the main repository
    - apk add --repository https://dl-cdn.alpinelinux.org/alpine/edge/testing/ kubectl
    - *use_context
  script:
    - kubectl delete --ignore-not-found=true namespace "$CI_ENVIRONMENT_SLUG"
  environment:
    action: stop
