"use strict";

const NO_RELEASE = false;
const PATCH = "patch";
const MINOR = "minor";
const MAJOR = "major";

const commonScopes = [
    "api",
    "commands",
    "import_export",
    "lba",
    "logging",
    "models",
    "nginx",
    "oidc",
    "recordings",
    "support_chat",
    "ui",
    "views",
    "webhooks"
];

const commitReleaseRules = {
    style: { section: null, release: NO_RELEASE, scopes: [null] },
    trans: {
        section: "Translations",
        release: PATCH,
        scopes: ["czech", "galician", "german", "italian", "russian", "spanish"]
    },
    build: { section: "Build System", release: PATCH, scopes: ["docker", "requirements"] },
    infra: { section: null, release: NO_RELEASE, scopes: ["assets", "ci", "generate"] },
    docs: { section: null, release: NO_RELEASE, scopes: [null, "comparison", ...commonScopes] },
    perf: { section: "Other", release: PATCH, scopes: [null, ...commonScopes] },
    test: { section: null, release: NO_RELEASE, scopes: [null, ...commonScopes] },
    refactor: { section: "Other", release: PATCH, scopes: [null, ...commonScopes] },
    chore: { section: "Other", release: PATCH, scopes: [null, ...commonScopes] },
    feat: { section: "Features", release: MINOR, scopes: [null, ...commonScopes] },
    fix: { section: "Bug Fixes", release: PATCH, scopes: [null, ...commonScopes] }
};

module.exports = {
    get typeScopeMap() {
        const typeScopeMap = { revert: Object.keys(commitReleaseRules) };
        for (const type in commitReleaseRules) {
            if (Object.prototype.hasOwnProperty.call(commitReleaseRules, type)) {
                typeScopeMap[type] = commitReleaseRules[type].scopes;
            }
        }
        return typeScopeMap;
    },
    get releaseRules() {
        const releaseRules = [{ breaking: true, release: MAJOR }];

        for (const type in commitReleaseRules) {
            if (Object.prototype.hasOwnProperty.call(commitReleaseRules, type)) {
                releaseRules.push(
                    {
                        type: type,
                        release: commitReleaseRules[type].release
                    },
                    {
                        type: "revert",
                        scope: type,
                        release: commitReleaseRules[type].release
                    }
                );
            }
        }
        return releaseRules;
    },
    get sectionMap() {
        const sectionMap = [{ type: "revert", section: "Reverts" }];
        for (const type in commitReleaseRules) {
            if (Object.prototype.hasOwnProperty.call(commitReleaseRules, type)) {
                const section = {
                    type: type
                };
                if (commitReleaseRules[type].section) {
                    section.section = commitReleaseRules[type].section;
                } else {
                    section.hidden = true;
                }
                sectionMap.push(section);
            }
        }
        return sectionMap;
    }
};
