# Description
_[A detailed description of the issue to tackle]_

# Goal
_[The goal to be achieved by resolving this spike]_

# Acceptance criteria
- [ ] _[**OPTIONAL**: Acceptance criteria one]_
- [ ] _[**OPTIONAL**: Acceptance criteria two]_
- [ ] _[**OPTIONAL**: Acceptance criteria three]_
