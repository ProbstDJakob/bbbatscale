import datetime
from typing import Iterable, Tuple, Union

import pytest
from core.constants import MEETING_STATE_RUNNING
from core.models import (
    GeneralParameter,
    HomeRoom,
    Meeting,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    User,
)
from django.contrib.sites.models import Site
from pytest import FixtureRequest


@pytest.mark.django_db
@pytest.fixture
def ordinary_room(
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="ordinary_room",
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.add(testserver_tenant)

    return room


@pytest.mark.django_db
@pytest.fixture
def homeroom__example_user(
    example_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = HomeRoom.objects.create(
        name="homeroom__example_user",
        owner=example_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def homeroom__example_moderator_user(
    example_moderator_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = HomeRoom.objects.create(
        name="homeroom__example_moderator_user",
        owner=example_moderator_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def homeroom__example_staff_user(
    example_staff_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = HomeRoom.objects.create(
        name="homeroom__example_staff_user",
        owner=example_staff_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def homeroom__example_superuser(
    example_superuser: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = HomeRoom.objects.create(
        name="homeroom__example_superuser",
        owner=example_superuser,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def personalroom__example_user(
    example_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = PersonalRoom.objects.create(
        name="personalroom__example_user",
        owner=example_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def personalroom__example_moderator_user(
    example_moderator_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = PersonalRoom.objects.create(
        name="personalroom__example_moderator_user",
        owner=example_moderator_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def personalroom__example_staff_user(
    example_staff_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = PersonalRoom.objects.create(
        name="personalroom__example_staff_user",
        owner=example_staff_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def personalroom__example_superuser(
    example_superuser: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = PersonalRoom.objects.create(
        name="personalroom__example_superuser",
        owner=example_superuser,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
def test_room_get_running_meeting_single(example_room: Room, example_meeting: Meeting) -> None:
    example_meeting.room = example_room
    running_meeting = example_room.get_running_meeting()
    assert running_meeting.pk == example_meeting.pk


@pytest.mark.django_db
def test_room_get_running_meeting_multiple(
    example_room: Room, example_meeting: Meeting, current_timestamp: datetime
) -> None:
    example_meeting.started = current_timestamp - datetime.timedelta(seconds=1)
    example_meeting.room = example_room
    example_meeting.save()

    example_meeting2 = Meeting.objects.create()
    example_meeting2.room = example_room
    example_meeting2.started = current_timestamp
    example_meeting2.state = MEETING_STATE_RUNNING
    example_meeting2.save()

    running_meeting = example_room.get_running_meeting()
    assert running_meeting.pk == example_meeting2.pk


@pytest.mark.django_db
def test_room_get_running_meeting_no_meeting(example_room: Room) -> None:
    running_meeting = example_room.get_running_meeting()
    assert running_meeting is None


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("rooms", "general_parameter_values"),
    [
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", True),
                ("homeroom__example_staff_user", True),
                ("homeroom__example_superuser", True),
                ("personalroom__example_user", True),
                ("personalroom__example_moderator_user", True),
                ("personalroom__example_staff_user", True),
                ("personalroom__example_superuser", True),
            ],
            [("home_room_enabled", True), ("home_room_teachers_only", True), ("personal_rooms_enabled", True)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", False),
                ("homeroom__example_staff_user", False),
                ("homeroom__example_superuser", False),
                ("personalroom__example_user", True),
                ("personalroom__example_moderator_user", True),
                ("personalroom__example_staff_user", True),
                ("personalroom__example_superuser", True),
            ],
            [("home_room_enabled", False), ("home_room_teachers_only", True), ("personal_rooms_enabled", True)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", True),
                ("homeroom__example_moderator_user", True),
                ("homeroom__example_staff_user", True),
                ("homeroom__example_superuser", True),
                ("personalroom__example_user", True),
                ("personalroom__example_moderator_user", True),
                ("personalroom__example_staff_user", True),
                ("personalroom__example_superuser", True),
            ],
            [("home_room_enabled", True), ("home_room_teachers_only", False), ("personal_rooms_enabled", True)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", False),
                ("homeroom__example_staff_user", False),
                ("homeroom__example_superuser", False),
                ("personalroom__example_user", True),
                ("personalroom__example_moderator_user", True),
                ("personalroom__example_staff_user", True),
                ("personalroom__example_superuser", True),
            ],
            [("home_room_enabled", False), ("home_room_teachers_only", False), ("personal_rooms_enabled", True)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", True),
                ("homeroom__example_staff_user", True),
                ("homeroom__example_superuser", True),
                ("personalroom__example_user", False),
                ("personalroom__example_moderator_user", False),
                ("personalroom__example_staff_user", False),
                ("personalroom__example_superuser", False),
            ],
            [("home_room_enabled", True), ("home_room_teachers_only", True), ("personal_rooms_enabled", False)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", False),
                ("homeroom__example_staff_user", False),
                ("homeroom__example_superuser", False),
                ("personalroom__example_user", False),
                ("personalroom__example_moderator_user", False),
                ("personalroom__example_staff_user", False),
                ("personalroom__example_superuser", False),
            ],
            [("home_room_enabled", False), ("home_room_teachers_only", True), ("personal_rooms_enabled", False)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", True),
                ("homeroom__example_moderator_user", True),
                ("homeroom__example_staff_user", True),
                ("homeroom__example_superuser", True),
                ("personalroom__example_user", False),
                ("personalroom__example_moderator_user", False),
                ("personalroom__example_staff_user", False),
                ("personalroom__example_superuser", False),
            ],
            [("home_room_enabled", True), ("home_room_teachers_only", False), ("personal_rooms_enabled", False)],
        ),
        (
            [
                ("ordinary_room", True),
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", False),
                ("homeroom__example_staff_user", False),
                ("homeroom__example_superuser", False),
                ("personalroom__example_user", False),
                ("personalroom__example_moderator_user", False),
                ("personalroom__example_staff_user", False),
                ("personalroom__example_superuser", False),
            ],
            [("home_room_enabled", False), ("home_room_teachers_only", False), ("personal_rooms_enabled", False)],
        ),
    ],
)
def test_room__objects_available(
    request: FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    rooms: Iterable[Tuple[str, bool]],
    general_parameter_values: Iterable[Tuple[str, bool]],
) -> None:
    def get_plain_room(room: Union[Room, HomeRoom, PersonalRoom]) -> Room:
        return getattr(room, "room_ptr", room)

    _rooms = set(get_plain_room(request.getfixturevalue(room)) for room, _ in rooms)
    _expected_rooms = set(get_plain_room(request.getfixturevalue(room)) for room, expected in rooms if expected)

    for key, value in general_parameter_values:
        setattr(gp_test_tenant, key, value)

    assert set(Room.objects.all()) == _rooms
    assert set(Room.objects_available.init(testserver_tenant, gp_test_tenant).all()) == _expected_rooms


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("rooms", "general_parameter_values"),
    [
        (
            [
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", True),
                ("homeroom__example_staff_user", True),
                ("homeroom__example_superuser", True),
            ],
            [("home_room_enabled", True), ("home_room_teachers_only", True)],
        ),
        (
            [
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", False),
                ("homeroom__example_staff_user", False),
                ("homeroom__example_superuser", False),
            ],
            [("home_room_enabled", False), ("home_room_teachers_only", True)],
        ),
        (
            [
                ("homeroom__example_user", True),
                ("homeroom__example_moderator_user", True),
                ("homeroom__example_staff_user", True),
                ("homeroom__example_superuser", True),
            ],
            [("home_room_enabled", True), ("home_room_teachers_only", False)],
        ),
        (
            [
                ("homeroom__example_user", False),
                ("homeroom__example_moderator_user", False),
                ("homeroom__example_staff_user", False),
                ("homeroom__example_superuser", False),
            ],
            [("home_room_enabled", False), ("home_room_teachers_only", False)],
        ),
    ],
)
def test_homeroom__objects_available(
    request: FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    rooms: Iterable[Tuple[str, bool]],
    general_parameter_values: Iterable[Tuple[str, bool]],
) -> None:
    _rooms = set(request.getfixturevalue(room) for room, _ in rooms)
    _expected_rooms = set(request.getfixturevalue(room) for room, expected in rooms if expected)

    for key, value in general_parameter_values:
        setattr(gp_test_tenant, key, value)

    assert set(HomeRoom.objects.all()) == _rooms
    assert set(HomeRoom.objects_available.init(testserver_tenant, gp_test_tenant).all()) == _expected_rooms


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("rooms", "general_parameter__personal_rooms_enabled"),
    [
        (
            [
                ("personalroom__example_user", True),
                ("personalroom__example_moderator_user", True),
                ("personalroom__example_staff_user", True),
                ("personalroom__example_superuser", True),
            ],
            True,
        ),
        (
            [
                ("personalroom__example_user", False),
                ("personalroom__example_moderator_user", False),
                ("personalroom__example_staff_user", False),
                ("personalroom__example_superuser", False),
            ],
            False,
        ),
    ],
)
def test_personalroom__objects_available(
    request: FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    rooms: Iterable[Tuple[str, bool]],
    general_parameter__personal_rooms_enabled: bool,
) -> None:
    _rooms = set(request.getfixturevalue(room) for room, _ in rooms)
    _expected_rooms = set(request.getfixturevalue(room) for room, expected in rooms if expected)

    gp_test_tenant.personal_rooms_enabled = general_parameter__personal_rooms_enabled

    assert set(PersonalRoom.objects.all()) == _rooms
    assert set(PersonalRoom.objects_available.init(testserver_tenant, gp_test_tenant).all()) == _expected_rooms
