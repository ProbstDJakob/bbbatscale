from unittest.mock import Mock

from requests import Response


def get_meeting_info_full_response_xml(meeting_id):
    return f"""
<response>
    <returncode>SUCCESS</returncode>
    <meetingName>Catherinechester</meetingName>
    <meetingID>{str(meeting_id)}</meetingID>
    <internalMeetingID>ae7020c93b2a9025170add256f50daf9086c1efa-1652347453328</internalMeetingID>
    <createTime>1652347453328</createTime>
    <createDate>Thu May 12 09:24:13 UTC 2022</createDate>
    <voiceBridge>43379</voiceBridge>
    <dialNumber>613-555-1234</dialNumber>
    <attendeePW>IXtJ9yStTAiINMrY31PFmU9damcLaSTH</attendeePW>
    <moderatorPW>EAvZQVHovKnvqPqTRKzy7SVDJeKaMjNV</moderatorPW>
    <running>true</running>
    <duration>0</duration>
    <hasUserJoined>true</hasUserJoined>
    <recording>false</recording>
    <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
    <startTime>1652347453339</startTime>
    <endTime>0</endTime>
    <participantCount>1</participantCount>
    <listenerCount>0</listenerCount>
    <voiceParticipantCount>0</voiceParticipantCount>
    <videoCount>0</videoCount>
    <maxUsers>0</maxUsers>
    <moderatorCount>1</moderatorCount>
    <attendees>
        <attendee>
            <userID>w_ulzy1izcwz0v</userID>
            <fullName>ADMIN, ADMIN</fullName>
            <role>MODERATOR</role>
            <isPresenter>true</isPresenter>
            <isListeningOnly>false</isListeningOnly>
            <hasJoinedVoice>false</hasJoinedVoice>
            <hasVideo>false</hasVideo>
            <clientType>HTML5</clientType>
            <customdata>
                <bbb_show_participants_on_login>true
                </bbb_show_participants_on_login>
                <bbb_record_video>true</bbb_record_video>
                <bbb_show_public_chat_on_login>true
                </bbb_show_public_chat_on_login>
                <bbb_skip_video_preview>false</bbb_skip_video_preview>
                <bbb_force_restore_presentation_on_new_events>false
                </bbb_force_restore_presentation_on_new_events>
                <bbb_skip_video_preview_on_first_join>false
                </bbb_skip_video_preview_on_first_join>
                <bbb_listen_only_mode>true</bbb_listen_only_mode>
                <bbb_auto_join_audio>true</bbb_auto_join_audio>
                <bbb_skip_check_audio_on_first_join>false
                </bbb_skip_check_audio_on_first_join>
                <bbb_auto_swap_layout>false</bbb_auto_swap_layout>
                <bbb_mirror_own_webcam>false</bbb_mirror_own_webcam>
                <bbb_auto_share_webcam>false</bbb_auto_share_webcam>
                <bbb_skip_check_audio>false</bbb_skip_check_audio>
            </customdata>
        </attendee>
    </attendees>
    <metadata>
        <creator>admin</creator>
        <roomsmeetingid>13b8c733-4d73-43c2-9a70-216c0a037410</roomsmeetingid>
        <streamingurl>None</streamingurl>
        <bbb-origin>BBB@Scale</bbb-origin>
    </metadata>
    <isBreakout>false</isBreakout>
</response>
"""


def get_meeting_info_failed_xml():
    return """
<response>
    <returncode>FAILED</returncode>
</response>
    """


def get_meeting_info_invalid_xml():
    return """
    <response>
        <meetingName>Catherinechester</meetingName>
        <meetingID>{str(meeting_id)}</meetingID>
    </response>
        """


def mock_response(content):
    resp = Mock(spec=Response)
    resp.text = content
    return resp
