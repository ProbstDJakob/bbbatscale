import json

import pytest
from core.models import Meeting, User
from core.views.recordings import map_recording, recording_update
from django.contrib.sites.models import Site
from django.test.client import Client
from django.urls import reverse
from freezegun import freeze_time


@pytest.mark.django_db
def test_post_recording_update_title(client, example_superuser, example_group, gp_test_tenant, mocker):
    def mock_update_publish_recordings(params, rpc, meta):
        pass

    mocker.patch("core.views.recordings.update_publish_recordings", mock_update_publish_recordings)

    client.force_login(example_superuser)
    example_recording = Meeting.objects.create(
        room_name="Example Room",
        replay_id="example-replay-id",
        replay_url="https://example.org/my-awesome-recording",
        replay_title="Test title",
    )
    response = client.post(
        reverse(recording_update, args=[example_recording.id]),
        data={"replay_title": "Changed example title"},
    )
    assert response.status_code == 302
    assert Meeting.objects.get(replay_id=example_recording.replay_id).replay_title == "Changed example title"


@pytest.mark.django_db
@freeze_time("2020-04-26T16:29:55+00:00")
def test_map_recording(example_user: User) -> None:
    example_recording = Meeting.objects.create(
        creator=example_user,
        creator_name="will be ignored as long as creator is set",
        room_name="Example Room",
        replay_id="example-replay-id",
        replay_url="https://example.org/my-awesome-recording",
        replay_title="Test title",
    )
    assert map_recording(example_recording) == {
        "updateUrl": f"/core/recordings/update/{example_recording.id}",
        "deleteUrl": f"/core/recording/mark/deletion/{example_recording.id}",
        "unmarkDeleteUrl": f"/core/recording/unmark/deletion/{example_recording.id}",
        "replayUrl": "/p/example-replay-id",
        "downloadUrl": f"/core/recording/download/zip/{example_recording.id}",
        "creator": str(example_user),
        "roomName": "Example Room",
        "titel": "Test title",
        "started": "2020-04-26T16:29:55+00:00",
        "toDelete": False,
    }

    example_recording.creator = None
    assert map_recording(example_recording) == {
        "updateUrl": f"/core/recordings/update/{example_recording.id}",
        "deleteUrl": f"/core/recording/mark/deletion/{example_recording.id}",
        "unmarkDeleteUrl": f"/core/recording/unmark/deletion/{example_recording.id}",
        "replayUrl": "/p/example-replay-id",
        "downloadUrl": f"/core/recording/download/zip/{example_recording.id}",
        "creator": example_recording.creator_name,
        "roomName": "Example Room",
        "titel": "Test title",
        "started": "2020-04-26T16:29:55+00:00",
        "toDelete": False,
    }


@pytest.mark.django_db
def test_get_rooms(
    client: Client,
    example_superuser: User,
    testserver_tenant: Site,
) -> None:
    for i in range(1, 50):
        m = Meeting.objects.create(
            creator_name=f"example.user{i}@example.org",
            room_name=f"Example Room {i}",
            replay_id="example-replay-id",
            replay_url="https://example.org/my-awesome-recording",
            replay_title=f"Test title {i}",
        )
        m.tenants.add(testserver_tenant)
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_recordings"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49

    response = client.get(f"{reverse('api_fetch_recordings')}?filter-search=5")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5
