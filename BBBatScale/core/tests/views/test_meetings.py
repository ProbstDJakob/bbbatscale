from datetime import datetime
from typing import List, Optional, Tuple, Union
from uuid import UUID

import pytest
from core.constants import MEETING_STATE_RUNNING, SERVER_STATE_UP
from core.models import Meeting, Room, SchedulingStrategy, Server, User
from core.views.meetings import MeetingDetail, map_meeting
from django.contrib.sites.models import Site
from django.utils import timezone
from freezegun import freeze_time


@pytest.mark.django_db
@pytest.fixture(scope="function")
@freeze_time("1970-01-01T00:00:00+00:00")
def meeting() -> Meeting:
    return Meeting.objects.create(
        room=None,
        room_name="Room Name",
        id=UUID("12345678-1234-5678-1234-567812345678"),
        server=None,
        state=MEETING_STATE_RUNNING,
        external_meeting=None,
        creator_name="Creator Name",
        replay_id="12345",
        replay_url="",
        participant_count=42,
        videostream_count=20,
        configuration=None,
        attendee_pw="abc",
        moderator_pw="def",
        started=timezone.now(),
        last_health_check=timezone.now(),
        to_delete=None,
        is_breakout=False,
        bbb_meeting_id="",
        published=False,
    )


@pytest.mark.django_db
@pytest.fixture(scope="function")
def scheduling_strategy() -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(name="Example Scheduling Strategy")


@pytest.mark.django_db
@pytest.fixture(scope="function")
def server(scheduling_strategy: SchedulingStrategy) -> Server:
    return Server.objects.create(scheduling_strategy=scheduling_strategy, dns="example.org", state=SERVER_STATE_UP)


@pytest.mark.django_db
def test_meeting_detail_class(
    meeting: Meeting,
    scheduling_strategy: SchedulingStrategy,
    server: Server,
    testserver_tenant: Site,
    testserver2_tenant: Site,
) -> None:
    def convert(
        meeting_detail: MeetingDetail,
    ) -> List[Union[Tuple[str, Optional[str], Optional[str]], Tuple[str, List[Tuple[Optional[str], Optional[str]]]]]]:
        return list(
            (detail.name, list(tuple(entry) for entry in detail))
            if detail.has_multiple_values()
            else (detail.name, *detail)
            for detail in meeting_detail
        )

    privileged_user = User.objects.create_superuser("privileged_user", tenant=testserver_tenant)
    unprivileged_user = User.objects.create_user("unprivileged_user", tenant=testserver_tenant)
    room = Room.objects.create(scheduling_strategy=scheduling_strategy, name="Example Room")

    assert convert(MeetingDetail(meeting, privileged_user, testserver_tenant)) == [
        ("ID", "12345678-1234-5678-1234-567812345678", None, False),
        ("BBB Meeting ID", None, None, False),
        ("External Meeting ID", None, None, False),
        ("Room", "Room Name", None, False),
        ("Server", None, None, False),
        ("State", "Running", None, False),
        ("Tenants", []),
        ("Creator", "Creator Name", None, False),
        ("Replay ID", "12345", None, False),
        ("Participant count", "42", None, False),
        ("Videostream count", "20", None, False),
        ("Configuration", None, None, False),
        ("Attendee Password", "abc", None, False),
        ("Moderator Password", "def", None, False),
        ("Started", "1970-01-01T00:00:00+00:00", None, True),
        ("Last health check", "1970-01-01T00:00:00+00:00", None, True),
        ("Is breakout", "False", None, False),
        ("Published", "False", None, False),
        ("To delete", None, None, False),
    ]

    meeting.room = room
    meeting.server = server
    meeting.is_breakout = True
    meeting.to_delete = datetime.fromisoformat("1970-01-01T00:00:00+00:00")
    meeting.replay_id = ""
    meeting.replay_url = "https://example.org"
    meeting.tenants.add(testserver_tenant.id, testserver2_tenant.id)

    assert convert(MeetingDetail(meeting, privileged_user, testserver_tenant)) == [
        ("ID", "12345678-1234-5678-1234-567812345678", None, False),
        ("BBB Meeting ID", None, None, False),
        ("External Meeting ID", None, None, False),
        ("Room", "Room Name", f"/core/room/update/{room.id}", False),
        ("Server", "example.org", f"/core/server/update/{server.id}", False),
        ("State", "Running", None, False),
        (
            "Tenants",
            [
                ("testserver", f"/core/tenant/update/{testserver_tenant.id}", False),
                ("testserver2", f"/core/tenant/update/{testserver2_tenant.id}", False),
            ],
        ),
        ("Creator", "Creator Name", None, False),
        ("Replay ID", None, "https://example.org", False),
        ("Participant count", "42", None, False),
        ("Videostream count", "20", None, False),
        ("Configuration", None, None, False),
        ("Attendee Password", "abc", None, False),
        ("Moderator Password", "def", None, False),
        ("Started", "1970-01-01T00:00:00+00:00", None, True),
        ("Last health check", "1970-01-01T00:00:00+00:00", None, True),
        ("Is breakout", "True", None, False),
        ("Published", "False", None, False),
        ("To delete", "1970-01-01T00:00:00+00:00", None, True),
    ]

    assert convert(MeetingDetail(meeting, unprivileged_user, testserver_tenant)) == [
        ("ID", "12345678-1234-5678-1234-567812345678", None, False),
        ("BBB Meeting ID", None, None, False),
        ("External Meeting ID", None, None, False),
        ("Room", "Room Name", None, False),
        ("Server", "example.org", None, False),
        ("State", "Running", None, False),
        ("Tenants", [("testserver", None, False), ("testserver2", None, False)]),
        ("Creator", "Creator Name", None, False),
        ("Replay ID", None, "https://example.org", False),
        ("Participant count", "42", None, False),
        ("Videostream count", "20", None, False),
        ("Configuration", None, None, False),
        ("Attendee Password", "abc", None, False),
        ("Moderator Password", "def", None, False),
        ("Started", "1970-01-01T00:00:00+00:00", None, True),
        ("Last health check", "1970-01-01T00:00:00+00:00", None, True),
        ("Is breakout", "True", None, False),
        ("Published", "False", None, False),
        ("To delete", "1970-01-01T00:00:00+00:00", None, True),
    ]


@pytest.mark.django_db
def test_map_meeting(meeting: Meeting, server: Server) -> None:
    assert map_meeting(meeting) == {
        "detailUrl": f"/core/meeting/detail/{meeting.id}",
        "roomName": "Room Name",
        "creator": "Creator Name",
        "state": "RUNNING",
        "server": None,
        "started": "1970-01-01T00:00:00+00:00",
        "participantCount": 42,
        "videostreamCount": 20,
    }

    meeting.server = server

    assert map_meeting(meeting) == {
        "detailUrl": f"/core/meeting/detail/{meeting.id}",
        "roomName": "Room Name",
        "creator": "Creator Name",
        "state": "RUNNING",
        "server": "example.org",
        "started": "1970-01-01T00:00:00+00:00",
        "participantCount": 42,
        "videostreamCount": 20,
    }
