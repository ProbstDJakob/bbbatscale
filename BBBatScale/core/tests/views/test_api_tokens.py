import json

import pytest
from core.models import ApiToken, User
from core.views.api_token import map_token
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_map_server() -> None:
    token = ApiToken.objects.create(name="example-token", secret="my-example-secret")
    assert map_token(token) == {
        "updateUrl": f"/core/token/update/{token.id}",
        "deleteUrl": f"/core/token/delete/{token.id}",
        "apiUrl": "/core/example-token/bigbluebutton/api/",
        "name": "example-token",
        "secret": "my-example-secret",
        "tenants": [],
        "slug": "example-token",
        "schedulingStrategy": None,
    }


@pytest.mark.django_db
def test_get_tokens(client: Client, example_superuser: User) -> None:
    for i in range(1, 50):
        ApiToken.objects.create(name=f"example-token-{i}")
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_tokens"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49
