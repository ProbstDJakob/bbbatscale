from unittest.mock import MagicMock

import pytest
from core.views import slides
from django.core.files import File
from django.urls import reverse


@pytest.mark.django_db
@pytest.mark.parametrize("media_enabled, response_code", [(True, 200), (False, 404)])
def test_slides_overview_gp_media_disabled_and_enabled(
    media_enabled, response_code, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = media_enabled
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.get(reverse(slides.slides_overview), SERVER_NAME=testserver_tenant.name)

    assert response.status_code == response_code


@pytest.mark.django_db
@pytest.mark.parametrize("media_enabled, response_code", [(True, 200), (False, 404)])
def test_owned_slides_overview_gp_media_disabled_and_enabled(
    media_enabled, response_code, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = media_enabled
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.get(reverse(slides.owned_slides_overview), SERVER_NAME=testserver_tenant.name)

    assert response.status_code == response_code


@pytest.mark.django_db
@pytest.mark.parametrize("media_enabled, response_code", [(True, 302), (False, 404)])
def test_slides_delete_overview_gp_media_disabled_and_enabled(
    example_slides, media_enabled, response_code, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = media_enabled
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.post(
        reverse(slides.slides_delete, kwargs={"slides_id": example_slides.id}), SERVER_NAME=testserver_tenant.name
    )

    assert response.status_code == response_code


@pytest.mark.django_db
@pytest.mark.parametrize("media_enabled, response_code", [(True, 302), (False, 404)])
def test_owned_slides_delete_overview_gp_media_disabled_and_enabled(
    example_slides, media_enabled, response_code, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_superuser
    example_slides.save()
    gp_test_tenant.media_enabled = media_enabled
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.post(
        reverse(slides.owned_slides_delete, kwargs={"slides_id": example_slides.id}), SERVER_NAME=testserver_tenant.name
    )

    assert response.status_code == response_code


@pytest.mark.django_db
@pytest.mark.parametrize("media_enabled, response_code", [(True, 200), (False, 404)])
def test_slides_create_update_overview_gp_media_disabled_and_enabled(
    media_enabled, response_code, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = media_enabled
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.get(reverse(slides.slides_create_update), SERVER_NAME=testserver_tenant.name)

    assert response.status_code == response_code


@pytest.mark.django_db
def test_slides_create_update_overview_media_enabled_and_slides_id_is_not_none(
    example_slides, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_superuser
    example_slides.save()
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.post(
        reverse(slides.slides_create_update, kwargs={"slides_id": example_slides.id}),
        SERVER_NAME=testserver_tenant.name,
    )
    assert response.status_code == 200


@pytest.mark.django_db
def test_slides_create_update_overview_media_enabled_and_slides_id_is_not_none_user_can_not_change_slides(
    example_slides, client, example_user, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_user
    example_slides.save()
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_user)
    response = client.post(
        reverse(slides.slides_create_update, kwargs={"slides_id": example_slides.id}),
        SERVER_NAME=testserver_tenant.name,
    )
    assert response.status_code == 403


@pytest.mark.django_db
def test_slides_create_update_overview_media_enabled_and_slides_id_is_none_user_can_not_add_slides(
    client, example_user, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_user)
    response = client.get(reverse(slides.slides_create_update), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 403


@pytest.mark.django_db
def test_slides_create_update_overview_media_enabled_and_slides_id_is_not_none_and_form_is_valid(
    example_slides, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_superuser
    example_slides.save()
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_superuser)
    file_mock = MagicMock(spec=File)
    file_mock.name = "example.txt"
    response = client.post(
        reverse(slides.slides_create_update, kwargs={"slides_id": example_slides.id}),
        data={"name": "test", "file": file_mock},
    )
    assert response.status_code == 302
    assert response.url == "/core/slides/overview/"


@pytest.mark.django_db
@pytest.mark.parametrize("media_enabled, response_code", [(True, 200), (False, 404)])
def test_owned_slides_create_update_overview_gp_media_disabled_and_enabled(
    media_enabled, response_code, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = media_enabled
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.get(reverse(slides.owned_slides_create_update), SERVER_NAME=testserver_tenant.name)

    assert response.status_code == response_code


@pytest.mark.django_db
def test_owned_slides_create_update_overview_media_enabled_and_slides_id_is_not_none(
    example_slides, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_superuser
    example_slides.save()
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_superuser)
    response = client.post(
        reverse(slides.owned_slides_create_update, kwargs={"slides_id": example_slides.id}),
        SERVER_NAME=testserver_tenant.name,
    )
    assert response.status_code == 200


@pytest.mark.django_db
def test_owned_slides_create_update_overview_media_enabled_and_slides_id_is_not_none_user_can_not_change_slides(
    example_slides, client, example_user, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_user
    example_slides.save()
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_user)
    response = client.post(
        reverse(slides.owned_slides_create_update, kwargs={"slides_id": example_slides.id}),
        SERVER_NAME=testserver_tenant.name,
    )
    assert response.status_code == 403


@pytest.mark.django_db
def test_owned_slides_create_update_overview_media_enabled_and_slides_id_is_none_user_can_not_add_slides(
    client, example_user, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_user)
    response = client.get(reverse(slides.owned_slides_create_update), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 403


@pytest.mark.django_db
def test_owned_slides_create_update_overview_media_enabled_and_slides_id_is_not_none_and_form_is_valid(
    example_slides, client, example_superuser, gp_test_tenant, testserver_tenant, settings
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    example_slides.owner = example_superuser
    example_slides.save()
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    client.force_login(example_superuser)
    file_mock = MagicMock(spec=File)
    file_mock.name = "example.txt"
    response = client.post(
        reverse(slides.owned_slides_create_update, kwargs={"slides_id": example_slides.id}),
        data={"name": "test", "file": file_mock},
    )
    assert response.status_code == 302
    assert response.url == "/core/ownedSlides/overview/"
