import json

import pytest
from core.constants import SERVER_STATE_DISABLED, SERVER_STATE_UP
from core.models import SchedulingStrategy, Server, User
from core.views.servers import map_server
from django.contrib.sites.models import Site
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_map_server(example_server: Server) -> None:
    assert map_server(example_server) == {
        "updateUrl": f"/core/server/update/{example_server.id}",
        "deleteUrl": f"/core/server/delete/{example_server.id}",
        "id": example_server.id,
        "schedulingStrategy": "example",
        "dns": "example.org",
        "machineId": None,
        "sharedSecret": "123456789",
        "state": SERVER_STATE_UP,
        "datacenter": None,
        "participants": 0,
        "maxParticipants": 250,
        "videostreams": 0,
        "maxVideostreams": 25,
        "utilization": 0,
        "lastHealthcheck": None,
    }


@pytest.mark.django_db
def test_get_servers(
    client: Client, example_superuser: User, example_scheduling_strategy: SchedulingStrategy, testserver_tenant: Site
) -> None:
    for i in range(1, 50):
        Server.objects.create(
            scheduling_strategy=example_scheduling_strategy,
            dns=f"example-{i}.org",
            shared_secret="123456789",
            state=SERVER_STATE_UP,
        )
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_servers"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49

    response = client.get(f"{reverse('api_fetch_servers')}?filter-search=5")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5

    Server.objects.filter(dns__icontains="6").update(state=SERVER_STATE_DISABLED)

    response = client.get(f"{reverse('api_fetch_servers')}?filter-state={SERVER_STATE_DISABLED}")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5

    scheduling_strategy_tenant2 = SchedulingStrategy.objects.create(name="NEW EXAMPLE")
    scheduling_strategy_tenant2.tenants.set([testserver_tenant])

    Server.objects.filter(dns__icontains="8").update(scheduling_strategy=scheduling_strategy_tenant2)

    response = client.get(f"{reverse('api_fetch_servers')}?filter-strategy={scheduling_strategy_tenant2.pk}")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5
