import pytest
from core.constants import SERVER_STATE_UP
from core.forms import ServerForm
from core.models import ServerType


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"dns": "bbb.example.org", "shared_secret": "EXAMPLE_SECRET", "state": SERVER_STATE_UP}, True),
        (
            {
                "dns": "bbb.example.org",
                "shared_secret": "EXAMPLE_SECRET",
                "state": SERVER_STATE_UP,
                "machine_id": "AB:CD:12:23:34",
            },
            True,
        ),
        (
            {"dns": None, "shared_secret": "EXAMPLE_SECRET", "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"},
            False,
        ),
        (
            {"dns": "bbb.example.org", "shared_secret": None, "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"},
            False,
        ),
        ({"shared_secret": "EXAMPLE_SECRET", "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"}, False),
        ({"dns": "bbb.example.org", "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"}, False),
        ({}, False),
    ],
)
def test_server_creation_form(
    data,
    is_valid,
    gp_test_tenant,
    example_scheduling_strategy,
):
    form_data = {
        "server_types": [ServerType.objects.get(name="worker")],
        "scheduling_strategy": example_scheduling_strategy,
        "participant_count_max": 250,
        "videostream_count_max": 25,
    }
    form_data.update(data)
    form = ServerForm(
        data=form_data,
        general_parameter=gp_test_tenant,
    )

    assert form.is_valid() is is_valid


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"dns": "bbb.example.org", "shared_secret": "EXAMPLE_SECRET", "state": SERVER_STATE_UP}, True),
        (
            {
                "dns": "bbb.example.org",
                "shared_secret": "EXAMPLE_SECRET",
                "state": SERVER_STATE_UP,
                "machine_id": "AB:CD:12:23:34",
            },
            True,
        ),
        (
            {"dns": None, "shared_secret": "EXAMPLE_SECRET", "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"},
            False,
        ),
        (
            {"dns": "bbb.example.org", "shared_secret": None, "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"},
            False,
        ),
        ({"shared_secret": "EXAMPLE_SECRET", "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"}, False),
        ({"dns": "bbb.example.org", "state": SERVER_STATE_UP, "machine_id": "AB:CD:12:23:34"}, False),
        ({}, False),
    ],
)
def test_server_update_form(
    data,
    is_valid,
    gp_test_tenant,
    example_scheduling_strategy,
    example_server,
):
    form_data = {
        "server_types": [ServerType.objects.get(name="worker")],
        "scheduling_strategy": example_scheduling_strategy,
        "participant_count_max": 250,
        "videostream_count_max": 25,
    }
    form_data.update(data)
    form = ServerForm(data=form_data, general_parameter=gp_test_tenant, instance=example_server)

    assert form.is_valid() is is_valid


@pytest.mark.django_db
def test_room_update_form_shared_resources(
    gp_test_tenant,
    example_scheduling_strategy,
    example_server,
    testserver2_tenant,
):
    example_scheduling_strategy.tenants.add(testserver2_tenant)
    form = ServerForm(
        data={
            "dns": "bbb.example.org",
            "shared_secret": "EXAMPLE_SECRET",
            "state": SERVER_STATE_UP,
            "machine_id": "AB:CD:12:23:34",
            "server_types": [ServerType.objects.get(name="worker")],
            "scheduling_strategy": example_scheduling_strategy,
            "participant_count_max": 250,
            "videostream_count_max": 25,
        },
        general_parameter=gp_test_tenant,
        instance=example_server,
    )

    assert form.is_valid() is True
    assert form.fields["scheduling_strategy"].queryset.count() == 1
