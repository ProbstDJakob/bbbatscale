from core.middleware import CurrentGeneralParameterMiddleware, CurrentTenantMiddleware, _get_tenant
from core.models import GeneralParameter
from django.contrib.sites.models import Site
from django.http import HttpRequest, HttpResponse


def test_get_tenant_does_not_cache(testserver_tenant: Site) -> None:
    assert _get_tenant(testserver_tenant.domain) is not _get_tenant(testserver_tenant.domain)


def test_current_tenant_parameter_middleware(testserver_tenant: Site) -> None:
    request = HttpRequest()
    request.META["HTTP_HOST"] = testserver_tenant.domain

    assert not hasattr(request, "tenant")

    CurrentTenantMiddleware(lambda _: HttpResponse())(request)

    assert getattr(request, "tenant", None) == testserver_tenant


def test_current_general_parameter_middleware(testserver_tenant: Site) -> None:
    general_parameter = GeneralParameter.objects.load(testserver_tenant)
    request = HttpRequest()
    request.tenant = testserver_tenant

    assert not hasattr(request, "general_parameter")

    CurrentGeneralParameterMiddleware(lambda _: HttpResponse())(request)

    assert getattr(request, "general_parameter", None) == general_parameter
