from typing import Any, Callable

import pytest
from conftest import RequestFactory
from core.models import GeneralParameter, Meeting, MeetingConfiguration, User
from core.services import create_meeting_and_bbb_parameter, moderator_message
from django.contrib.sites.models import Site


def test_create_meeting_and_bbb_parameter(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting, example_user: User
) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.logoutUrl = "https://testserver"
    create_parameter = {}
    example_meeting.creator = example_user
    example_meeting.creator_name = example_user.username + "-not-only-the-username"

    gp_test_tenant.enable_meeting_rtmp_streaming = True
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert bbb_parameter["name"] == example_meeting.room_name
    assert bbb_parameter["meetingID"] == example_meeting.id
    assert bbb_parameter["attendeePW"] == example_meeting.attendee_pw
    assert bbb_parameter["moderatorPW"] == example_meeting.moderator_pw
    assert bbb_parameter["logoutURL"] == meeting_config.logoutUrl
    assert bbb_parameter["muteOnStart"] == "true"
    assert bbb_parameter["lockSettingsDisableCam"] == "false"
    assert bbb_parameter["lockSettingsDisableMic"] == "false"
    assert bbb_parameter["lockSettingsDisableNote"] == "false"
    assert bbb_parameter["lockSettingsDisablePublicChat"] == "false"
    assert bbb_parameter["lockSettingsDisablePrivateChat"] == "false"
    assert bbb_parameter["allowModsToUnmuteUsers"] == "false"
    assert bbb_parameter["allowModsToEjectCameras"] == "false"
    assert bbb_parameter["learningDashboardEnabled"] == "false"
    assert bbb_parameter["meetingLayout"] == meeting_config.meetingLayout
    assert bbb_parameter["guestPolicy"] == meeting_config.guest_policy
    assert bbb_parameter["record"] == "false"
    assert bbb_parameter["allowStartStopRecording"] == "false"
    assert bbb_parameter["meta_creator"] == example_meeting.creator_display_name
    assert bbb_parameter["meta_roomsmeetingid"] == example_meeting.pk
    assert bbb_parameter["meta_bbb-origin"] == "BBB@Scale"
    assert bbb_parameter["meta_streamingUrl"] == meeting_config.streamingUrl

    example_meeting.creator = None
    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)
    assert bbb_parameter["meta_creator"] == example_meeting.creator_name


def test_create_meeting_and_bbb_parameter_moderator_message(testserver_tenant: Site, example_meeting: Meeting) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration

    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    meeting_config.meeting_duration_time = 200
    meeting_config.save()

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)
    assert bbb_parameter["moderatorOnlyMessage"] == moderator_message(
        request,
        example_meeting.room_name,
        meeting_config.access_code,
        meeting_config.only_prompt_guests_for_access_code,
        meeting_config.guest_policy,
        meeting_config.maxParticipants,
    )


def test_create_meeting_and_bbb_parameter_duration_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration

    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    meeting_config.meeting_duration_time = 200
    meeting_config.save()

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert bbb_parameter["duration"] == 200


def test_create_meeting_and_bbb_parameter_no_duration_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert "duration" not in bbb_parameter


def test_create_meeting_and_bbb_parameter_welcome_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.welcome_message = "Hello"
    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert bbb_parameter["welcome"] == "Hello"


def test_create_meeting_and_bbb_parameter_no_welcome_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert "welcome" not in bbb_parameter


def test_create_meeting_and_bbb_parameter_dialNumber_set(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting
) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.dialNumber = "0123456789"

    gp_test_tenant.enable_meeting_dial_number = True
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")
    create_parameter = {}

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert bbb_parameter["dialNumber"] == "0123456789"


def test_create_meeting_and_bbb_parameter_no_dialNumber_set(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting
) -> None:
    create_parameter = {}

    gp_test_tenant.enable_meeting_dial_number = True
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert "dialNumber" not in bbb_parameter


def test_create_meeting_and_bbb_parameter_maxParticipants_set(
    testserver_tenant: Site, example_meeting: Meeting
) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.maxParticipants = "10"
    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert bbb_parameter["maxParticipants"] == "10"


def test_create_meeting_and_bbb_parameter_no_maxParticipants_set(
    testserver_tenant: Site, example_meeting: Meeting
) -> None:
    request = RequestFactory(testserver_tenant).get("/")
    create_parameter = {}

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)

    assert "maxParticipants" not in bbb_parameter


@pytest.mark.parametrize(
    "general_parameter_key, meeting_config_key, meeting_config_value_before,"
    "meeting_config_value_after, test_expression",
    [
        (
            "enable_recordings",
            "allow_recording",
            False,
            True,
            lambda bbb_parameter: bbb_parameter["record"] == "false"
            and bbb_parameter["allowStartStopRecording"] == "false",
        ),
        (
            "enable_adminunmute",
            "allow_unmuteusers",
            False,
            True,
            lambda bbb_parameter: bbb_parameter["allowModsToUnmuteUsers"] == "false",
        ),
        (
            "enable_meeting_dial_number",
            "dialNumber",
            "",
            "0123456789",
            lambda bbb_parameter: "dialNumber" not in bbb_parameter,
        ),
        (
            "enable_meeting_rtmp_streaming",
            "streamingUrl",
            "",
            "rtmp://example.org/",
            lambda bbb_parameter: bbb_parameter["meta_streamingUrl"] == "",
        ),
    ],
)
def test_create_meeting_and_bbb_parameter_disabled_recording(
    testserver_tenant: Site,
    example_meeting: Meeting,
    general_parameter_key: str,
    meeting_config_key: str,
    meeting_config_value_before: Any,
    meeting_config_value_after: Any,
    test_expression: Callable[[dict], bool],
) -> None:
    general_parameter = GeneralParameter.objects.load(testserver_tenant)
    setattr(general_parameter, general_parameter_key, False)
    general_parameter.save()

    request = RequestFactory(testserver_tenant, general_parameter=general_parameter).get("/")
    create_parameter = {}

    setattr(example_meeting.configuration, meeting_config_key, meeting_config_value_before)
    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)
    assert test_expression(bbb_parameter)

    setattr(example_meeting.configuration, meeting_config_key, meeting_config_value_after)
    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, create_parameter)
    assert test_expression(bbb_parameter)
