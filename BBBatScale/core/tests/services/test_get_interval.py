from datetime import timedelta

import pytest
from core.models import AgentConfiguration, ApiToken, SchedulingStrategy
from core.services import (
    AgentConfigurationSelector,
    apitoken_agent_config_selector,
    get_interval,
    select_registration_interval,
    select_stats_interval,
)


def test_get_interval():
    def mock_agent_selector() -> AgentConfiguration:
        return AgentConfiguration(registration_interval=timedelta(seconds=10), stats_interval=timedelta(seconds=20))

    agent_selector: AgentConfigurationSelector = mock_agent_selector

    interval = get_interval(agent_selector, select_stats_interval)
    assert timedelta(seconds=20) == interval

    interval = get_interval(agent_selector, select_registration_interval)
    assert timedelta(seconds=10) == interval


@pytest.mark.parametrize(
    "token,expected_interval",
    [
        (
            ApiToken(
                scheduling_strategy=SchedulingStrategy(
                    agent_configuration=AgentConfiguration(
                        registration_interval=timedelta(seconds=10),
                    )
                )
            ),
            timedelta(seconds=10),
        ),
        (ApiToken(scheduling_strategy=SchedulingStrategy(agent_configuration=None)), None),
        (ApiToken(scheduling_strategy=None), None),
    ],
)
def test_get_interval_api_token(token, expected_interval):
    reg_interval = get_interval(apitoken_agent_config_selector(token), select_registration_interval)
    stats_interval = get_interval(apitoken_agent_config_selector(token), select_stats_interval)

    assert expected_interval == reg_interval
    assert None is stats_interval
