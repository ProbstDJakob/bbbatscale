import logging
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from tendo.singleton import SingleInstance, SingleInstanceException

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Remove all media files marked for removal."

    def handle(self, *args, **options):
        logger.info("Start cleanup of media files")

        try:
            SingleInstance()
            logger.debug("Start clean_media_files for any file marked for removal")
            for root, _, files in os.walk(settings.MEDIA_ROOT):
                for file in files:
                    if file.endswith(".delete"):
                        file_path = os.path.join(root, file)
                        os.remove(file_path)
                        logger.info(f"Removed: {file_path}")
            logger.debug("End clean_media_files")

        except SingleInstanceException as e:
            logger.error("An error occurred during the cleanup. Details: " + str(e))

        logger.info("End clean_media_files command")
