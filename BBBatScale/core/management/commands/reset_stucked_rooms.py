# encoding: utf-8
import logging
from datetime import timedelta

from core.constants import MEETING_STATE_CREATING, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE
from core.models import Room
from core.services import end_meeting
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone
from tendo.singleton import SingleInstance, SingleInstanceException

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Reset all rooms which are stucked in creating state for 60sec and more."

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start reset for stucked rooms")

        try:
            SingleInstance()
            logger.debug("Start reset_room for rooms that were in creating state for more than 60 sec")
            for room in Room.objects.filter(
                last_running__lte=timezone.now() - timedelta(seconds=60), state=ROOM_STATE_CREATING
            ):
                meeting = room.meeting_set.filter(state=MEETING_STATE_CREATING).first()
                if meeting:
                    end_meeting(meeting)
                else:
                    room.state = ROOM_STATE_INACTIVE
                    room.last_running = None
                    room.save()
                logger.error("Room ({}) was in state 'creating' for more than 60 seconds. Has been reset!".format(room))
            logger.debug("End reset_room")

        except SingleInstanceException as e:
            logger.error("An error occurred during the reset. Details:" + str(e))

        logger.info("End reset_stucked_rooms command ")
