const { computed, unref } = Vue;

export default {
    props: {
        cardType: {
            type: String,
            default: null,
            validator(value) {
                return ["primary", "secondary", "success", "danger", "warning", "info", "light", "dark"].includes(value);
            }
        }
    },
    setup({ cardType }) {
        return {
            cardClass: computed(() => {
                return cardType ? [`card-${unref(cardType)}`] : [];
            })
        };
    },
    template: `
        <div class="card" :class="cardClass">
            <div class="card-header">
                <slot name="header" />
            </div>

            <div class="card-body">
                <slot />
            </div>
        </div>
    `
};
