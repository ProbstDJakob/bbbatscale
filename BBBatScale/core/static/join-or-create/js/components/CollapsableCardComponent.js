const { ref, onMounted } = Vue;

let nextId = 0;

export default {
    setup(props, context) {
        /**
         * @type {fetchJoinOrCreate}
         */
        const body = ref(null);
        const toggle = ref(null);
        const icon = ref(null);
        const rotateIcon = ref(false);

        const id = `collapsable-card-${nextId++}`;

        onMounted(() => {
            const jQueryBody = jQuery(body.value);
            jQueryBody.collapse({ toggle: false });
            jQueryBody.on("show.bs.collapse", () => {
                rotateIcon.value = true;
            });
            jQueryBody.on("hide.bs.collapse", () => {
                rotateIcon.value = false;
            });
        });

        context.expose({
            show() {
                jQuery(body.value).collapse("show");
            }
        });

        return {
            id,
            body,
            toggle,
            icon,
            rotateIcon
        };
    },
    template: `
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <button class="btn btn-flat stretched-link" type="button"
                            ref="toggle" data-toggle="collapse" :aria-controls="id"
                            :data-target="'#' + id" aria-expanded="false">
                        <slot name="header" />
                    </button>
                    <i ref="icon" class="fas fa-angle-left collapse-icon" :class="{ 'rotate': rotateIcon }" />
                </div>
            </div>

            <div ref="body" :id="id" class="collapse">
                <div class="card-body">
                    <slot />
                </div>
            </div>
        </div>
    `
};
