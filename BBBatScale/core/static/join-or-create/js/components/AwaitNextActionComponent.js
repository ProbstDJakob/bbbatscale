const { inject, onMounted, onUnmounted } = Vue;
import LoadingComponent from "./LoadingComponent.js";

export default {
    components: {
        LoadingComponent
    },
    setup(props, context) {
        /**
         * @type {fetchJoinOrCreate}
         */
        const fetchJoinOrCreate = inject("fetchJoinOrCreate");

        let isMounted = false;
        let intervalId = null;
        let controller = null;

        function start() {
            if (intervalId !== null || !isMounted) {
                return;
            }

            intervalId = setInterval(() => {
                controller?.abort();
                controller = new AbortController();
                fetchJoinOrCreate("/api/join-create/next-action", { showLoading: false, signal: controller.signal });
            }, 5000);
        }

        function cancel() {
            clearInterval(intervalId);
            controller?.abort();

            intervalId = null;
            controller = null;
        }

        onMounted(() => {
            isMounted = true;
            start();
        });

        onUnmounted(() => {
            isMounted = false;
            cancel();
        });

        context.expose({
            start,
            cancel
        });
    },
    template: `
        <LoadingComponent class="mb-3" />

        <div class="text-center">
            <slot />
        </div>
    `
};
