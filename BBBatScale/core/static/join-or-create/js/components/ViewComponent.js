import CardComponent from "./CardComponent.js";

export default {
    props: ["cardType"],
    components: {
        CardComponent
    },
    template: `
        <CardComponent :card-type="cardType">
            <template #header>
                <h3 class="text-center">
                    <slot name="header" />
                </h3>
            </template>

            <slot />
        </CardComponent>
    `
};
