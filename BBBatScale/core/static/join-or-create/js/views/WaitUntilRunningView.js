const { inject, ref, watchEffect } = Vue;
const { Translation } = VueI18n;
import ContainerComponent from "../components/ContainerComponent.js";
import ViewComponent from "../components/ViewComponent.js";
import AwaitNextActionComponent from "../components/AwaitNextActionComponent.js";

export default {
    components: {
        Translation,
        ContainerComponent,
        ViewComponent,
        AwaitNextActionComponent
    },
    setup() {
        const roomName = inject("roomName");
        const data = inject("data");
        const startedBySomeoneElse = ref(false);

        watchEffect(() => {
            /*
             * `startedBySomeoneElse` will only be sent if configuring a meeting has failed due to the fact
             * that someone else has configured it, but not on subsequent calls to `/api/join-create/next-action`
             * therefore we must cache this value.
             */
            startedBySomeoneElse.value = data.value.startedBySomeoneElse ?? startedBySomeoneElse.value;
        });

        return {
            roomName,
            startedBySomeoneElse
        };
    },
    template: `
        <ContainerComponent>
            <div v-if="startedBySomeoneElse" class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="icon fas fa-info" />
                <span v-t="'wait.untilRunning.startedBySomeoneElseInfo'" />
                <button type="button" class="close" data-dismiss="alert" :aria-label="$t('common.button.close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <ViewComponent>
                <template #header>
                    <Translation keypath="wait.untilRunning.header" tag="span">
                        <template #name>
                            <strong>{{ roomName }}</strong>
                        </template>
                    </Translation>
                </template>

                <AwaitNextActionComponent>
                    <span v-t="'wait.untilRunning.body'" />
                </AwaitNextActionComponent>
            </ViewComponent>
        </ContainerComponent>
    `
};
