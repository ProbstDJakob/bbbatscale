const { ref, inject, computed } = Vue;
const { Translation } = VueI18n;
import ViewContainerComponent from "../components/ViewContainerComponent.js";

export default {
    components: {
        Translation,
        ViewContainerComponent
    },
    setup() {
        /**
         * @type {fetchJoinOrCreate}
         */
        const fetchJoinOrCreate = inject("fetchJoinOrCreate");
        const roomName = inject("roomName");
        const data = inject("data");

        const joinName = ref(data.value.joinName);

        function submit() {
            fetchJoinOrCreate("/api/join-create/set-join-name", { requestData: { joinName: joinName.value } });
        }

        return {
            roomName,
            joinName,
            nameIsTooShort: computed(() => data.value.nameIsTooShort),
            submit
        };
    },
    directives: {
        focus: {
            mounted: (el) => el.focus()
        }
    },
    template: `
        <ViewContainerComponent>
            <template #header>
                <Translation keypath="join.joinName.header" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <div class="input-group" :class="{ 'is-invalid': nameIsTooShort }">
                <input @keyup.enter="submit" v-focus v-model="joinName" id="join-name" type="text"
                       class="form-control" :class="{ 'is-invalid': nameIsTooShort }" :placeholder="$t('join.joinName.placeholder')">
                <div class="input-group-append">
                    <button @click="submit" class="btn btn-primary" :title="$t('common.button.submit')">
                        <i class="fas fa-arrow-right" />
                    </button>
                </div>
            </div>

            <div class="invalid-feedback" v-t="'join.joinName.invalid'" />
            <div class="form-text text-muted" v-t="'join.joinName.help'" />
        </ViewContainerComponent>
    `
};
