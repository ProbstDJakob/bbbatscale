from typing import Callable

from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from core.exceptions import BaseHttpException
from core.models import GeneralParameter
from core.utils import get_tenant
from django.contrib.sites.models import Site
from django.core.handlers.exception import response_for_exception
from django.http import HttpRequest, HttpResponse
from django.http.request import split_domain_port
from django.utils.functional import LazyObject


class TenantLazyObject(LazyObject):
    def __init__(self, host: str) -> None:
        super().__init__()
        # Assign to __dict__ to avoid infinite __setattr__ loops.
        self.__dict__["_host"] = host

    def _setup(self) -> None:
        self._wrapped = _get_tenant(self.__dict__["_host"])


def _get_tenant(host: str) -> Site:
    try:
        # First attempt to look up the site by host with or without port.
        return Site.objects.get(domain__iexact=host)
    except Site.DoesNotExist:
        # Fallback to looking up site after stripping port from the host.
        domain, port = split_domain_port(host)
        return Site.objects.get(domain__iexact=domain)


class CurrentTenantASGIMiddleware(BaseMiddleware):
    def populate_scope(self, scope: dict) -> None:
        host_headers = [value for key, value in scope["headers"] if key == b"host"]

        if len(host_headers) == 0:
            raise ValueError("The required host HTTP-Header is not present.")
        elif len(host_headers) != 1:
            raise ValueError("The host HTTP-Header is specified more than once.")
        elif "tenant" not in scope:
            scope["tenant"] = TenantLazyObject(host_headers[0].decode("ISO-8859-1"))

    async def resolve_scope(self, scope: dict) -> None:
        await database_sync_to_async(scope["tenant"]._setup)()


class CurrentTenantMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        request.tenant = _get_tenant(request.get_host())
        return self.get_response(request)


class CurrentGeneralParameterMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        assert hasattr(request, "tenant"), (
            "The general parameter middleware requires the tenant middleware "
            "to be installed. Edit your MIDDLEWARE setting to insert "
            "'core.middleware.CurrentTenantMiddleware' before "
            "'core.middleware.CurrentGeneralParameterMiddleware'."
        )
        request.general_parameter = GeneralParameter.objects.load(get_tenant(request))
        return self.get_response(request)


class CatchHttpExceptionMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        return self.get_response(request)

    @staticmethod
    def process_exception(request: HttpRequest, exception: Exception) -> HttpResponse:
        if isinstance(exception, BaseHttpException):
            return exception.response(request)

        return response_for_exception(request, exception)
