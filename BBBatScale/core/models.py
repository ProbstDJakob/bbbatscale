from __future__ import annotations

import binascii
import copy
import itertools
import logging
import os
import random
import uuid
from datetime import datetime, timedelta
from hashlib import sha1
from math import ceil
from pathlib import Path
from typing import Any, Iterable, Optional, TypeVar
from urllib.parse import urljoin

from core.constants import (
    GUEST_POLICY,
    GUEST_POLICY_ALLOW,
    MEETING_STATE,
    MEETING_STATE_CREATING,
    MEETING_STATE_FINISHED,
    MEETING_STATE_RUNNING,
    MEETINGLAYOUT,
    MEETINGLAYOUT_SMART_LAYOUT,
    MODERATION_MODE,
    MODERATION_MODE_MODERATORS,
    ROOM_STATE,
    ROOM_STATE_INACTIVE,
    ROOM_VISIBILITY,
    ROOM_VISIBILITY_PRIVATE,
    SCHEDULING_STRATEGIES,
    SCHEDULING_STRATEGY_LEAST_PARTICIPANTS,
    SCHEDULING_STRATEGY_LEAST_UTILIZATION,
    SCHEDULING_STRATEGY_RANDOM_PICK_FROM_LEAST_UTILIZED,
    SERVER_STATE_DISABLED,
    SERVER_STATE_ERROR,
    SERVER_STATE_UP,
    SERVER_STATE_WAITING_RESPONSE,
    SERVER_STATES,
    WEBHOOKS,
)
from core.feature_flag import Feature
from core.storage import FileSystemStorageWithRestrictedAccess
from core.utils import BigBlueButton, get_permissions, meeting_recording_xml
from django.conf import settings
from django.contrib.auth.models import AbstractUser, Permission
from django.contrib.auth.models import UserManager as DjangoUserManager
from django.contrib.sites.models import Site
from django.core.validators import MinValueValidator
from django.db import IntegrityError, models
from django.db.models import Q, QuerySet, Sum
from django.db.models.signals import m2m_changed, post_delete
from django.db.transaction import atomic
from django.dispatch import receiver
from django.templatetags.static import static
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)

file_system_storage_with_restricted_access = FileSystemStorageWithRestrictedAccess()

T = TypeVar("T")


def get_default_room_config() -> MeetingConfigurationTemplate:
    """
    provides the platform wide meeting configuration template
    """

    template, _ = MeetingConfigurationTemplate.objects.get_or_create(name="Default")
    return template


def get_default_room_config_id() -> int:
    """
    provides id of the platform wide meeting configuration template
    """

    return get_default_room_config().id


def _random_token() -> str:
    return get_random_string(length=32)


def _random_hex_token() -> str:
    return binascii.b2a_hex(os.urandom(12)).decode("utf-8")


def unique_slug_api_token(cls: ApiToken):
    slug = slugify(cls.name)
    while ApiToken.objects.filter(slug=slug).exists():
        slug = slugify(f"{slug}-{get_random_string(length=4)}")
    return slug


class UserManager(DjangoUserManager):
    def _create_user(self, username, email, password, tenant=None, **extra_fields):
        if isinstance(tenant, str):
            tenant = Site.objects.get_by_natural_key(tenant)
        return super()._create_user(username, email, password, tenant=tenant, **extra_fields)


class User(AbstractUser):
    class Meta:
        permissions = [
            ("moderate", _("Can moderate")),
        ]

    REQUIRED_FIELDS = ["tenant", *AbstractUser.REQUIRED_FIELDS]

    username = models.CharField(
        _("username"),
        max_length=511,
        unique=True,
        help_text=_("Required. 511 characters or fewer. Letters, digits and @/./+/-/_ only."),
        validators=[AbstractUser.username_validator],
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    theme = models.ForeignKey("Theme", on_delete=models.SET_NULL, null=True)
    personal_rooms_max_number = models.IntegerField(null=True, blank=True)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    tenant = models.ForeignKey(
        Site,
        on_delete=models.CASCADE,
        related_name="tenant_users",
        help_text=_("Association of users main tenant."),
    )
    # Application parameters
    bbb_auto_join_audio = models.BooleanField(
        default=True,
        help_text=_("If enabled, the process of joining the audio will automatically be started."),
    )
    bbb_listen_only_mode = models.BooleanField(
        default=True,
        help_text=_(
            "If enabled, joining the audio part of the meeting without a microphone will be possible, otherwise"
            " listen-only mode will be disabled. Disabling this will make it possible, in conjunction with skipping the"
            ' "echo test" prompt, to directly join a meeting without selecting further options.'
        ),
    )
    bbb_skip_check_audio = models.BooleanField(
        default=False, help_text=_('If enabled, the "echo test" prompt will not be shown when sharing audio.')
    )
    bbb_skip_check_audio_on_first_join = models.BooleanField(
        default=False,
        help_text=_(
            'If enabled, the "echo test" prompt will not be shown when sharing audio for the first time in the meeting.'
            ' If audio sharing will be stopped, next time trying to share the audio the "echo test" prompt will be'
            " displayed, allowing for configuration changes to be made prior to sharing audio again."
        ),
    )
    # Kurento parameters
    bbb_auto_share_webcam = models.BooleanField(
        default=False,
        help_text=_("If enabled, the process of sharing the webcam (if any) will automatically be started."),
    )
    bbb_record_video = models.BooleanField(default=True, help_text=_("If enabled, the video stream will be recorded."))
    bbb_skip_video_preview = models.BooleanField(
        default=False, help_text=_("If enabled, a preview of the webcam before sharing it will not be shown.")
    )
    bbb_skip_video_preview_on_first_join = models.BooleanField(
        default=False,
        help_text=_(
            "If enabled, a preview of the webcam before sharing it will not be shown when sharing for the first time in"
            " the meeting. If video sharing will be stopped, next time trying to share the webcam the video preview"
            " will be displayed, allowing for configuration changes to be made prior to sharing."
        ),
    )
    bbb_mirror_own_webcam = models.BooleanField(
        default=False,
        help_text=_(
            "If enabled, a mirrored version of the webcam will be shown."
            " Does not affect the incoming video streams for other users."
        ),
    )
    # Presentation parameters
    bbb_force_restore_presentation_on_new_events = models.BooleanField(
        default=False,
        help_text=_(
            "If enabled, the presentation area will be forcefully restored if it is minimized (viewers only) on the"
            " following actions: changing presentation/slide/zoom, publishing polls, or adding annotations."
        ),
    )
    # Layout parameters
    bbb_auto_swap_layout = models.BooleanField(
        default=False,
        help_text=_("If enabled, the presentation area will be minimized when joining a meeting."),
    )
    bbb_show_participants_on_login = models.BooleanField(
        default=True,
        help_text=_("If enabled, the participants panel will be displayed when joining a meeting."),
    )
    bbb_show_public_chat_on_login = models.BooleanField(
        default=True,
        help_text=_(
            "If enabled, the chat panel will be visible when joining a meeting."
            " The participants panel must also be shown to work."
        ),
    )
    favorite_rooms = models.ManyToManyField("Room", blank=True)

    objects = UserManager()

    def __str__(self) -> str:
        if self.display_name:
            return self.display_name
        elif self.first_name and self.last_name:
            return f"{self.last_name}, {self.first_name}"
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        elif self.email:
            return self.email
        return gettext("Unknown")

    def has_tenant_based_perm(self, perm: str, tenant: Site, obj: Any = None, *, elevate_staff: bool = True) -> bool:
        if self.is_superuser:
            return True

        if self.tenant != tenant:
            return False

        if elevate_staff and self.is_staff:
            return True

        return self.has_perm(perm, obj)

    def has_tenant_based_perms(
        self, perm_list: Iterable[str], tenant: Site, obj: Any = None, *, elevate_staff: bool = True
    ) -> bool:
        return all(self.has_tenant_based_perm(perm, tenant, obj, elevate_staff=elevate_staff) for perm in perm_list)

    def get_max_number_of_personal_rooms(self):
        if self.personal_rooms_max_number is not None:
            return self.personal_rooms_max_number
        else:
            general_parameters = GeneralParameter.objects.load(self.tenant)
            if self.has_tenant_based_perm("core.moderate", self.tenant):
                return general_parameters.personal_rooms_teacher_max_number
            else:
                return general_parameters.personal_rooms_non_teacher_max_number

    def get_theme(self) -> Theme:
        theme = self.theme
        if theme is None:
            theme = GeneralParameter.objects.load(self.tenant).default_theme
        return theme

    @property
    def homeroom(self) -> Optional[HomeRoom]:
        with atomic():
            general_parameter = GeneralParameter.objects.load(self.tenant)

            if Feature.HomeRooms(general_parameter).is_enabled() and (
                not general_parameter.home_room_teachers_only
                or self.has_tenant_based_perm("core.moderate", self.tenant)
            ):
                if hasattr(self, "homeroom_ptr"):
                    return self.homeroom_ptr
                else:
                    if (
                        general_parameter.home_room_scheduling_strategy is None
                        or general_parameter.home_room_room_configuration is None
                    ):
                        return None

                    logger.debug("Create homeroom for user %s", self.username)
                    try:
                        if self.email and not HomeRoom.objects.filter(name=f"home-{self.email}").exists():
                            room_name = f"home-{self.email}"
                        else:
                            room_name = f"home-{self.last_name}-{sha1(bytes(self.username, 'UTF-8')).hexdigest()[:8]}"

                        homeroom = HomeRoom.objects.create(
                            name=room_name,
                            owner=self,
                            scheduling_strategy=general_parameter.home_room_scheduling_strategy,
                            visibility=ROOM_VISIBILITY_PRIVATE,
                            default_meeting_configuration=general_parameter.home_room_room_configuration,
                        )
                        homeroom.tenants.add(self.tenant)
                        logger.debug(
                            "Home room created with the following parameters: "
                            + "name=%s owner=%s scheduling_strategy=%s is_public=%s default_meeting_configuration=%s",
                            room_name,
                            self.username,
                            general_parameter.home_room_scheduling_strategy,
                            False,
                            general_parameter.home_room_room_configuration,
                        )
                        return homeroom
                    except IntegrityError as e:
                        logger.warning("Home room could not be created. " + str(e))

        return None


class AgentConfiguration(models.Model):
    registration_interval = models.DurationField(
        null=True,
        help_text=_("Duration in seconds between periodic registration calls from agent"),
    )

    stats_interval = models.DurationField(
        null=True,
        help_text=_("Duration in seconds between periodic stats calls from agent"),
    )


class SchedulingStrategy(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    notifications_emails = models.TextField(null=True, blank=True)
    scheduling_strategy = models.CharField(
        max_length=255, choices=SCHEDULING_STRATEGIES, default=SCHEDULING_STRATEGY_LEAST_UTILIZATION
    )
    tenants = models.ManyToManyField(
        Site, help_text=_("Specification which tenant is able to see this scheduling strategy")
    )

    agent_configuration = models.OneToOneField(AgentConfiguration, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    def get_current_participant_count(self):
        return self.room_set.all().aggregate(Sum("participant_count"))["participant_count__sum"] or 0

    def get_utilization(self):
        usage = 0
        for server in self.get_servers_up():
            usage = usage + server.get_utilization()
        return ceil(usage)

    def get_utilization_max(self):
        return ceil(sum(map(lambda server: server.videostream_count_max, self.get_servers_up())))

    def get_utilization_percent(self):
        utilization_max = self.get_utilization_max()
        return 0 if utilization_max <= 0 else int(round(self.get_utilization() / utilization_max, 2) * 100)

    def _select_server_by_least_participants(self) -> Optional[Server]:
        return min(
            self.server_set.filter(state=SERVER_STATE_UP, server_types__name__in=["worker"]),
            key=lambda server: server.get_participant_count(),
            default=None,
        )

    def _select_server_by_least_utilization(self) -> Optional[Server]:
        return min(
            self.server_set.filter(state=SERVER_STATE_UP, server_types__name__in=["worker"]),
            key=lambda server: server.get_utilization(),
            default=None,
        )

    def _select_server_randomly_by_least_utilization(self) -> Optional[Server]:
        max_server_candidates = 5
        min_server_candidates_with_zero_utilization = 5

        server_utilization_list = [
            (server.get_utilization(), server)
            for server in self.server_set.filter(state=SERVER_STATE_UP, server_types__name__in=["worker"])
        ]

        if len(server_utilization_list) == 0:
            return None

        sorted_server_utilization_list = sorted(server_utilization_list, key=lambda entry: entry[0])

        servers_with_zero_utilization = [element[1] for element in sorted_server_utilization_list if element[0] == 0]

        if len(servers_with_zero_utilization) >= min_server_candidates_with_zero_utilization:
            return random.choice(servers_with_zero_utilization)
        else:
            return random.choice(sorted_server_utilization_list[:max_server_candidates])[1]

    def get_server_for_room(self) -> Optional[Server]:
        logger.info("Selecting new server for scheduling with strategy: {}".format(self.scheduling_strategy))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_PARTICIPANTS:
            select_server = self._select_server_by_least_participants
        elif self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_UTILIZATION:
            select_server = self._select_server_by_least_utilization
        elif self.scheduling_strategy == SCHEDULING_STRATEGY_RANDOM_PICK_FROM_LEAST_UTILIZED:
            select_server = self._select_server_randomly_by_least_utilization
        else:
            raise ValueError("Unknown scheduling strategy: " + self.scheduling_strategy)

        while True:
            server_to_be = select_server()

            if server_to_be is None or server_to_be.health_check() == SERVER_STATE_UP:
                break

        logger.info("Selected: {}".format(server_to_be))

        return server_to_be

    def get_servers_up(self):
        return self.server_set.filter(state=SERVER_STATE_UP)


class ApiToken(models.Model):
    name = models.CharField(max_length=250)
    secret = models.CharField(max_length=255, default=_random_hex_token)
    tenants = models.ManyToManyField(Site)
    scheduling_strategy = models.ForeignKey(SchedulingStrategy, on_delete=models.SET_NULL, null=True, blank=True)
    slug = models.SlugField(blank=True, max_length=255, editable=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = unique_slug_api_token(self)
        super(ApiToken, self).save(*args, **kwargs)


class AbstractMeetingConfiguration(models.Model):
    mute_on_start = models.BooleanField(default=True)
    moderation_mode = models.CharField(max_length=16, choices=MODERATION_MODE, default=MODERATION_MODE_MODERATORS)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=16, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    only_prompt_guests_for_access_code = models.BooleanField(default=False)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_unmuteusers = models.BooleanField(default=False)
    allow_ejectcam = models.BooleanField(default=False)
    allow_learningdashboard = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)
    dialNumber = models.CharField(max_length=255, null=True, blank=True)
    logoutUrl = models.CharField(max_length=255, null=True, blank=True)
    welcome_message = models.CharField(max_length=255, null=True, blank=True)
    maxParticipants = models.PositiveIntegerField(blank=True, null=True)
    streamingUrl = models.CharField(max_length=255, null=True, blank=True)
    meetingLayout = models.CharField(max_length=20, choices=MEETINGLAYOUT, default=MEETINGLAYOUT_SMART_LAYOUT)
    meeting_duration_time = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        abstract = True


class MeetingConfigurationTemplate(AbstractMeetingConfiguration):
    name = models.CharField(max_length=255, unique=True)
    tenants = models.ManyToManyField(
        Site, help_text=_("Specification which tenant is able to see and use this template")
    )

    def __str__(self):
        return self.name

    def instantiate(self, commit: bool = True) -> MeetingConfiguration:
        data = dict(
            (field.name, getattr(self, field.name))
            for field in AbstractMeetingConfiguration._meta.get_fields(True, True)
        )

        instance = MeetingConfiguration(**data)

        if commit:
            instance.save()

        return instance


class MeetingConfiguration(AbstractMeetingConfiguration):
    slides = models.ManyToManyField(
        "Slides",
        blank=True,
        verbose_name=_("Slides"),
        help_text=_("Slides to be pre-uploaded when a meeting will be started."),
    )
    initial_slides = models.ForeignKey(
        "Slides",
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
        verbose_name=_("Initial Slides"),
        help_text=_(
            "The initial slides to be displayed at the meeting start, "
            "if no others have been selected by the room or user."
        ),
    )

    @property
    def ordered_slides(self) -> Iterable["Slides"]:
        slides = self.slides.all()

        if self.initial_slides_id:
            slides = itertools.chain([self.initial_slides], slides.exclude(id=self.initial_slides_id))

        return slides

    def __str__(self):
        if hasattr(self, "meeting") and self.meeting.room is not None:
            return f"{self.meeting.room.name}-config-{self.meeting.started.isoformat()}"

        return super().__str__()


class ServerType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class DNSField(models.CharField):
    description = _("Sanitized DNS string in lower case and without trailing dot")

    def to_python(self, value):
        if isinstance(value, str):
            return self._sanitize(value)

        return value

    def get_prep_value(self, value):
        if isinstance(value, str):
            return self._sanitize(value)

    @staticmethod
    def _sanitize(raw: str) -> str:
        return raw.lower().rstrip(".")


class Server(models.Model):
    scheduling_strategy = models.ForeignKey(SchedulingStrategy, on_delete=models.SET_NULL, null=True)
    server_types = models.ManyToManyField(ServerType)
    dns = DNSField(max_length=255, unique=True)
    state = models.CharField(max_length=255, choices=SERVER_STATES, default=SERVER_STATE_WAITING_RESPONSE)
    datacenter = models.CharField(max_length=255, null=True, blank=True)
    shared_secret = models.CharField(max_length=255, null=True, blank=True)
    participant_count_max = models.PositiveIntegerField(default=250, validators=[MinValueValidator(1)])
    videostream_count_max = models.PositiveIntegerField(default=25, validators=[MinValueValidator(1)])
    machine_id = models.CharField(max_length=255, unique=True, null=True, blank=True)

    last_health_check = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = (
            "scheduling_strategy",
            "dns",
        )

    def __str__(self):
        return self.dns

    # quick and dirty util cals.
    def get_utilization(self):
        audio_video_factor = self.participant_count_max / self.videostream_count_max
        audio_usage = self.get_participant_count() / audio_video_factor
        video_usage = self.get_videostream_count()
        total_usage = audio_usage + video_usage

        return 0 if total_usage <= 0 else total_usage

    def get_utilization_percent(self):
        return int(round((self.get_utilization()) / self.videostream_count_max, 2) * 100)

    def health_check(self):
        logger.info("Health Check started for {}".format(self.dns))
        # skip disabled servers
        if self.state != SERVER_STATE_DISABLED:
            try:
                bbb = BigBlueButton(self.dns, self.shared_secret)
                api_version = bbb.validate_api(bbb.api())
                logger.debug("Validated api version for {} is {}".format(self.dns, api_version))
                self.state = SERVER_STATE_UP

            except Exception as e:
                logger.error("setting server state of {} to ERROR".format(self.dns) + str(e))
                self.state = SERVER_STATE_ERROR

            self.last_health_check = timezone.now()
            self.save()
        logger.info("Health Check finished for {}".format(self.dns))

        return self.state

    def get_participant_count(self):
        return (
            self.meeting_set.filter(state=MEETING_STATE_RUNNING).aggregate(Sum("participant_count"))[
                "participant_count__sum"
            ]
            or 0
        )

    def get_videostream_count(self):
        return (
            self.meeting_set.filter(state=MEETING_STATE_RUNNING).aggregate(Sum("videostream_count"))[
                "videostream_count__sum"
            ]
            or 0
        )


class HasTenantBasedPermFilter(Q):
    def __init__(
        self,
        perm: QuerySet[Permission],
        tenant: Site,
        *,
        elevate_staff: bool = True,
        prefix: str = "",
    ) -> None:
        if prefix and not prefix.endswith("__"):
            prefix = prefix + "__"

        elevate_staff_filter = Q(**{prefix + "is_staff": True}) if elevate_staff else Q()

        super().__init__(
            Q(**{prefix + "is_superuser": True})
            | (
                Q(**{prefix + "tenant": tenant})
                & (
                    elevate_staff_filter
                    | Q(**{prefix + "user_permissions__in": perm})
                    | Q(**{prefix + "groups__permissions__in": perm})
                )
            )
        )


class AvailableRoomManager(models.Manager):
    _tenant: Optional[Site] = None
    _general_parameter: Optional[GeneralParameter] = None
    _distinct: bool = False

    def init(self, tenant: Site, general_parameter: GeneralParameter, *, distinct: bool = True) -> AvailableRoomManager:
        clone = copy.copy(self)
        clone._tenant = tenant
        clone._general_parameter = general_parameter
        clone._distinct = distinct
        return clone

    def _room_filter(self, queryset: QuerySet[Room]) -> QuerySet[Room]:
        if not Feature.HomeRooms(self._general_parameter).is_enabled():
            queryset = queryset.exclude(models.Exists(HomeRoom.objects.filter(pk=models.OuterRef("pk"))))
        elif self._general_parameter.home_room_teachers_only:
            queryset = queryset.filter(
                Q(homeroom__isnull=True)
                | HasTenantBasedPermFilter(get_permissions("core.moderate"), self._tenant, prefix="homeroom__owner")
            )

        if not Feature.PersonalRooms(self._general_parameter).is_enabled():
            queryset = queryset.exclude(models.Exists(PersonalRoom.objects.filter(pk=models.OuterRef("pk"))))

        return queryset

    def _homeroom_filter(self, queryset: QuerySet[HomeRoom]) -> QuerySet[HomeRoom]:
        if not Feature.HomeRooms(self._general_parameter).is_enabled():
            queryset = queryset.none()
        elif self._general_parameter.home_room_teachers_only:
            queryset = queryset.filter(
                HasTenantBasedPermFilter(get_permissions("core.moderate"), self._tenant, prefix="owner")
            )

        return queryset

    def _personalroom_filter(self, queryset: QuerySet[PersonalRoom]) -> QuerySet[PersonalRoom]:
        if not Feature.PersonalRooms(self._general_parameter).is_enabled():
            queryset = queryset.none()

        return queryset

    def get_queryset(self) -> QuerySet[Room]:
        if self._general_parameter is None or self._tenant is None:
            raise ValueError(
                "init() must be called with the corresponding general_parameter"
                + " and tenant before executing queries."
            )

        queryset = super().get_queryset()

        if self._distinct:
            queryset = queryset.distinct()

        if self.model is Room:
            queryset = self._room_filter(queryset)
        elif self.model is HomeRoom:
            queryset = self._homeroom_filter(queryset)
        elif self.model is PersonalRoom:
            queryset = self._personalroom_filter(queryset)

        return queryset


class Room(models.Model):
    # Check on delete
    scheduling_strategy = models.ForeignKey(SchedulingStrategy, on_delete=models.PROTECT)
    name = models.CharField(max_length=255, unique=True)
    # Check on delete
    default_meeting_configuration = models.ForeignKey(
        MeetingConfigurationTemplate,
        default=get_default_room_config_id,
        on_delete=models.SET(get_default_room_config),
        verbose_name=_("Default meeting configuration"),
    )
    visibility = models.CharField(max_length=8, choices=ROOM_VISIBILITY, default=ROOM_VISIBILITY_PRIVATE)
    state = models.CharField(max_length=30, choices=ROOM_STATE, default=ROOM_STATE_INACTIVE)
    # TODO replace with internal secret to create tokens (secret not exposed to frontend and can be reset via button)
    direct_join_secret = models.CharField(max_length=255, blank=True)

    comment_public = models.CharField(max_length=255, null=True, blank=True)
    comment_private = models.CharField(max_length=255, null=True, blank=True)

    click_counter = models.PositiveIntegerField(default=0)

    event_collection_strategy = models.CharField(max_length=255, null=True, blank=True)
    event_collection_parameters = models.TextField(null=True, blank=True)

    last_meeting_creating = models.DateTimeField(null=True, blank=True)
    last_running = models.DateTimeField(null=True, blank=True)

    tenants = models.ManyToManyField(Site, help_text=_("Specification which tenant is able to see this room"))

    slides = models.ManyToManyField(
        "Slides",
        blank=True,
        verbose_name=_("Slides"),
        help_text=_("Slides to be pre-uploaded when a meeting will be started."),
    )
    initial_slides = models.ForeignKey(
        "Slides",
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
        verbose_name=_("Initial Slides"),
        help_text=_(
            "The initial slides to be displayed at the meeting start, "
            "if no others have been selected by the room or user."
        ),
    )

    class Meta:
        ordering = ("name",)

    objects = models.Manager()
    objects_available = AvailableRoomManager()

    def __str__(self):
        return self.name

    def get_participants_current(self):
        return (
            self.meeting_set.filter(state=MEETING_STATE_RUNNING).aggregate(Sum("participant_count"))[
                "participant_count__sum"
            ]
            or 0
        )

    @staticmethod
    def get_total_instantiation_counter():
        return Room.objects.all().aggregate(Sum("instantiation_counter"))["instantiation_counter__sum"] or 0

    def end_meeting(self):
        meeting = self.meeting_set.filter(state=MEETING_STATE_RUNNING).first()
        if meeting:
            bbb = BigBlueButton(meeting.server.dns, meeting.server.shared_secret)
            return bbb.end(meeting.meeting_id, meeting.moderator_pw)
        return False

    def is_room_only(self):
        is_subclass = self.is_homeroom() or self.is_personalroom()
        return not is_subclass

    def is_homeroom(self):
        return hasattr(self, "homeroom")

    def is_personalroom(self):
        return hasattr(self, "personalroom")

    def get_running_meeting(self) -> Optional[Meeting]:
        return self.meeting_set.filter_by_states([MEETING_STATE_RUNNING]).first()

    def get_unfinished_meeting(self) -> Optional[Meeting]:
        return self.meeting_set.filter_by_states([MEETING_STATE_CREATING, MEETING_STATE_RUNNING]).first()


class HomeRoom(Room):
    owner = models.OneToOneField(User, on_delete=models.CASCADE, null=False, related_name="homeroom_ptr")


class PersonalRoom(Room):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    co_owners = models.ManyToManyField(User, related_name="co_owners", blank=True)

    def has_co_owner(self, user: User):
        return user in self.co_owners.all()

    @staticmethod
    def co_owner_shall_not_contain_owner(**kwargs):
        action: str = kwargs["action"]

        if action == "pre_add":
            instance: PersonalRoom = kwargs["instance"]
            pk_set: set = kwargs["pk_set"]

            if instance.owner.id in pk_set:
                logger.warning("Prevented storing owner ({}) as co-owner for room {}.".format(instance.owner, instance))
                pk_set.remove(instance.owner.id)


m2m_changed.connect(PersonalRoom.co_owner_shall_not_contain_owner, sender=PersonalRoom.co_owners.through)


class RoomEvent(models.Model):
    uid = models.CharField(max_length=255, primary_key=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=True)
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return "{} – {}".format(self.room, self.name)

    class Meta:
        ordering = ("room",)


class GeneralParameterManager(models.Manager):
    use_in_migrations = True

    def load(self, tenant: Site) -> GeneralParameter:
        return self.get_or_create(
            tenant=tenant,
            defaults={
                "default_theme_id": Theme.objects.db_manager(self.db).order_by("id").values_list("id", flat=True)[:1]
            },
        )[0]


class GeneralParameter(models.Model):
    class Meta:
        permissions = [
            ("view_statistics", _("Can view statistics")),
        ]

    tenant = models.OneToOneField(
        Site,
        on_delete=models.CASCADE,
        help_text=_("Describes which tenant this general parameter is for"),
    )
    latest_news = models.TextField(
        null=True,
        blank=True,
        verbose_name="Latest news",
        help_text=_(
            "This text will be shown in a red alert box on the start page. "
            "You can use it to inform your users about critical news."
        ),
    )
    participant_total_max = models.PositiveIntegerField(default=0)
    jitsi_enable = models.BooleanField(
        default=False,
        verbose_name=_("Enable Jitsi ad-hoc meeting"),
        help_text=_(
            "If checked a button for ad-hoc meeting creation using Jitsi is enabled."
            " If enabled, please make sure to also set a target jisti server in the jits_url field"
        ),
    )
    jitsi_url = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        default="https://meet.jit.si/",
        verbose_name=_("JITSI URL"),
        help_text=_("Defines the target of the create ad-hoc meeting button on the start page."),
    )
    playback_url = models.CharField(max_length=255, blank=True, null=True)
    download_url = models.CharField(max_length=255, blank=True, null=True)
    faq_url = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("FAQ URL"),
        help_text=_("Defines the target of the read FAQ button on the start page."),
    )
    # TODO Check if still used. Dont think so....
    feedback_email = models.CharField(max_length=255, blank=True, null=True)
    footer = models.TextField(
        blank=True, null=True, verbose_name=_("Footer"), help_text=_("Defines the footer text of the application")
    )
    logo_link = models.CharField(
        max_length=510,
        blank=True,
        null=True,
        verbose_name=_("Logo URL"),
        help_text=_("The image behind this link will be used as logo on this application."),
    )
    favicon_link = models.CharField(
        max_length=510,
        blank=True,
        null=True,
        verbose_name=_("FAV Icon URL"),
        help_text=_("The image behind this link will be used as fav icon on this application."),
    )
    page_title = models.CharField(
        max_length=510,
        blank=True,
        null=True,
        default="bbb@scale | Virtual Rooms",
        verbose_name=_("Page Title"),
        help_text=_("Define the title of this application."),
    )
    app_title = models.CharField(
        max_length=255,
        default="Virtual Rooms",
        verbose_name=_("App Title"),
        help_text=_("Define the application title showed beside the logo."),
    )
    home_room_enabled = models.BooleanField(
        default=False,
        verbose_name=_("Enable home room"),
        help_text=_(
            "If checked the home room feature is enabled and users "
            "have access to their personal room via a button on the start page."
        ),
    )
    home_room_teachers_only = models.BooleanField(
        default=True,
        verbose_name=_("Teachers only"),
        help_text=_("If checked only teachers are allowed to access their private rooms."),
    )
    home_room_scheduling_strategy = models.ForeignKey(
        SchedulingStrategy,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Scheduling Strategy"),
        help_text=_("Define which scheduling strategy will be used for new home rooms."),
    )
    home_room_room_configuration = models.ForeignKey(
        MeetingConfigurationTemplate,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Room configuration"),
        help_text=_("Define which room configuration will be used for new home rooms."),
    )
    home_room_last_used_config_enabled = models.BooleanField(
        default=True,
        verbose_name=_("Allow to start home room with last used configuration"),
        help_text=_("If checked the users can start their home room with their last used configuration."),
    )
    default_theme = models.ForeignKey(
        "Theme",
        on_delete=models.PROTECT,
        verbose_name=_("Default Theme"),
        help_text=_(
            "Define which theme should be the default theme for users "
            + "who have not changed their theme yet or for non-logged-in users."
        ),
    )
    personal_rooms_enabled = models.BooleanField(
        default=False,
        verbose_name=_("Enable personal room"),
        help_text=_("If checked the personal room feature is enabled and users can create personal rooms"),
    )
    personal_rooms_scheduling_strategy = models.ForeignKey(
        SchedulingStrategy,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="personal_room_scheduling_strategy",
        verbose_name=_("Personal Room Scheduling Strategy"),
        help_text=_("Define which scheduling strategy will be used for personal rooms."),
    )
    personal_rooms_teacher_max_number = models.IntegerField(
        default=0,
        verbose_name=_("Teacher max number"),
        help_text=_(
            "Defines the maximum number of rooms a teacher can "
            "create. Can be overwritten on a per user basis using "
            "the user field."
        ),
    )
    personal_rooms_non_teacher_max_number = models.IntegerField(
        default=0,
        verbose_name=_("Non-teacher max number"),
        help_text=_(
            "Defines the maximum number of rooms a non-teacher "
            "can create. "
            "Can be overwritten on a per user basis "
            "using the user field."
        ),
    )
    enable_occupancy = models.BooleanField(
        default=True,
        verbose_name=_("Enable occupancy columns"),
        help_text=_('If checked the columns "Current/Next Occupancy" are enabled on the front page'),
    )
    enable_recordings = models.BooleanField(
        default=True,
        verbose_name=_("Enables recordings for tenant"),
        help_text=_("If checked the recordings option will show up while starting a room"),
    )
    enable_adminunmute = models.BooleanField(
        default=False,
        verbose_name=_("Enables user unmute by moderator for tenant"),
        help_text=_("If checked the allow mod to unmute option will show up while starting a room"),
    )

    hide_statistics_enable = models.BooleanField(
        default=False,
        verbose_name=_("Hide statistics page from anonymous users"),
        help_text=_("If checked the statistics page will be hidden from anonymous users."),
    )

    hide_statistics_logged_in_users = models.BooleanField(
        default=False,
        verbose_name=_("Hide statistics page from logged in users"),
        help_text=_("If checked the statistics page will be hidden from logged in users."),
    )

    show_personal_rooms = models.BooleanField(
        default=True,
        verbose_name=_("Show personal rooms on frontpage"),
        help_text=_("If enabled, the personal rooms tab will be shown on the frontpage"),
    )

    show_home_rooms = models.BooleanField(
        default=True,
        verbose_name=_("Show home rooms on frontpage"),
        help_text=_("If enabled, the home rooms tab will be shown on the frontpage"),
    )

    media_enabled = models.BooleanField(
        default=False,
        verbose_name=_("Enable media upload"),
        help_text=_("If enabled, media files like slides can be uploaded and used (e.g. for pre-uploading slides)."),
    )
    moderators_can_upload_slides = models.BooleanField(default=True, verbose_name=_("Moderators can upload slides"))
    slides_max_upload_size = models.PositiveBigIntegerField(
        default=100_000_000,
        verbose_name=_("Max upload size of slides"),
        help_text=_("A number with an optional decimal part followed by B, MB, GB, MiB, GiB, and so on."),
    )
    slides = models.ManyToManyField(
        "Slides",
        blank=True,
        verbose_name=_("Slides"),
        help_text=_("Slides to be pre-uploaded when a meeting will be started."),
    )
    initial_slides = models.ForeignKey(
        "Slides",
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
        verbose_name=_("Initial Slides"),
        help_text=_(
            "The initial slides to be displayed at the meeting start, "
            "if no others have been selected by the room or user."
        ),
    )

    recording_management_url = models.CharField(
        help_text=_("Management url+port for recordings GRPC service."),
        blank=True,
        max_length=255,
        verbose_name=_("Recording management url"),
    )
    recording_cert = models.TextField(
        blank=True,
        help_text=_("Client cert for authentication with recordings GRPC service."),
        verbose_name=_("Recording client certificate"),
    )
    recording_key = models.TextField(
        blank=True,
        help_text=_("Client key for communication with recordings GRPC service."),
        verbose_name=_("Recording client key"),
    )
    recording_deletion_period = models.IntegerField(
        default=72,
        help_text=_(
            "Time period in hours until the recording is deleted after marking. "
            "Does not affect already marked recordings."
        ),
        verbose_name=_("Recording deletion period"),
    )
    enable_meeting_dial_number = models.BooleanField(
        default=False,
        verbose_name=_("Enable passing a dial number"),
        help_text=_("If checked a dial number can be passed to BBB while configuring/creating a meeting."),
    )
    enable_meeting_rtmp_streaming = models.BooleanField(
        default=False,
        verbose_name=_("Enable the possibility for rtmp streaming"),
        help_text=_("If checked a rtmp streaming url can be passed to BBB while configuring/creating a meeting."),
    )

    objects = GeneralParameterManager()

    def __str__(self) -> str:
        return self.__class__.__name__ + "(tenant=" + repr(self.tenant.name) + ")"


class MeetingQuerySet(models.QuerySet):
    def for_deletion(self) -> QuerySet[Meeting]:
        return self.filter(to_delete__lte=timezone.now())

    def filter_by_states(self, states: Iterable[MEETING_STATE]) -> QuerySet[Meeting]:
        return self.filter(state__in=states)


class Meeting(models.Model):
    objects = MeetingQuerySet.as_manager()
    # Room reference
    room: Optional[Room] = models.ForeignKey(Room, on_delete=models.SET_NULL, null=True, blank=True)
    room_name = models.CharField(max_length=255, blank=True)

    id = models.UUIDField(unique=True, editable=False, default=uuid.uuid4, primary_key=True)

    server = models.ForeignKey(Server, on_delete=models.SET_NULL, null=True, blank=True)
    state = models.CharField(max_length=30, choices=MEETING_STATE, default=MEETING_STATE_FINISHED)
    tenants = models.ManyToManyField(
        Site, help_text=_("Specification which tenant is able to interact with this meeting")
    )
    external_meeting = models.ForeignKey("ExternalMeeting", null=True, on_delete=models.SET_NULL)

    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    creator_name = models.CharField(max_length=255, blank=True)
    replay_id = models.CharField(max_length=255, blank=True, default="")
    replay_url = models.URLField(max_length=511, blank=True, default="")
    replay_title = models.CharField(max_length=255, blank=True, default="")
    participant_count = models.PositiveIntegerField(default=0)
    videostream_count = models.PositiveIntegerField(default=0)

    configuration = models.OneToOneField(MeetingConfiguration, on_delete=models.SET_NULL, null=True, blank=True)

    attendee_pw = models.CharField(max_length=255, default=_random_token)
    moderator_pw = models.CharField(max_length=255, default=_random_token)

    started = models.DateTimeField(auto_now_add=True)
    last_health_check = models.DateTimeField(default=timezone.now)
    to_delete = models.DateTimeField(null=True)

    # BBB parameter
    is_breakout = models.BooleanField(default=False)
    bbb_meeting_id = models.CharField(max_length=255, blank=True)
    published = models.BooleanField(default=True)

    def __str__(self) -> str:
        creator_username_or_name = (
            self.creator.username if self.creator_id is not None else (self.creator_name or "anonymous")
        )
        return f"{self.started.date()}-{self.room_name}-{creator_username_or_name}"

    class Meta:
        ordering = ("-started",)
        permissions = [
            ("view_recording", _("Can view recording")),
            ("create_meeting", _("Can create (start) meeting")),
            ("join_meeting", _("Can join (any non-finished) meeting")),
        ]

    @property
    def creator_display_name(self) -> str:
        return str(self.creator) if self.creator_id is not None else self.creator_name

    def delete(self, *args, **kwargs):
        if not self.is_breakout and self.configuration:
            self.configuration.delete()
        super(Meeting, self).delete(*args, **kwargs)

    def get_recordings_xml(self):
        if self.replay_url:
            return meeting_recording_xml(self.replay_url)
        return None

    def delete_recording(self):
        self.replay_id = ""
        self.replay_url = ""
        self.to_delete = None
        self.save()

    def mark_for_deletion(self, to_delete: datetime):
        self.__set_marker_for_deletion(to_delete)
        logger.debug(f"recording with id {self.id} was marked for deletion with datetime {to_delete}")

    def unmark_for_deletion(self):
        self.__set_marker_for_deletion(None)
        logger.debug(f"recording with id {self.id} was unmarked for deletion")

    def __set_marker_for_deletion(self, to_delete: Optional[datetime]):
        self.to_delete = to_delete
        self.save()

    def get_zip_url(self):
        return urljoin(f"{self.replay_url}/", "zip")


class ExternalMeeting(models.Model):
    name = models.CharField(max_length=255)
    external_meeting_id = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Theme(models.Model):
    name = models.CharField(max_length=255, unique=True)
    base_stylesheet_static_path = models.TextField(null=True)
    multi_select_stylesheet_static_path = models.TextField(null=True)

    def __str__(self) -> str:
        return self.name

    def __getattr__(self, element: str) -> Optional[str]:
        try:
            static_path = self.__getattribute__(element + "_static_path")
        except AttributeError as exc:
            exc.args = (exc.args[0].replace("_static_path", ""), *exc.args[1:])
            raise

        if static_path is None:
            return None

        return static(static_path)


class Webhook(models.Model):
    name = models.CharField(max_length=255, unique=True, help_text=_("Free-text name field"))

    url = models.CharField(max_length=255, help_text=_("The webhook receiver's URL"))

    event = models.CharField(
        max_length=255,
        choices=WEBHOOKS,
        help_text=_("The event this Webhook is triggered on"),
    )

    enabled = models.BooleanField(
        default=True,
        help_text=_("Whether this Webhook is active"),
    )

    secret = models.CharField(
        max_length=255,
        help_text=_(
            "Hex-encoded secret for authenticating webhooks. "
            "Used to compute a HMAC-SHA512 over the request body and a timestamp. "
            "The HMAC (but not the secret!) is included with the request in "
            "the X-Hook-Signature header."
        ),
    )

    timeout = models.DurationField(
        default=timedelta(seconds=10),
        help_text=_("Set a timeout for the webhook request to complete"),
    )

    num_retries = models.IntegerField(
        default=3,
        help_text=_("Retry failed execution this many times"),
    )

    def __str__(self) -> str:
        return self.name


def slides_file_path(_instance, filename):
    return f"slides/{get_random_string(length=32)}{Path(filename).suffix}"


class Slides(models.Model):
    class Meta:
        verbose_name = _("slides")
        verbose_name_plural = _("slides")
        permissions = [
            ("add_own_slides", _("Can add own slides")),
            ("change_own_slides", _("Can change own slides")),
            ("delete_own_slides", _("Can delete own slides")),
            ("view_own_slides", _("Can view own slides")),
        ]

    name = models.CharField(max_length=255, help_text=_("The name of these slides"))
    file = models.FileField(upload_to=slides_file_path, storage=file_system_storage_with_restricted_access)
    tenant = models.ForeignKey(Site, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, related_name="owned_slides", blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.name


@receiver(post_delete, sender=Slides)
def mark_slides_file_for_removal(*_, instance: Slides = None, path: str = None, **__):
    if path is None and instance and instance.file:
        path = instance.file.path

    if path:
        os.rename(path, path + ".delete")
        logger.info(f"Marked slides file {os.path.relpath(path, settings.MEDIA_ROOT)} for removal")
