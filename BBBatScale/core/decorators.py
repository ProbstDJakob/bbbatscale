from functools import wraps
from typing import Callable, Iterable, Union
from urllib.parse import urlparse

from core.utils import get_tenant
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.exceptions import PermissionDenied
from django.shortcuts import resolve_url


def tenant_based_permission_required(
    perm: Union[str, Iterable[str]],
    login_url=None,
    redirect_field_name=REDIRECT_FIELD_NAME,
    raise_exception=False,
    *,
    elevate_staff=True,
) -> Callable:
    if isinstance(perm, str):
        perm_list = (perm,)
    else:
        perm_list = perm

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if hasattr(request.user, "has_tenant_based_perms") and request.user.has_tenant_based_perms(
                perm_list, get_tenant(request), elevate_staff=elevate_staff
            ):
                return view_func(request, *args, **kwargs)

            # In case the 403 handler should be called raise the exception
            if raise_exception:
                raise PermissionDenied

            path = request.build_absolute_uri()
            resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if (not login_scheme or login_scheme == current_scheme) and (
                not login_netloc or login_netloc == current_netloc
            ):
                path = request.get_full_path()
            from django.contrib.auth.views import redirect_to_login

            return redirect_to_login(path, resolved_login_url, redirect_field_name)

        return _wrapped_view

    return decorator
