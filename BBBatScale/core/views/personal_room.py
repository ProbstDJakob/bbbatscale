import logging

from core.feature_flag import Feature
from core.forms import PersonalRoomForm
from core.models import PersonalRoom
from core.utils import get_general_parameter, get_tenant
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@Feature.PersonalRooms.is_enabled
@login_required
def personal_room_create(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))
    form = PersonalRoomForm(
        request.POST or None,
        tenant=get_tenant(request),
        general_parameter=get_general_parameter(request),
        requesting_user=request.user,
        initial={
            "owner": request.user,
            "scheduling_strategy": get_general_parameter(request).personal_rooms_scheduling_strategy,
            "tenants": [get_tenant(request)],
        },
    )
    if request.method == "POST" and form.is_valid():
        _personal_room = form.save()
        messages.success(request, _("Personal room {} was created successfully.").format(_personal_room.name))
        # return to URL
        return redirect("home")
    return render(request, "personal_rooms_create.html", {"form": form})


@Feature.PersonalRooms.is_enabled
@login_required
def personal_room_update(request: HttpRequest, personal_room: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, personal_room))

    instance = get_object_or_404(
        PersonalRoom.objects.filter(
            Q(pk=personal_room) & (Q(owner=request.user) | Q(co_owners__in=[request.user]))
        ).distinct()
    )

    form = PersonalRoomForm(
        request.POST or None,
        instance=instance,
        tenant=get_tenant(request),
        general_parameter=get_general_parameter(request),
        requesting_user=request.user,
    )
    if request.method == "POST" and form.is_valid():
        _room = form.save()
        messages.success(request, _("Personal room {} was updated successfully.").format(_room.name))
        return redirect("home")
    return render(request, "personal_rooms_create.html", {"form": form})


@Feature.PersonalRooms.is_enabled
@login_required
def personal_room_delete(request: HttpRequest, personal_room: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, personal_room))

    instance: PersonalRoom = get_object_or_404(PersonalRoom, pk=personal_room)
    # TODO: check with has_tenant_based_perm
    if instance.owner == request.user or request.user.is_superuser:
        instance.delete()
        messages.success(request, _("Personal room was deleted successfully."))
    else:
        messages.error(request, _("Only owner can delete a personal room!"))
        logger.warning("{} has tried to delete room {} (not possible through UI)".format(request.user, instance.owner))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))
