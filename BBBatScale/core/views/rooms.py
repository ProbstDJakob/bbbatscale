import logging

from core.constants import MEETING_STATE_CREATING, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE
from core.decorators import tenant_based_permission_required
from core.forms import RoomForm
from core.models import MeetingConfigurationTemplate, Room, SchedulingStrategy
from core.services import end_meeting
from core.utils import get_general_parameter, get_tenant, join_or_create_url, paginate
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.timesince import timesince
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_room", raise_exception=True)
def rooms_overview(request):
    logger.info(create_view_access_logging_message(request))

    return render(
        request,
        "rooms_overview.html",
        {
            "scheduling_strategies": SchedulingStrategy.objects.filter(tenants__in=[get_tenant(request)]),
            "configurations": MeetingConfigurationTemplate.objects.filter(tenants__in=[get_tenant(request)]),
        },
    )


@login_required
@tenant_based_permission_required("core.add_room", raise_exception=True)
def room_create(request):
    logger.info(create_view_access_logging_message(request))
    form = RoomForm(
        request.POST or None,
        tenant=get_tenant(request),
        general_parameter=get_general_parameter(request),
        requesting_user=request.user,
        initial={"tenants": [get_tenant(request)]},
    )
    if request.method == "POST" and form.is_valid():
        _room = form.save()
        messages.success(request, _("Room {} was created successfully.").format(_room.name))
        # return to URL
        return redirect("rooms_overview")
    return render(request, "rooms_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_room", raise_exception=True)
def room_delete(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    instance.delete()
    messages.success(request, _("Room was deleted successfully."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@tenant_based_permission_required(["core.view_room", "core.change_room"], raise_exception=True)
def room_update(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    form = RoomForm(
        request.POST or None,
        tenant=get_tenant(request),
        general_parameter=get_general_parameter(request),
        requesting_user=request.user,
        instance=instance,
    )
    if request.method == "POST" and form.is_valid():
        _room = form.save()
        messages.success(request, _("Room {} was updated successfully.").format(_room.name))
        return redirect("rooms_overview")
    return render(request, "rooms_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.change_room", raise_exception=True)
def force_end_meeting(request, room_pk):
    logger.info(create_view_access_logging_message(request, room_pk))

    room = get_object_or_404(Room, pk=room_pk)
    if room.end_meeting():
        messages.success(request, _("Meeting in {} successfully ended.").format(room.name))
        logger.debug("Meeting in {} successfully ended.".format(room.name))
    else:
        messages.error(request, _("Error while trying to end meeting in {}.").format(room.name))
        logger.debug("Error while trying to end meeting in {}.".format(room.name))
    return redirect("rooms_overview")


@login_required
@tenant_based_permission_required("core.change_room", raise_exception=True)
def room_reset_stucked(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    if instance.state == ROOM_STATE_CREATING:
        meeting = instance.meeting_set.filter(state=MEETING_STATE_CREATING).first()
        if meeting:
            end_meeting(meeting)
        else:
            instance.state = ROOM_STATE_INACTIVE
            instance.last_running = None
            instance.save()
        logger.debug("Room ({}) was in state 'creating' and was manual reset by {}.".format(room, request.user))
    else:
        logger.debug("Room ({}) was not in state 'creating' and will not be reset.".format(room))

    return redirect("rooms_overview")


def map_room(room: Room) -> dict:
    return {
        "updateUrl": reverse("room_update", args=[room.id]),
        "deleteUrl": reverse("room_delete", args=[room.id]),
        "forceEndMeetingUrl": reverse("force_end_meeting", args=[room.id]),
        "roomResetStuckUrl": reverse("room_reset_stucked", args=[room.id]),
        "createOrJoinUrl": join_or_create_url(room),
        "name": room.name,
        "state": room.state,
        "tenants": list(room.tenants.all().values_list("name", flat=True)),
        "schedulingStrategy": room.scheduling_strategy.name if room.scheduling_strategy else None,
        "configuration": room.default_meeting_configuration.name if room.default_meeting_configuration else None,
        "visibility": room.visibility,
        "lastRunning": timesince(room.last_running) if room.last_running else None,
        "clicks": room.click_counter,
        "comment": room.comment_private,
        "secret": room.direct_join_secret if room.direct_join_secret else None,
    }


@login_required
@tenant_based_permission_required("core.view_room", elevate_staff=False, raise_exception=True)
def get_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_tenant(request)
    query = Room.objects.filter(tenants__in=[tenant]).order_by("name")

    search_filter = request.GET.get("filter-search")
    if search_filter:
        query = query.filter(Q(name__icontains=search_filter))

    config_filter = request.GET.get("filter-config")
    if config_filter:
        query = query.filter(default_meeting_configuration=config_filter)

    scheduling_strategy_filter = request.GET.get("filter-strategy")
    if scheduling_strategy_filter:
        query = query.filter(scheduling_strategy=scheduling_strategy_filter)

    return paginate(query, int(request.GET.get("page", 1)), map_room)
