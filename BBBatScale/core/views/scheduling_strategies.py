import logging

from core.decorators import tenant_based_permission_required
from core.forms import AgentConfigurationForm, SchedulingStrategyForm
from core.models import SchedulingStrategy
from core.utils import paginate
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_schedulingstrategy", elevate_staff=False, raise_exception=True)
def scheduling_strategy_overview(request):
    logger.info(create_view_access_logging_message(request))

    return render(request, "scheduling_strategy_overview.html")


@login_required
@tenant_based_permission_required("core.add_schedulingstrategy", elevate_staff=False, raise_exception=True)
def scheduling_strategy_create(request):
    logger.info(create_view_access_logging_message(request))

    form = SchedulingStrategyForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        scheduling_strategy = form.save()

        messages.success(request, _("Scheduling Strategy was created successfully."))
        logger.debug("Scheduling Strategy (%s) was created successfully.", scheduling_strategy)
        # return to URL
        return redirect("scheduling_strategy_overview")

    logger.debug("Scheduling Strategy (%s) was not updated correctly; is_valid=%s", form.is_valid())
    return render(request, "scheduling_strategy_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_schedulingstrategy", elevate_staff=False, raise_exception=True)
def scheduling_strategy_delete(request, scheduling_strategy):
    logger.info(create_view_access_logging_message(request, scheduling_strategy))

    instance = get_object_or_404(SchedulingStrategy, pk=scheduling_strategy)
    instance.delete()
    messages.success(request, _("Scheduling Strategy was deleted successfully."))
    logger.debug("Scheduling Strategy (%s) was deleted successfully", scheduling_strategy)

    return redirect("scheduling_strategy_overview")


@login_required
@tenant_based_permission_required(
    ["core.view_schedulingstrategy", "core.change_schedulingstrategy"], elevate_staff=False, raise_exception=True
)
def scheduling_strategy_update(request, scheduling_strategy):
    logger.info(create_view_access_logging_message(request, scheduling_strategy))

    instance = get_object_or_404(SchedulingStrategy, pk=scheduling_strategy)
    form = SchedulingStrategyForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _scheduling_strategy = form.save()
        messages.success(request, _("Scheduling Strategy was updated successfully."))
        logger.debug("Scheduling Strategy (%s) was updated successfully", _scheduling_strategy)

        return redirect("scheduling_strategy_overview")

    logger.debug("Scheduling Strategy (%s) was NOT updated successfully", instance)
    return render(request, "scheduling_strategy_create.html", {"form": form})


@login_required
@tenant_based_permission_required(
    ["core.change_schedulingstrategy", "core.view_agentconfiguration", "core.change_agentconfiguration"],
    elevate_staff=False,
    raise_exception=True,
)
def agent_configuration_update(request, scheduling_strategy):
    logger.info(create_view_access_logging_message(request, scheduling_strategy))

    scheduling_strategy = get_object_or_404(SchedulingStrategy, pk=scheduling_strategy)

    form = AgentConfigurationForm(
        request.POST or None, instance=scheduling_strategy.agent_configuration, scheduling_strategy=scheduling_strategy
    )

    if request.method == "POST" and form.is_valid():
        agent_config = form.save()

        messages.success(request, _("Scheduling Strategy was updated successfully."))
        logger.debug("Scheduling Strategy (%s) was updated successfully", agent_config)

        return redirect("scheduling_strategy_overview")

    return render(request, "scheduling_strategy_agent_configuration_create.html", {"form": form})


def map_scheduling_strategy(scheduling_strategy: SchedulingStrategy) -> dict:
    return {
        "updateUrl": reverse("scheduling_strategy_update", args=[scheduling_strategy.id]),
        "deleteUrl": reverse("scheduling_strategy_delete", args=[scheduling_strategy.id]),
        "agentConfigUrl": reverse("agent_configuration_update", args=[scheduling_strategy.id]),
        "name": scheduling_strategy.name,
        "strategy": scheduling_strategy.get_scheduling_strategy_display(),
        "tenants": list(scheduling_strategy.tenants.all().values_list("name", flat=True)),
        "description": scheduling_strategy.description,
        "emailNotifications": scheduling_strategy.notifications_emails,
    }


@login_required
@tenant_based_permission_required("core.view_schedulingstrategy", elevate_staff=False, raise_exception=True)
def get_scheduling_strategies(request: HttpRequest) -> HttpResponse:
    query = SchedulingStrategy.objects.order_by("name")

    name_filter = request.GET.get("filter-search")
    if name_filter:
        query = query.filter(Q(name__icontains=name_filter))

    return paginate(query, int(request.GET.get("page", 1)), map_scheduling_strategy)
