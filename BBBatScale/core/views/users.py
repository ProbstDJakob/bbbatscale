import logging
from functools import partial

from core.decorators import tenant_based_permission_required
from core.forms import PasswordChangeForm, SetPasswordForm, UserForm, UserFormWithPassword, UserSettings
from core.models import User
from core.utils import get_tenant, has_tenant_based_perm, paginate
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.db.models import Q
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_user", raise_exception=True)
def users_overview(request):
    logger.info(create_view_access_logging_message(request))
    return render(request, "users_overview.html")


@login_required
@tenant_based_permission_required("core.add_user", raise_exception=True)
def user_create(request):
    logger.info(create_view_access_logging_message(request))

    form = UserFormWithPassword(request.POST or None, initial={"tenant": get_tenant(request)})
    if request.method == "POST" and form.is_valid():
        user = form.save()
        messages.success(request, _("User {} successfully created.").format(user.username))
        # return to URL
        return redirect("users_overview")
    return render(request, "user_settings.html", {"form": form, "administration": True})


@login_required
@tenant_based_permission_required("core.delete_user", raise_exception=True)
def user_delete(request, user):
    logger.info(create_view_access_logging_message(request, user))

    instance = get_object_or_404(User, pk=user)
    instance.delete()
    messages.success(request, _("User successfully deleted."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@tenant_based_permission_required(["core.view_user", "core.change_user"], raise_exception=True)
def user_update(request, user):
    logger.info(create_view_access_logging_message(request, user))

    instance = get_object_or_404(User, pk=user)

    form = UserForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            _user = form.save()
            messages.success(request, _("User {} successfully updated").format(_user.username))
            return redirect("users_overview")
    return render(request, "user_settings.html", {"form": form, "administration": True})


@login_required
def user_settings(request):
    logger.info(create_view_access_logging_message(request))

    form = UserSettings(request.POST or None, instance=request.user)
    if request.method == "POST":
        if form.is_valid():
            _user = form.save()
            messages.success(request, _("User settings successfully updated").format(_user.username))
            return redirect("user_settings")
    return render(request, "user_settings.html", {"form": form})


@login_required
def user_change_password(request):
    if not request.user.has_usable_password():
        raise Http404

    logger.info(create_view_access_logging_message(request))

    form = PasswordChangeForm(request.user, request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        messages.success(request, _("Password successfully changed."))
        return redirect("home")

    return render(request, "password_change.html", {"form": form})


@login_required
@tenant_based_permission_required(["core.view_user", "core.change_user"], raise_exception=True)
def user_change_password_admin(request, user):
    instance = get_object_or_404(User, pk=user)
    if not instance.has_usable_password():
        raise Http404

    logger.info(create_view_access_logging_message(request, user))
    form = SetPasswordForm(instance, request.POST or None)
    if request.method == "POST" and form.is_valid():
        _user = form.save()
        messages.success(request, _("User {} successfully changed password").format(_user.username))
        return redirect("users_overview")
    return render(request, "user_settings.html", {"form": form, "administration": True})


@login_required
def user_search_json(request):
    logger.info(create_view_access_logging_message(request))

    q = request.GET.get("q")
    users = []
    if q and len(q) >= 2:
        for user in User.objects.filter(
            Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(email__icontains=q),
            tenant=get_tenant(request),
        ):
            users.append({"id": user.id, "display_name": str(user)})
    return JsonResponse({"users": users})


def map_user(tenant: Site, user: User) -> dict:
    return {
        "updateUrl": reverse("user_update", args=[user.id]),
        "deleteUrl": reverse("user_delete", args=[user.id]),
        "changePassword": reverse("user_reset_password_admin", args=[user.id]),
        "hasUsablePassword": user.has_usable_password(),
        "username": user.username,
        "firstName": user.first_name,
        "lastName": user.last_name,
        "email": user.email,
        "isStaff": user.is_staff,
        "isModerator": has_tenant_based_perm(user, "core.moderate", tenant),
    }


@login_required
@tenant_based_permission_required("core.view_user", raise_exception=True)
def get_users(request: HttpRequest) -> HttpResponse:
    tenant = get_tenant(request)
    query = User.objects.filter(tenant=tenant).order_by("username")

    name_filter = request.GET.get("filter-search")
    if name_filter:
        query = query.filter(
            Q(username__icontains=name_filter)
            | Q(first_name__icontains=name_filter)
            | Q(last_name__icontains=name_filter)
        )

    return paginate(query, int(request.GET.get("page", 1)), partial(map_user, tenant))
