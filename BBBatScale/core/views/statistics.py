import logging

from core.models import Meeting, Room, SchedulingStrategy, Server
from core.utils import get_general_parameter, get_tenant, has_tenant_based_perm
from core.views import create_view_access_logging_message
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.shortcuts import render

logger = logging.getLogger(__name__)


def statistics(request):
    general_parameter = get_general_parameter(request)
    tenant = get_tenant(request)

    if general_parameter.hide_statistics_enable:
        if not request.user.is_authenticated:
            return redirect_to_login(request.build_absolute_uri())
        elif general_parameter.hide_statistics_logged_in_users and not has_tenant_based_perm(
            request.user, "core.view_statistics", tenant, elevate_staff=False
        ):
            raise PermissionDenied

    logger.info(create_view_access_logging_message(request))
    return render(
        request,
        "statistics.html",
        {
            "servers": Server.objects.filter(scheduling_strategy__tenants__in=[tenant]),
            "rooms": Room.objects.filter(tenants__in=[tenant]),
            "participants_current": Room.get_participants_current(tenant),
            "performed_meetings": Meeting.objects.all().count(),
            "scheduling_strategies": SchedulingStrategy.objects.filter(tenants__in=[tenant]),
        },
    )
