import dataclasses
import json
import logging
from abc import ABC, abstractmethod
from typing import Any, Dict, List, Optional, Type, Union

from core.exceptions import BaseHttpException
from core.forms import ConfigureMeetingForm
from core.models import GeneralParameter, Meeting, Room, User
from core.services import join_or_create, meeting_create
from core.services.join_or_create import (
    Action,
    ConfigureMeetingAction,
    CreateJoinToken,
    IntermediateAction,
    JsonType,
    PromptAccessCodeAction,
    PromptJoinNameAction,
    RedirectAction,
    WaitUntilConfiguredAction,
    WaitUntilRunningAction,
    get_next_action,
    is_user_moderator,
)
from core.utils import get_general_parameter, get_tenant, join_or_create_url
from core.views import view_access_logging_decorator
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from django.core import signing
from django.core.signing import BadSignature, SignatureExpired
from django.db import transaction
from django.db.transaction import atomic, non_atomic_requests
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView, TemplateView

logger = logging.getLogger(__name__)

_empty = object()


class JoinCreateException(BaseHttpException):
    def __init__(self, error_type: str, status: int, reason: str) -> None:
        self._error_type = error_type
        self._status = status
        self._reason = reason

    def response(self, request: HttpRequest) -> JsonResponse:
        return JsonResponse(
            {
                "type": "error",
                "error": self._error_type,
                "reason": self._reason,
            },
            status=self._status,
        )


class InvalidContentTypeException(JoinCreateException):
    def __init__(self) -> None:
        super().__init__("INVALID_CONTENT_TYPE", 400, "The request must be of type application/json")


class MalformedRequestException(JoinCreateException):
    def __init__(self) -> None:
        super().__init__("MALFORMED_REQUEST", 400, "The request could not be parsed/is not valid JSON")


class DoesNotExistException(JoinCreateException):
    def __init__(self, reason: str) -> None:
        super().__init__("DOES_NOT_EXIST", 404, reason)


class UnprocessableEntityException(JoinCreateException):
    def __init__(self, reason: str) -> None:
        super().__init__("UNPROCESSABLE_ENTITY", 422, reason)


class ExpectedJSONObjectException(UnprocessableEntityException):
    def __init__(self) -> None:
        super().__init__("The request must be a JSON object")


class InvalidTypeException(UnprocessableEntityException):
    def __init__(self, variable_name: str, variable_type: str, *, required: bool = False) -> None:
        if required:
            reason = f"The request must contain the {variable_name} and be of type {variable_type}"
        else:
            reason = f"The value of {variable_name} must be of type {variable_type}"

        super().__init__(reason)


def load_token(raw_token: str) -> CreateJoinToken:
    if not isinstance(raw_token, str):
        raise UnprocessableEntityException("The token must be of type string")

    try:
        json_token = signing.loads(raw_token)
    except (ValueError, BadSignature):
        raise UnprocessableEntityException("The token is not valid")
    except SignatureExpired:
        raise UnprocessableEntityException("The token has expired")

    try:
        return CreateJoinToken.from_json_token(json_token)
    except Room.DoesNotExist:
        raise DoesNotExistException("The room has been deleted")
    except Meeting.DoesNotExist:
        raise DoesNotExistException("The meeting has been deleted")


def _post_transaction(action: Action, request: HttpRequest) -> None:
    if isinstance(action, WaitUntilRunningAction) and action.should_create_meeting:
        meeting_create(request, None, action.token.room.scheduling_strategy, action.token.meeting)


# TODO deprecated
@view_access_logging_decorator(logger)
class DeprecatedJoinRedirectView(RedirectView):
    http_method_names = ["get"]

    def get_redirect_url(self, room_name: str) -> str:
        kwargs = dict()

        if "joinSecret" in self.request.GET:
            kwargs["secret"] = self.request.GET["joinSecret"]
            kwargs["enforce_create_meeting_permission"] = True
            kwargs["enforce_join_meeting_permission"] = True
            kwargs["enforce_moderate_permission"] = True
            kwargs["join_as_room"] = True

        if "directJoin" in self.request.GET:
            kwargs["skip_meeting_configuration"] = self.request.GET["directJoin"].lower() == "true"

        return join_or_create_url(room_name, **kwargs)


@view_access_logging_decorator(logger)
class JoinOrCreateMeetingView(TemplateView):
    http_method_names = ["get"]
    template_name = "join_or_create.html"
    extra_context = {
        "vue_url": "/static/vue/vue.global.js" if settings.DEBUG else "/static/vue/vue.global.prod.js",
        "vue_i18n_url": "/static/vue/vue-i18n.global.js" if settings.DEBUG else "/static/vue/vue-i18n.global.prod.js",
    }


@method_decorator(non_atomic_requests, "dispatch")
@method_decorator(csrf_exempt, "dispatch")
@view_access_logging_decorator(logger)
class JoinOrCreateInitView(View):
    http_method_names = ["post"]

    def post(self, request: HttpRequest) -> HttpResponse:
        if request.content_type != "application/json":
            raise InvalidContentTypeException()

        try:
            data = json.loads(request.body)
        except ValueError:
            raise MalformedRequestException()

        validated_data = self.validate_data(data)

        with transaction.atomic(durable=True):
            token = self.process_data(**validated_data)
            action = get_next_action(token, get_tenant(request), request.user)

        _post_transaction(action, request)

        return JsonResponse(action.serialize())

    @classmethod
    def validate_data(cls, data: Any) -> Dict[str, Any]:
        if not isinstance(data, dict):
            raise ExpectedJSONObjectException()

        if "roomName" not in data or not isinstance(data["roomName"], str):
            raise InvalidTypeException("roomName", "string", required=True)
        room_name = data["roomName"]

        raw_token = data.get("token", _empty)

        skip_meeting_configuration = data.get("skipMeetingConfiguration", None)
        if not isinstance(skip_meeting_configuration, (type(None), bool)):
            raise InvalidTypeException("skipMeetingConfiguration", "boolean")

        secret = data.get("secret", None)
        if not isinstance(secret, (type(None), str)):
            raise InvalidTypeException("secret", "string")

        enforce_create_meeting_permission = data.get("enforceCreateMeetingPermission", None)
        if not isinstance(enforce_create_meeting_permission, (type(None), bool)):
            raise InvalidTypeException("enforceCreateMeetingPermission", "boolean")

        enforce_join_meeting_permission = data.get("enforceJoinMeetingPermission", None)
        if not isinstance(enforce_join_meeting_permission, (type(None), bool)):
            raise InvalidTypeException("enforceJoinMeetingPermission", "boolean")

        enforce_moderate_permission = data.get("enforceModeratePermission", None)
        if not isinstance(enforce_moderate_permission, (type(None), bool)):
            raise InvalidTypeException("enforceModeratePermission", "boolean")

        join_as_room = data.get("joinAsRoom", None)
        if not isinstance(join_as_room, (type(None), bool)):
            raise InvalidTypeException("joinAsRoom", "boolean")

        return {
            "room_name": room_name,
            "raw_token": raw_token,
            "skip_meeting_configuration": skip_meeting_configuration,
            "secret": secret,
            "enforce_create_meeting_permission": enforce_create_meeting_permission,
            "enforce_join_meeting_permission": enforce_join_meeting_permission,
            "enforce_moderate_permission": enforce_moderate_permission,
            "join_as_room": join_as_room,
        }

    def process_data(
        self,
        room_name: str,
        raw_token: Union[str, "_empty"],
        skip_meeting_configuration: Optional[bool],
        secret: Optional[str],
        enforce_create_meeting_permission: Optional[bool],
        enforce_join_meeting_permission: Optional[bool],
        enforce_moderate_permission: Optional[bool],
        join_as_room: Optional[bool],
    ) -> CreateJoinToken:
        token = None

        if raw_token is not _empty:
            token = load_token(raw_token)
            if token.room.name != room_name:
                token = None

        if token is None:
            try:
                token = CreateJoinToken.from_init_request(
                    get_tenant(self.request),
                    get_general_parameter(self.request),
                    room_name,
                    skip_meeting_configuration,
                    secret,
                    enforce_create_meeting_permission,
                    enforce_join_meeting_permission,
                    enforce_moderate_permission,
                    join_as_room,
                )
            except Room.DoesNotExist:
                raise DoesNotExistException("Room does not exist")

        return token


@method_decorator(non_atomic_requests, "dispatch")
@method_decorator(csrf_exempt, "dispatch")
@view_access_logging_decorator(logger)
class _ActionBaseView(View, ABC):
    tenant: Site
    user: Union[AnonymousUser, User]

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.tenant = get_tenant(request)
        self.user = request.user

    def dispatch(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if request.method != "POST":
            return self.http_method_not_allowed(request, *args, **kwargs)

        if request.content_type != "application/json":
            raise InvalidContentTypeException()

        try:
            data = json.loads(request.body)
        except ValueError:
            raise MalformedRequestException()

        if not isinstance(data, dict):
            raise ExpectedJSONObjectException()

        return self.post(data)

    @abstractmethod
    def post(self, data: Dict[str, JsonType]) -> JsonResponse:
        ...

    def _allowed_methods(self) -> List["str"]:
        return ["POST"]


class _ActionView(_ActionBaseView, ABC):
    @classmethod
    @abstractmethod
    def _action_type(cls) -> Type[IntermediateAction]:
        ...

    @abstractmethod
    def perform_action(self, token: CreateJoinToken, data: Dict[str, JsonType]) -> [Action, int]:
        ...

    def post(self, data: Dict[str, JsonType]) -> JsonResponse:
        with transaction.atomic(durable=True):
            action = get_next_action(load_token(data.pop("token", None)), self.tenant, self.user)
            status = 200

            action_type = self._action_type()
            if isinstance(action, action_type):
                action, status = self.perform_action(action.token, data)

        _post_transaction(action, self.request)

        return JsonResponse(action.serialize(), status=status)


class JoinOrCreateNextActionView(_ActionBaseView):
    def post(self, data: Dict[str, JsonType]) -> JsonResponse:
        with transaction.atomic(durable=True):
            action = get_next_action(load_token(data.pop("token", None)), self.tenant, self.user)

        _post_transaction(action, self.request)

        return JsonResponse(action.serialize())


@method_decorator(non_atomic_requests, "dispatch")
@method_decorator(csrf_exempt, "dispatch")
@view_access_logging_decorator(logger)
class CreateMeetingView(View):
    http_method_names = ["post"]
    tenant: Site
    general_parameter: GeneralParameter

    def setup(self, request: HttpRequest, *args: Any, **kwargs: Any) -> None:
        super().setup(request, *args, **kwargs)

        self.tenant = get_tenant(request)
        self.general_parameter = get_general_parameter(request)

    def post(self, request: HttpRequest) -> JsonResponse:
        with atomic(durable=True):
            data = request.POST.copy()
            # Please use the action.token instance instead since it holds more recent data
            _do_not_use_token = load_token(data.get("__token", None))
            action = get_next_action(_do_not_use_token, self.tenant, request.user)
            data.pop("__token")

            if not isinstance(action, ConfigureMeetingAction):
                if isinstance(action, WaitUntilRunningAction):
                    action = WaitUntilRunningAction(
                        action.token,
                        should_create_meeting=action.should_create_meeting,
                        started_by_someone_else=not action.token.is_creator,
                    )
                elif isinstance(action, RedirectAction):
                    # action does not contain a token at this point therefore
                    # we will fall back to the one from the request
                    token = _do_not_use_token

                    unfinished_meeting = token.room.get_unfinished_meeting()
                    is_creator = unfinished_meeting.creator is not None and unfinished_meeting.creator == request.user

                    token = dataclasses.replace(token, meeting=unfinished_meeting, is_creator=is_creator)

                    action = WaitUntilRunningAction(token, started_by_someone_else=not is_creator)

                return JsonResponse(action.serialize())

            form = ConfigureMeetingForm.create(
                self.tenant, self.general_parameter, action.token.room, request.user, data=data
            )

            if form.is_valid():
                action = self.form_valid(form, action.token)
            else:
                return JsonResponse(ConfigureMeetingAction(action.token, form=form).serialize(), status=400)

        _post_transaction(action, request)

        return JsonResponse(action.serialize())

    def form_valid(self, form: ConfigureMeetingForm, token: CreateJoinToken) -> WaitUntilRunningAction:
        meeting = join_or_create.create_meeting(
            token.room,
            self.tenant,
            self.request.user,
            configuration=form.save(),
        )

        token = dataclasses.replace(token, is_creator=True, meeting=meeting)

        return WaitUntilRunningAction(token, should_create_meeting=True)


class OptOutCreateMeetingView(_ActionView):
    @classmethod
    def _action_type(cls) -> Type[IntermediateAction]:
        return ConfigureMeetingAction

    def perform_action(self, token: CreateJoinToken, data: Dict[str, JsonType]) -> tuple[Action, int]:
        token = dataclasses.replace(token, opt_out_create_meeting=True)
        return get_next_action(token, self.tenant, self.user), 200


class OptInCreateMeetingView(_ActionView):
    @classmethod
    def _action_type(cls) -> Type[IntermediateAction]:
        return WaitUntilConfiguredAction

    def perform_action(self, token: CreateJoinToken, data: Dict[str, JsonType]) -> tuple[Action, int]:
        token = dataclasses.replace(token, opt_out_create_meeting=False)
        return get_next_action(token, self.tenant, self.user), 200


class ValidateAccessCodeView(_ActionView):
    @classmethod
    def _action_type(cls) -> Type[IntermediateAction]:
        return PromptAccessCodeAction

    def perform_action(self, token: CreateJoinToken, data: Dict[str, JsonType]) -> tuple[Action, int]:
        access_code = data.get("accessCode", None)
        if not isinstance(access_code, str):
            raise InvalidTypeException("accessCode", "string", required=True)

        if access_code != token.meeting.configuration.access_code:
            return PromptAccessCodeAction(token, invalid=True), 400

        token = dataclasses.replace(token, access_granted=True)
        return get_next_action(token, self.tenant, self.user), 200


class SetJoinNameView(_ActionView):
    @classmethod
    def _action_type(cls) -> Type[IntermediateAction]:
        return PromptJoinNameAction

    def perform_action(self, token: CreateJoinToken, data: Dict[str, JsonType]) -> tuple[Action, int]:
        join_name = data.get("joinName", None)
        if not isinstance(join_name, str):
            raise InvalidTypeException("joinName", "string", required=True)

        join_name = join_name.strip()
        if len(join_name) < 2:
            return PromptJoinNameAction(token, join_name, name_is_too_short=True), 400

        is_moderator = is_user_moderator(token, self.tenant, self.user)
        return RedirectAction(token.meeting, self.user, is_moderator, join_name), 200
