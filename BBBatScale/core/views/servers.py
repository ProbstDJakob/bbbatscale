import logging

from core.constants import SERVER_STATES
from core.decorators import tenant_based_permission_required
from core.forms import ServerForm
from core.models import SchedulingStrategy, Server
from core.utils import get_general_parameter, get_tenant, paginate
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.timesince import timesince
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_server", elevate_staff=False, raise_exception=True)
def servers_overview(request):
    logger.info(create_view_access_logging_message(request))
    return render(
        request,
        "servers_overview.html",
        {
            "server_states": SERVER_STATES,
            "scheduling_strategies": SchedulingStrategy.objects.filter(tenants__in=[get_tenant(request)]),
        },
    )


@login_required
@tenant_based_permission_required("core.add_server", elevate_staff=False, raise_exception=True)
def server_create(request):
    logger.info(create_view_access_logging_message(request))

    form = ServerForm(request.POST or None, general_parameter=get_general_parameter(request))
    if request.method == "POST" and form.is_valid():
        instance = form.save()
        messages.success(request, _("Server: {} was created successfully.").format(instance.dns))
        # return to URL
        return redirect("servers_overview")
    return render(request, "servers_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_server", elevate_staff=False, raise_exception=True)
def server_delete(request, server):
    logger.info(create_view_access_logging_message(request, server))

    instance = get_object_or_404(Server, pk=server)
    instance.delete()
    messages.success(request, _("Server was deleted successfully"))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@tenant_based_permission_required(["core.view_server", "core.change_server"], elevate_staff=False, raise_exception=True)
def server_update(request, server):
    logger.info(create_view_access_logging_message(request, server))

    instance = get_object_or_404(Server, pk=server)
    form = ServerForm(request.POST or None, instance=instance, general_parameter=get_general_parameter(request))
    if request.method == "POST":
        if form.is_valid():
            _server = form.save()
            messages.success(request, _("Server: {} was updated successfully.").format(_server.dns))
            return redirect("servers_overview")
    return render(request, "servers_create.html", {"form": form})


def map_server(server: Server) -> dict:
    return {
        "updateUrl": reverse("server_update", args=[server.id]),
        "deleteUrl": reverse("server_delete", args=[server.id]),
        "id": server.id,
        "schedulingStrategy": server.scheduling_strategy.name,
        "dns": server.dns,
        "machineId": server.machine_id,
        "sharedSecret": server.shared_secret,
        "state": server.state,
        "datacenter": server.datacenter,
        "participants": server.get_participant_count(),
        "maxParticipants": server.participant_count_max,
        "videostreams": server.get_videostream_count(),
        "maxVideostreams": server.videostream_count_max,
        "utilization": server.get_utilization_percent(),
        "lastHealthcheck": timesince(server.last_health_check) if server.last_health_check else None,
    }


@login_required
@tenant_based_permission_required("core.view_server", elevate_staff=False, raise_exception=True)
def get_servers(request: HttpRequest) -> HttpResponse:
    tenant = get_tenant(request)
    query = (
        Server.objects.filter(scheduling_strategy__tenants__in=[tenant])
        .prefetch_related("scheduling_strategy")
        .order_by("scheduling_strategy", "dns")
    )

    search_filter = request.GET.get("filter-search")
    if search_filter:
        query = query.filter(
            Q(dns__icontains=search_filter)
            | Q(datacenter__icontains=search_filter)
            | Q(machine_id__icontains=search_filter)
        )

    state_filter = request.GET.get("filter-state")
    if state_filter:
        query = query.filter(state=state_filter)

    scheduling_strategy_filter = request.GET.get("filter-strategy")
    if scheduling_strategy_filter:
        query = query.filter(scheduling_strategy=scheduling_strategy_filter)

    return paginate(query, int(request.GET.get("page", 1)), map_server)
