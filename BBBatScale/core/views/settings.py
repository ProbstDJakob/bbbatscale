import logging

from core.decorators import tenant_based_permission_required
from core.forms import GeneralParametersForm
from core.utils import get_general_parameter
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required(["core.view_generalparameter", "core.change_generalparameter"], raise_exception=True)
def settings_edit(request):
    logger.info(create_view_access_logging_message(request))
    gp = get_general_parameter(request)
    form = GeneralParametersForm(request.POST or None, requesting_user=request.user, instance=gp)

    if request.method == "POST" and form.is_valid():
        logger.debug("request.method == 'POST' and form.is_valid()")
        form.save()

        success_msg = _("Settings successfully saved.")
        messages.success(request, success_msg)
        logger.debug("Success '%s' was set.", success_msg)

        return redirect("settings_edit")
    else:
        logger.debug("NOT request.method == 'POST' and form.is_valid()")
        return render(request, "settings.html", {"form": form})
