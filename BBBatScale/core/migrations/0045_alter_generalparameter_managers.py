# Generated by Django 3.2.13 on 2022-06-29 16:27

import core.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0044_meeting_replay_title"),
    ]

    operations = [
        migrations.AlterModelManagers(
            name="generalparameter",
            managers=[
                ("objects", core.models.GeneralParameterManager()),
            ],
        ),
    ]
