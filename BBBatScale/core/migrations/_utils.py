from typing import TYPE_CHECKING

from django.apps.registry import Apps
from django.db.backends.base.schema import BaseDatabaseSchemaEditor

if TYPE_CHECKING:
    from django.contrib.auth.models import Permission


def ensure_permission_exists(
    apps: Apps, schema_editor: BaseDatabaseSchemaEditor, model_name: str, codename: str, name: str
) -> "Permission":
    ContentType = apps.get_model("contenttypes", "ContentType")
    Permission = apps.get_model("auth", "Permission")
    Model = apps.get_model("core", model_name)
    db_alias = schema_editor.connection.alias

    content_type = ContentType.objects.db_manager(db_alias).get_for_model(Model)
    return Permission.objects.db_manager(db_alias).get_or_create(
        codename=codename,
        content_type=content_type,
        defaults={
            "name": name,
        },
    )[0]
