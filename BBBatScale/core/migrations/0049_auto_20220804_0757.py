# Generated by Django 3.2.13 on 2022-08-04 07:57

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0048_generalparameter_home_room_last_used_config_enabled"),
    ]

    operations = [
        migrations.AlterField(
            model_name="agentconfiguration",
            name="registration_interval",
            field=models.DurationField(
                help_text="Duration in seconds between periodic registration calls from agent", null=True
            ),
        ),
        migrations.AlterField(
            model_name="agentconfiguration",
            name="stats_interval",
            field=models.DurationField(
                help_text="Duration in seconds between periodic stats calls from agent", null=True
            ),
        ),
        migrations.AlterField(
            model_name="generalparameter",
            name="default_theme",
            field=models.ForeignKey(
                help_text="Define which theme should be the default theme for users who have not changed their theme"
                " yet or for non-logged-in users.",
                on_delete=django.db.models.deletion.PROTECT,
                to="core.theme",
                verbose_name="Default Theme",
            ),
        ),
        migrations.AlterField(
            model_name="generalparameter",
            name="home_room_enabled",
            field=models.BooleanField(
                default=False,
                help_text="If checked the home room feature is enabled and users have access to their personal room"
                " via a button on the start page.",
                verbose_name="Enable home room",
            ),
        ),
        migrations.AlterField(
            model_name="generalparameter",
            name="home_room_last_used_config_enabled",
            field=models.BooleanField(
                default=True,
                help_text="If checked the users can start their home room with their last used configuration.",
                verbose_name="Allow to start home room with last used configuration",
            ),
        ),
        migrations.AlterField(
            model_name="generalparameter",
            name="home_room_room_configuration",
            field=models.ForeignKey(
                blank=True,
                help_text="Define which room configuration will be used for new home rooms.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="core.meetingconfigurationtemplate",
                verbose_name="Room configuration",
            ),
        ),
        migrations.AlterField(
            model_name="generalparameter",
            name="home_room_scheduling_strategy",
            field=models.ForeignKey(
                blank=True,
                help_text="Define which scheduling strategy will be used for new home rooms.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="core.schedulingstrategy",
                verbose_name="Scheduling Strategy",
            ),
        ),
    ]
