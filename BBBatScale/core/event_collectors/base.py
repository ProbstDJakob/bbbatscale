import ast
import logging
from abc import ABC, abstractmethod

from core.models import RoomEvent

logger = logging.getLogger(__name__)


class EventCollectorStrategy(ABC):
    @abstractmethod
    def collect_events(self, room_pk, parameters):
        pass

    def _evaluate_parameters(self, room_pk, parameters, necessary_keys):
        logger.debug(
            f"Evaluate event collector parameters dict for room with pk={room_pk} necessary_keys={necessary_keys}"
        )
        if isinstance(parameters, str):
            try:
                parameters = ast.literal_eval(parameters)
            except SyntaxError as e:
                msg = (
                    "Parameters of room with pk={} is a string but cannot be converted to ".format(room_pk)
                    + "dictionary. Please check if the syntax is properly set, e.g. "
                    + '{"key0": "value0, "key1": "value1"}'
                    + "The error ist {}".format(e)
                )
                logger.warning(msg)
                raise Exception(msg)
            except Exception as e:
                msg = (
                    "Parameters of room with pk={} is a string but cannot be converted to ".format(room_pk)
                    + "dictionary. Please check if the syntax is properly set, e.g."
                    + '{"key0": "value0, "key1": "value1"}'
                    + "The error ist {}".format(e)
                )
                logger.warning(msg)
                raise Exception(msg)
            logger.debug("Parameters of room with pk={} was read as Dict ".format(room_pk))

        if isinstance(parameters, dict):
            if not all(key in parameters for key in necessary_keys):
                msg = "Not all necessary parameters provided for collector strategy of room pk={}. ".format(
                    room_pk
                ) + "Necessary keys: {} - Provided keys: {}".format(necessary_keys, parameters.keys())
                logger.warning(msg)
                raise Exception(msg)
        elif parameters is None:
            msg = "No parameters set for room with pk={} even though the strategy was set.".format(room_pk)
            logger.warning(msg)
            raise TypeError(msg)
        else:
            msg = "No usable value set for parameters of room with pk={}".format(room_pk)
            logger.warning(msg)
            raise TypeError(msg)
        logger.debug(f"Event collector parameters for room with pk {room_pk} successfully read.")

        return parameters

    def _update_room_events_in_db(self, room_events, room_pk):
        logger.debug("update room_events ({}) for room with room_pk={}".format(room_events, room_pk))
        # TODO: this is poor programming. Simply deleting everything an recreating new data...
        RoomEvent.objects.filter(room=room_pk).delete()

        for event in list(room_events.values()):
            event.save()


class EventCollectorContext:
    def __init__(self, strategy_name: str):
        logger.debug("Init event collector context for strategy {}".format(strategy_name))

        self._strategy_name = strategy_name
        try:
            import importlib

            loaded_class = getattr(
                importlib.import_module("core.event_collectors.{}".format(strategy_name)), strategy_name
            )
        except (AttributeError, ModuleNotFoundError) as err:
            if self._strategy_name is None:
                logger.debug(
                    "{} No event_collection_strategy set for room. This is probably ".format(err)
                    + "fine if not all your rooms have a calender associated"
                )
            else:
                logging.warning(
                    "Event collector strategy '{}' initiallization failed!".format(strategy_name)
                    + "{} Make sure the module core.event_collectors.{} exists ".format(err, strategy_name)
                    + "and provides a class {} as implementation of EventCollectorStrategy".format(strategy_name)
                )
            raise err

        self._strategy = loaded_class()
        logging.debug("EventCollector Strategy {} loaded successfully".format(strategy_name))

    def collect_events(self, room_pk, parameters):
        logging.debug("Start reading data for room with pk {} with strategy {}".format(room_pk, self._strategy_name))
        self._strategy.collect_events(room_pk, parameters)
