import logging
import re
from datetime import datetime, timedelta
from typing import Any, Callable, Dict, Iterable, Optional, Tuple, Union

from core.feature_flag import Feature
from core.models import (
    AgentConfiguration,
    ApiToken,
    GeneralParameter,
    Meeting,
    MeetingConfiguration,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    Server,
    ServerType,
    Slides,
    User,
    mark_slides_file_for_removal,
)
from core.utils import (
    format_human_readable_size,
    has_tenant_based_perm,
    load_moderator_group,
    parse_human_readable_size,
)
from crispy_forms.bootstrap import FieldWithButtons, StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Field, Layout, MultiWidgetField, Submit
from django import forms
from django.conf import settings
from django.contrib.auth.forms import PasswordChangeForm as DjangoPasswordChangeForm
from django.contrib.auth.forms import SetPasswordForm as DjangoSetPasswordForm
from django.contrib.auth.models import AnonymousUser, Group
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import Site
from django.core import validators
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import FileExtensionValidator
from django.db.models import Q, Subquery
from django.template import loader
from django.utils.formats import get_format
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class IntRepresentedAsByteSize(int):
    def __new__(cls, x: Union[str, int] = 0) -> "IntRepresentedAsByteSize":
        return int.__new__(cls, parse_human_readable_size(x) if isinstance(x, str) else x)

    def __repr__(self) -> str:
        return format_human_readable_size(int(self))

    def __str__(self) -> str:
        return format_human_readable_size(int(self))


class ByteSizeInput(forms.fields.TextInput):
    def format_value(self, value):
        return str(IntRepresentedAsByteSize(value))

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        ts = re.escape(get_format("THOUSAND_SEPARATOR"))
        ds = re.escape(get_format("DECIMAL_SEPARATOR"))
        byte_number_regex = f"(?:\\d+|\\d{{1,3}}(?:{ts}\\d{{3}})+)(?:(?:{ds}\\d+)?[kMGTPEZY]i?)?B"

        context["widget"]["attrs"] = self.build_attrs(
            context["widget"]["attrs"],
            {"pattern": f"^{byte_number_regex}$"},
        )
        return context


class ByteSizeField(forms.IntegerField):
    widget = ByteSizeInput

    def __init__(self, *, max_value=None, min_value=None, **kwargs):
        self.max_value, self.min_value = max_value, min_value
        # Localized number input is not well-supported on most browsers
        kwargs["localize"] = False
        super().__init__(**kwargs)

        if max_value is not None:
            self.validators.append(validators.MaxValueValidator(IntRepresentedAsByteSize(max_value)))
        if min_value is not None:
            self.validators.append(validators.MinValueValidator(IntRepresentedAsByteSize(min_value)))

    def has_changed(self, initial, data):
        if self.disabled:
            return False
        try:
            if not isinstance(data, str):
                data = str(IntRepresentedAsByteSize(data))
        except ValidationError:
            return True
        # For purposes of seeing whether something has changed, None is
        # the same as an empty string, if the data or initial value we get
        # is None, replace it with "".
        initial_value = str(IntRepresentedAsByteSize(initial)) if initial is not None else ""
        data_value = data if data is not None else ""
        return initial_value != data_value

    def clean(self, value):
        value = super().clean(value)
        # convert into string and back to be sure, "what you see is what you get"
        return int(IntRepresentedAsByteSize(str(IntRepresentedAsByteSize(value))))

    def to_python(self, value):
        value = super(forms.IntegerField, self).to_python(value)
        if value in self.empty_values:
            return None
        try:
            value = int(IntRepresentedAsByteSize(value))
        except (ValueError, TypeError):
            raise ValidationError(self.error_messages["invalid"], code="invalid")
        return value


def conditional_field(
    form: forms.ModelForm,
    field_name: str,
    condition: bool,
    *,
    crispy_attributes: Optional[Dict[str, Any]] = None,
) -> Optional[Field]:
    if condition:
        return Field(field_name, **(crispy_attributes or dict()))
    else:
        form.fields[field_name].disabled = True
        return None


def init_slides_fields(
    form: forms.ModelForm,
    general_parameter: GeneralParameter,
    requesting_user: Union[User, AnonymousUser],
    tenant: Site,
    own_slides: bool,
) -> Tuple[Field, Field]:
    slides_enabled = Feature.Media(general_parameter).is_enabled() and has_tenant_based_perm(
        requesting_user, "core.view_own_slides" if own_slides else "core.view_slides", tenant
    )
    slides_crispy_field = conditional_field(form, "slides", slides_enabled)
    initial_slides_crispy_field = conditional_field(form, "initial_slides", slides_enabled)

    if slides_enabled and not (requesting_user.is_anonymous and own_slides):
        slides_query_filter = Q(owner=requesting_user) if own_slides else Q(owner__isnull=True)

        if form.instance.pk is not None:
            slides_query_filter |= Q(pk__in=Subquery(getattr(form.instance, "slides").values("pk")))

        slides_form_field = form.fields["slides"]
        slides_form_field.label = _("Slides")
        slides_form_field.queryset = slides_form_field.queryset.filter(slides_query_filter)
        slides_form_field.widget.option_template_name = "file_select_option.html"

        initial_slides_form_field = form.fields["initial_slides"]
        initial_slides_form_field.label = _("Initial Slides")
        initial_slides_form_field.queryset = initial_slides_form_field.queryset.filter(slides_query_filter)
        initial_slides_form_field.widget = forms.HiddenInput()

    return slides_crispy_field, initial_slides_crispy_field


class GeneralParametersForm(forms.ModelForm):
    class Meta:
        model = GeneralParameter
        fields = [
            "latest_news",
            "jitsi_url",
            "faq_url",
            "feedback_email",
            "footer",
            "logo_link",
            "favicon_link",
            "page_title",
            "home_room_enabled",
            "playback_url",
            "home_room_teachers_only",
            "home_room_scheduling_strategy",
            "enable_occupancy",
            "download_url",
            "home_room_room_configuration",
            "home_room_last_used_config_enabled",
            "jitsi_enable",
            "default_theme",
            "personal_rooms_enabled",
            "personal_rooms_scheduling_strategy",
            "personal_rooms_teacher_max_number",
            "personal_rooms_non_teacher_max_number",
            "enable_recordings",
            "enable_adminunmute",
            "enable_meeting_dial_number",
            "enable_meeting_rtmp_streaming",
            "hide_statistics_enable",
            "hide_statistics_logged_in_users",
            "show_personal_rooms",
            "show_home_rooms",
            "app_title",
            "recording_management_url",
            "recording_cert",
            "recording_key",
            "recording_deletion_period",
            "media_enabled",
            "moderators_can_upload_slides",
            "slides_max_upload_size",
            "slides",
            "initial_slides",
        ]
        field_classes = {
            "slides_max_upload_size": ByteSizeField,
        }

    def __init__(self, *args, requesting_user: User, **kwargs):
        super(GeneralParametersForm, self).__init__(*args, **kwargs)

        media_enabled_field = conditional_field(self, "media_enabled", settings.BBBATSCALE_MEDIA_ENABLED)
        media_section_seperator = (
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Media"))
            )
            if settings.BBBATSCALE_MEDIA_ENABLED
            else None
        )
        media_enabled = settings.BBBATSCALE_MEDIA_ENABLED and Feature.Media(self.instance).is_enabled()
        moderators_can_upload_slides_field = conditional_field(self, "moderators_can_upload_slides", media_enabled)
        slides_max_upload_size_field = conditional_field(self, "slides_max_upload_size", media_enabled)
        slides_field, initial_slides_field = init_slides_fields(
            self, kwargs["instance"], requesting_user, kwargs["instance"].tenant, False
        )

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-3"
        self.helper.field_class = "col-lg-9"
        self.helper.layout = Layout(
            Field("latest_news"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("General site settings"))
            ),
            Field("page_title"),
            Field("app_title"),
            Field("logo_link"),
            Field("favicon_link"),
            Field("default_theme"),
            Field("jitsi_enable"),
            Field("jitsi_url"),
            Field("faq_url"),
            Field("hide_statistics_logged_in_users"),
            Field("hide_statistics_enable"),
            Field("enable_adminunmute"),
            Field("enable_meeting_dial_number"),
            Field("enable_meeting_rtmp_streaming"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Recordings"))
            ),
            Field("enable_recordings"),
            Field("recording_deletion_period"),
            Field("recording_management_url"),
            Field("recording_cert"),
            Field("recording_key"),
            Field("playback_url"),
            Field("download_url"),
            Field("footer"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Home room"))
            ),
            Field("home_room_enabled"),
            Field("show_home_rooms"),
            Field("home_room_teachers_only"),
            Field("home_room_last_used_config_enabled"),
            Field("home_room_scheduling_strategy"),
            Field("home_room_room_configuration"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Personal room"))
            ),
            Field("personal_rooms_enabled"),
            Field("show_personal_rooms"),
            Field("personal_rooms_scheduling_strategy"),
            Field("personal_rooms_teacher_max_number"),
            Field("personal_rooms_non_teacher_max_number"),
            Field("enable_occupancy"),
            media_section_seperator,
            media_enabled_field,
            moderators_can_upload_slides_field,
            slides_max_upload_size_field,
            slides_field,
            initial_slides_field,
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def _save_m2m(self):
        super()._save_m2m()

        load_moderator_group(self.instance.tenant, force_update_perms=True)


class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        fields = [
            "dns",
            "datacenter",
            "shared_secret",
            "scheduling_strategy",
            "participant_count_max",
            "videostream_count_max",
            "server_types",
            "machine_id",
        ]

    def __init__(self, *args, general_parameter: GeneralParameter, **kwargs):
        super(ServerForm, self).__init__(*args, **kwargs)

        self.fields["dns"].label = _("DNS")
        self.fields["machine_id"].label = _("Maschine Id")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        scheduling_strategy_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.scheduling_strategy_id:
            scheduling_strategy_filter |= Q(id=self.instance.scheduling_strategy_id)

        self.fields["scheduling_strategy"].queryset = SchedulingStrategy.objects.filter(
            scheduling_strategy_filter
        ).distinct()
        self.fields["datacenter"].label = _("Datacenter")
        self.fields["shared_secret"].label = _("Shared Secret")
        self.fields["shared_secret"].required = True
        self.fields["participant_count_max"].label = _("Max participants")
        self.fields["videostream_count_max"].label = _("Max videostreams")
        self.fields["server_types"].label = _("Server Types")
        self.fields["server_types"].queryset = ServerType.objects.all()
        self.fields["server_types"].help_text = _(
            "Server types are determining which work is scheduled on that server."
        )

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("dns"),
            Field("machine_id"),
            Field("scheduling_strategy"),
            Field("datacenter"),
            Field("shared_secret"),
            MultiWidgetField("server_types"),
            Field("participant_count_max"),
            Field("videostream_count_max"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = [
            "scheduling_strategy",
            "name",
            "default_meeting_configuration",
            "visibility",
            "state",
            "direct_join_secret",
            "comment_public",
            "comment_private",
            "click_counter",
            "event_collection_strategy",
            "event_collection_parameters",
            "last_running",
            "tenants",
            "slides",
            "initial_slides",
        ]

    def __init__(self, *args, tenant: Site, general_parameter: GeneralParameter, requesting_user: User, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)

        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        scheduling_strategy_filter = Q(tenants__in=[tenant])
        if self.instance.scheduling_strategy_id:
            scheduling_strategy_filter |= Q(id=self.instance.scheduling_strategy_id)

        self.fields["scheduling_strategy"].queryset = SchedulingStrategy.objects.filter(
            scheduling_strategy_filter
        ).distinct()
        self.fields["tenants"].label = _("Tenants")
        self.fields["name"].label = _("Name")
        self.fields["default_meeting_configuration"].label = _("Configuration")
        default_meeting_configuration_filter = Q(tenants__in=[tenant])
        if self.instance.default_meeting_configuration_id:
            default_meeting_configuration_filter |= Q(id=self.instance.default_meeting_configuration_id)
        self.fields["default_meeting_configuration"].queryset = MeetingConfigurationTemplate.objects.filter(
            default_meeting_configuration_filter
        ).distinct()
        self.fields["visibility"].label = _("Visible on front page for")
        self.fields["comment_private"].label = _("Comment private")
        self.fields["comment_public"].label = _("Comment public")
        self.fields["event_collection_strategy"].label = _("Collection Strategy")
        self.fields["event_collection_parameters"].label = _("Collection Parameters")

        # System automatic configs
        self.fields["state"].label = _("State")
        self.fields["direct_join_secret"].label = _("Direct Join Secret")
        self.fields["click_counter"].label = _("Click counter")
        self.fields["last_running"].label = _("Last running")

        slides_field, initial_slides_field = init_slides_fields(self, general_parameter, requesting_user, tenant, False)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("scheduling_strategy"),
            Field("name"),
            Field("default_meeting_configuration"),
            Field("tenants"),
            Field("visibility"),
            Field("comment_private"),
            Field("comment_public"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-4"></div><div class="col-lg-8">'
                "<h4>{}</h4></div></div>".format(_("Event Collection Configs"))
            ),
            Field("event_collection_strategy"),
            Field("event_collection_parameters"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-4"></div><div class="col-lg-8">'
                "<h4>{}</h4></div></div>".format(_("System provided values"))
            ),
            Field("state"),
            FieldWithButtons(
                Field("direct_join_secret", id="direct_join_secret"),
                StrictButton(_("Generate Secret"), css_class="btn-secondary", id="direct_join_secret_generate"),
            ),
            Field("click_counter"),
            Field("last_running"),
            slides_field,
            initial_slides_field,
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class HomeRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = [
            "name",
            "default_meeting_configuration",
            "visibility",
            "comment_public",
            "slides",
            "initial_slides",
        ]

    def __init__(self, *args, tenant: Site, general_parameter: GeneralParameter, requesting_user: User, **kwargs):
        super(HomeRoomForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["name"].disabled = True
        self.fields["default_meeting_configuration"].label = _("Configuration")
        default_meeting_configuration_filter = Q(tenants__in=[tenant])
        if self.instance.default_meeting_configuration_id:
            default_meeting_configuration_filter |= Q(id=self.instance.default_meeting_configuration_id)
        self.fields["default_meeting_configuration"].queryset = MeetingConfigurationTemplate.objects.filter(
            default_meeting_configuration_filter
        ).distinct()
        self.fields["visibility"].label = _("Visible on front page for")
        self.fields["comment_public"].label = _("Comment")

        slides_field, initial_slides_field = init_slides_fields(self, general_parameter, requesting_user, tenant, True)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("default_meeting_configuration"),
            Field("visibility"),
            Field("comment_public"),
            slides_field,
            initial_slides_field,
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PersonalRoomForm(forms.ModelForm):
    class Meta:
        model = PersonalRoom
        fields = [
            "owner",
            "scheduling_strategy",
            "tenants",
            "name",
            "default_meeting_configuration",
            "visibility",
            "comment_public",
            "co_owners",
            "slides",
            "initial_slides",
        ]

    def __init__(self, *args, tenant: Site, general_parameter: GeneralParameter, requesting_user: User, **kwargs):
        super(PersonalRoomForm, self).__init__(*args, **kwargs)
        self.tenant = tenant

        is_owner = self.fields["owner"].prepare_value(self.initial["owner"]) == self.fields["owner"].prepare_value(
            requesting_user
        )
        if not is_owner:
            self.fields["name"].disabled = True
            self.fields["visibility"].disabled = True
            self.fields["comment_public"].disabled = True

        self.fields["owner"].disabled = True
        self.fields["scheduling_strategy"].disabled = True
        self.fields["tenants"].disabled = True

        self.fields["owner"].label = _("Owner")
        self.fields["name"].label = _("Name")
        self.fields["default_meeting_configuration"].label = _("Room configuration")
        default_meeting_configuration_filter = Q(tenants__in=[tenant])
        if self.instance.default_meeting_configuration_id:
            default_meeting_configuration_filter |= Q(id=self.instance.default_meeting_configuration_id)
        self.fields["default_meeting_configuration"].queryset = MeetingConfigurationTemplate.objects.filter(
            default_meeting_configuration_filter
        ).distinct()
        self.fields["visibility"].label = _("Visible on front page for")
        self.fields["comment_public"].label = _("Comment")
        self.fields["co_owners"].label = _("Co-Owners")
        # Restrict the queryset to improve performance and lazy load (via fetch api) the users in the frontend.
        self.fields["co_owners"].queryset = (
            User.objects.none() if not self.instance.pk else self.instance.co_owners.all()
        )

        slides_field, initial_slides_field = init_slides_fields(self, general_parameter, requesting_user, tenant, True)

        self.helper: FormHelper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("owner") if not is_owner else None,
            Field("name"),
            Field("default_meeting_configuration"),
            Field("visibility"),
            Field("comment_public"),
            HTML(
                '<hr><div class="form-group row "><label for="id_filter_owner" '
                'class="col-form-label col-lg-4">Filter Users</label>'
                '<div class="col-lg-8"><input type="text" name="filter_owner" '
                'class="textinput textInput form-control" id="id_filter_owner"></div></div>'
            ),
            MultiWidgetField("co_owners"),
            slides_field,
            initial_slides_field,
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def _clean_fields(self):
        # Extend the queryset to allow all suitable users.
        self.fields["co_owners"].queryset = User.objects.filter(tenant=self.tenant)
        super()._clean_fields()


class MeetingConfigurationsTemplateForm(forms.ModelForm):
    class Meta:
        model = MeetingConfigurationTemplate
        fields = [
            "name",
            "tenants",
            "mute_on_start",
            "moderation_mode",
            "meetingLayout",
            "everyone_can_start",
            "allow_guest_entry",
            "access_code",
            "only_prompt_guests_for_access_code",
            "disable_mic",
            "disable_cam",
            "allow_recording",
            "disable_note",
            "disable_private_chat",
            "disable_public_chat",
            "allow_unmuteusers",
            "allow_ejectcam",
            "allow_learningdashboard",
            "authenticated_user_can_start",
            "guest_policy",
            "dialNumber",
            "welcome_message",
            "logoutUrl",
            "maxParticipants",
            "streamingUrl",
        ]

    def __init__(self, *args, general_parameter: GeneralParameter, **kwargs):
        super(MeetingConfigurationsTemplateForm, self).__init__(*args, **kwargs)

        self.fields["tenants"].disabled = True

        self.fields["name"].label = _("Name")
        self.fields["mute_on_start"].label = _("Everyone is muted on start")
        self.fields["moderation_mode"].label = _("Moderation mode")
        self.fields["meetingLayout"].label = _("Meeting Layout")
        self.fields["everyone_can_start"].label = _("Everyone can start meeting")
        self.fields["authenticated_user_can_start"].label = _("Registered user can start meeting")
        self.fields["allow_guest_entry"].label = _("Allow guests")
        self.fields["access_code"].label = _("Access code")
        self.fields["only_prompt_guests_for_access_code"].label = _("Only prompt guests for access code")
        self.fields["disable_cam"].label = _("Disable camera for non moderators")
        self.fields["disable_mic"].label = _("Disable microphone for non moderators")
        self.fields["allow_recording"].label = _("Allow recording")
        self.fields["disable_note"].label = _("Disable shared notes - editing only for moderators")
        self.fields["disable_public_chat"].label = _("Disable public chat - editing only for moderators")
        self.fields["disable_private_chat"].label = _("Disable private chat - editing only for moderators")
        self.fields["allow_unmuteusers"].label = _("Allow Moderators to unmute Users")
        self.fields["allow_ejectcam"].label = _("Allow Moderators to eject Users-Webcams")
        self.fields["allow_learningdashboard"].label = _("Enable Learning Dashboard")
        self.fields["guest_policy"].label = _("Entry policy for non-moderators")
        self.fields["logoutUrl"].label = _("Logout URL")
        self.fields["dialNumber"].label = _("Dial number")
        self.fields["welcome_message"].label = _("Welcome message")
        self.fields["welcome_message"].help_text = _("Max. 255 characters. Limited support of HTML.")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")

        allow_unmuteusers_field = conditional_field(
            self, "allow_unmuteusers", Feature.AdminUnmute(general_parameter).is_enabled()
        )
        allow_recording_field = conditional_field(
            self, "allow_recording", Feature.Recordings(general_parameter).is_enabled()
        )
        dial_number_field = conditional_field(
            self, "dialNumber", Feature.MeetingDialNumber(general_parameter).is_enabled()
        )
        streaming_url_field = conditional_field(
            self,
            "streamingUrl",
            Feature.MeetingRTMPStreaming(general_parameter).is_enabled(),
            crispy_attributes={"placeholder": "rtmp://streaming.server.tld/url/key"},
        )

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            Field("name"),
            Field("mute_on_start"),
            Field("moderation_mode"),
            Field("meetingLayout"),
            Field("everyone_can_start"),
            Field("authenticated_user_can_start"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            allow_unmuteusers_field,
            Field("allow_ejectcam"),
            Field("allow_learningdashboard"),
            allow_recording_field,
            Field("allow_guest_entry"),
            Field("access_code"),
            Field("only_prompt_guests_for_access_code"),
            Field("guest_policy"),
            Field("welcome_message"),
            Field("logoutUrl"),
            dial_number_field,
            Field("maxParticipants"),
            streaming_url_field,
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class SchedulingStrategyForm(forms.ModelForm):
    class Meta:
        model = SchedulingStrategy
        fields = [
            "name",
            "scheduling_strategy",
            "description",
            "notifications_emails",
            "tenants",
        ]

    tenants = forms.ModelMultipleChoiceField(queryset=Site.objects.all(), required=True)

    def __init__(self, *args, **kwargs):
        super(SchedulingStrategyForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["tenants"].label = _("Tenants")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        self.fields["description"].label = _("Description")
        self.fields["notifications_emails"].label = _("Email Notifications")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("scheduling_strategy"),
            Field("tenants"),
            Field("description"),
            Field("notifications_emails"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class AgentConfigurationForm(forms.ModelForm):
    class Meta:
        model = AgentConfiguration
        fields = [
            "registration_interval",
            "stats_interval",
        ]

    def __init__(self, *args, scheduling_strategy: SchedulingStrategy, **kwargs):
        super(AgentConfigurationForm, self).__init__(*args, **kwargs)
        self.scheduling_strategy = scheduling_strategy

        self.fields["registration_interval"].label = _("Registration interval")
        self.fields["stats_interval"].label = _("Stats interval")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("registration_interval"),
            Field("stats_interval"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def _save_m2m(self):
        super()._save_m2m()

        if hasattr(self.instance, "schedulingstrategy"):
            scheduling_strategy = self.instance.schedulingstrategy

            if scheduling_strategy == self.scheduling_strategy:
                return

            scheduling_strategy.agent_configuration = None
            scheduling_strategy.save(update_fields=["agent_configuration"])

        self.scheduling_strategy.agent_configuration = self.instance
        self.scheduling_strategy.save(update_fields=["agent_configuration"])


class EndTimeWidget(forms.MultiWidget):
    def __init__(self, **kwargs):
        super().__init__([forms.TimeInput(attrs={"type": "time"}), forms.NumberInput(), forms.HiddenInput()])

    def decompress(self, value):
        if value is None:
            return [None, None, None]
        return [None, value, int((datetime.utcnow() + timedelta(minutes=value)).timestamp())]


MAX_MEETING_DURATION = 12 * 60
MIN_MEETING_DURATION = 5


class EndTimeField(forms.MultiValueField):
    def __init__(self, **kwargs):
        fields = (forms.TimeField(), forms.IntegerField(), forms.IntegerField())
        super().__init__(fields=fields, widget=EndTimeWidget(), **kwargs)

    def compress(self, data_list):
        if len(data_list) < 3:
            return None
        meeting_end_time = datetime.utcfromtimestamp(data_list[2])
        current_time = datetime.utcnow()
        meeting_duration = round((meeting_end_time - current_time).total_seconds() / 60)
        if current_time > meeting_end_time:
            raise ValidationError(
                _("Meeting ends in the past"),
                code="invalid",
            )
        if MIN_MEETING_DURATION > meeting_duration:
            raise ValidationError(
                _("Meeting duration is too short (min 5m)"),
                code="invalid",
            )
        if MAX_MEETING_DURATION < meeting_duration:
            raise ValidationError(
                _("Meeting duration is too long (max 12h)"),
                code="invalid",
            )

        return meeting_duration


class ConfigureMeetingForm(forms.ModelForm):
    class Meta:
        model = MeetingConfiguration
        fields = [
            "mute_on_start",
            "moderation_mode",
            "meetingLayout",
            "allow_guest_entry",
            "access_code",
            "only_prompt_guests_for_access_code",
            "disable_mic",
            "disable_cam",
            "allow_recording",
            "disable_note",
            "disable_private_chat",
            "disable_public_chat",
            "allow_unmuteusers",
            "allow_ejectcam",
            "allow_learningdashboard",
            "guest_policy",
            "welcome_message",
            "logoutUrl",
            "dialNumber",
            "maxParticipants",
            "streamingUrl",
            "meeting_duration_time",
            "slides",
            "initial_slides",
        ]

    def __init__(
        self,
        *args,
        enforced_slides: Iterable[Slides],
        initial_slides: Optional[Slides],
        requesting_user: Union[User, AnonymousUser],
        tenant: Site,
        general_parameter: GeneralParameter,
        **kwargs,
    ):
        if Feature.Media(general_parameter).is_enabled():
            self.enforced_slides = enforced_slides
            self.initial_slides = initial_slides
        else:
            self.enforced_slides = None
            self.initial_slides = None

        super(ConfigureMeetingForm, self).__init__(*args, **kwargs)

        self.fields["mute_on_start"].label = _("Everyone is muted on start")
        self.fields["moderation_mode"].label = _("Moderation mode")
        self.fields["meetingLayout"].label = _("Meeting layout")
        self.fields["allow_guest_entry"].label = _("Allow guests")
        self.fields["access_code"].label = _("Access code")
        self.fields["only_prompt_guests_for_access_code"].label = _("Only prompt guests for access code")
        self.fields["disable_cam"].label = _("Disable camera for non-moderators")
        self.fields["disable_mic"].label = _("Disable microphone for non-moderators")
        self.fields["allow_unmuteusers"].label = _("Allow Moderators to unmute Users")
        self.fields["allow_ejectcam"].label = _("Allow Moderators to eject Users-Webcams")
        self.fields["allow_learningdashboard"].label = _("Enable Learning Dashboard")
        self.fields["allow_recording"].label = _("Allow recording")
        self.fields["disable_note"].label = _("Disable shared notes - editing only for moderators")
        self.fields["disable_public_chat"].label = _("Disable public chat - editing only for moderators")
        self.fields["disable_private_chat"].label = _("Disable private chat - editing only for moderators")
        self.fields["guest_policy"].label = _("Entry policy for non-moderators")
        self.fields["logoutUrl"].label = _("Logout URL")
        self.fields["dialNumber"].label = _("Dial number")
        self.fields["welcome_message"].label = _("Welcome message")
        self.fields["welcome_message"].help_text = _("Max. 255 characters. Limited support of HTML.")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")
        self.fields["meeting_duration_time"] = EndTimeField(require_all_fields=False, required=False)
        self.fields["meeting_duration_time"].label = _("End meeting in")
        self.fields["meeting_duration_time"].help_text = _("Duration in minutes")

        allow_unmuteusers_field = conditional_field(
            self, "allow_unmuteusers", Feature.AdminUnmute(general_parameter).is_enabled()
        )
        allow_recording_field = conditional_field(
            self, "allow_recording", Feature.Recordings(general_parameter).is_enabled()
        )
        dial_number_field = conditional_field(
            self, "dialNumber", Feature.MeetingDialNumber(general_parameter).is_enabled()
        )
        streaming_url_field = conditional_field(
            self,
            "streamingUrl",
            Feature.MeetingRTMPStreaming(general_parameter).is_enabled(),
            crispy_attributes={"placeholder": "rtmp://streaming.server.tld/url/key"},
        )
        slides_field, initial_slides_field = init_slides_fields(self, general_parameter, requesting_user, tenant, True)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            Field("mute_on_start"),
            Field("moderation_mode"),
            Field("meetingLayout"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            allow_unmuteusers_field,
            Field("allow_ejectcam"),
            Field("allow_learningdashboard"),
            allow_recording_field,
            Field("allow_guest_entry"),
            Field("access_code"),
            Field("only_prompt_guests_for_access_code"),
            Field("guest_policy"),
            Field("welcome_message"),
            Field("logoutUrl"),
            dial_number_field,
            Field("maxParticipants"),
            Field("meeting_duration_time"),
            streaming_url_field,
            slides_field,
            initial_slides_field,
        )

    @classmethod
    def create(
        cls,
        tenant: Site,
        general_parameter: GeneralParameter,
        room: Room,
        requesting_user: Union[AnonymousUser, User],
        *,
        data: Optional[dict] = None,
    ) -> "ConfigureMeetingForm":
        enforced_slides = general_parameter.slides.union(room.slides.all())
        default_meeting_configuration = room.default_meeting_configuration.instantiate(commit=False)

        kwargs = {
            "enforced_slides": enforced_slides,
            "initial_slides": room.initial_slides or general_parameter.initial_slides or None,
            "requesting_user": requesting_user,
            "tenant": tenant,
            "general_parameter": general_parameter,
        }

        if data is not None:
            kwargs["data"] = data
        else:
            if general_parameter.home_room_last_used_config_enabled and room.is_homeroom():
                last_configuration = (
                    MeetingConfiguration.objects.filter(meeting__room=room).order_by("-meeting__started").first()
                )
                slides = list(last_configuration.slides.filter(owner=requesting_user)) if last_configuration else []

                meeting_configuration = last_configuration or default_meeting_configuration
                meeting_configuration.pk = None
                meeting_configuration.id = None
                meeting_configuration._state.adding = True
                meeting_configuration.meeting_duration_time = None

                if (
                    meeting_configuration.initial_slides
                    and meeting_configuration.initial_slides.owner != requesting_user
                ):
                    meeting_configuration.initial_slides = None

                if slides:
                    kwargs.setdefault("initial", dict())
                    # If slides exists, force it to list in case it is a QuerySet.
                    kwargs["initial"]["slides"] = list(kwargs["initial"].get("slides", []))
                    kwargs["initial"]["slides"].extend(slides)
            else:
                meeting_configuration = default_meeting_configuration

            kwargs["instance"] = meeting_configuration

        return cls(**kwargs)

    def _post_clean(self):
        super(ConfigureMeetingForm, self)._post_clean()

        if self.initial_slides and not self.instance.initial_slides:
            self.instance.initial_slides = self.initial_slides

    def _save_m2m(self):
        super(ConfigureMeetingForm, self)._save_m2m()
        if self.enforced_slides:
            self.instance.slides.add(*self.enforced_slides)


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RecordingForm(forms.ModelForm):
    class Meta:
        model = Meeting
        fields = ["replay_title"]

    def __init__(self, *args, **kwargs):
        super(RecordingForm, self).__init__(*args, **kwargs)

        self.fields["replay_title"].label = _("Title")
        self.fields["replay_title"].required = True

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("replay_title"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserSettings(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            "theme",
            "bbb_auto_join_audio",
            "bbb_listen_only_mode",
            "bbb_skip_check_audio",
            "bbb_skip_check_audio_on_first_join",
            "bbb_auto_share_webcam",
            "bbb_record_video",
            "bbb_skip_video_preview",
            "bbb_skip_video_preview_on_first_join",
            "bbb_mirror_own_webcam",
            "bbb_force_restore_presentation_on_new_events",
            "bbb_auto_swap_layout",
            "bbb_show_participants_on_login",
            "bbb_show_public_chat_on_login",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["theme"].label = _("Change Theme")
        self.fields["theme"].empty_label = None
        self.fields["bbb_auto_join_audio"].label = _("Auto join audio")
        self.fields["bbb_listen_only_mode"].label = _("Listen only mode")
        self.fields["bbb_skip_check_audio"].label = _("Skip check audio")
        self.fields["bbb_skip_check_audio_on_first_join"].label = _("Skip check audio on first join")
        self.fields["bbb_auto_share_webcam"].label = _("Auto share webcam")
        self.fields["bbb_record_video"].label = _("Record video")
        self.fields["bbb_skip_video_preview"].label = _("Skip video preview")
        self.fields["bbb_skip_video_preview_on_first_join"].label = _("Skip video preview on first join")
        self.fields["bbb_mirror_own_webcam"].label = _("Mirror own webcam")
        self.fields["bbb_force_restore_presentation_on_new_events"].label = _(
            "Force restore presentation on new events"
        )
        self.fields["bbb_auto_swap_layout"].label = _("Auto swap layout")
        self.fields["bbb_show_participants_on_login"].label = _("Show participants on first join")
        self.fields["bbb_show_public_chat_on_login"].label = _("Show public chat on first join")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("theme"),
            Field("bbb_auto_join_audio"),
            Field("bbb_listen_only_mode"),
            Field("bbb_skip_check_audio"),
            Field("bbb_skip_check_audio_on_first_join"),
            Field("bbb_auto_share_webcam"),
            Field("bbb_record_video"),
            Field("bbb_skip_video_preview"),
            Field("bbb_skip_video_preview_on_first_join"),
            Field("bbb_mirror_own_webcam"),
            Field("bbb_force_restore_presentation_on_new_events"),
            Field("bbb_auto_swap_layout"),
            Field("bbb_show_participants_on_login"),
            Field("bbb_show_public_chat_on_login"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserForm(UserSettings):
    class Meta(UserSettings.Meta):
        fields = [
            "username",
            "first_name",
            "last_name",
            "email",
            "groups",
            "is_staff",
            "personal_rooms_max_number",
            *UserSettings.Meta.fields,
        ]

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["username"].label = _("Username")
        # only display username if it has no usable password (e.i. the user is from OIDC)
        if not self.instance.has_usable_password():
            self.fields["username"].disabled = True
            self.fields["username"].readonly = True
            self.fields["username"].validators = []
            self.fields["username"].help_text = ""
        self.fields["first_name"].label = _("First name")
        self.fields["last_name"].label = _("Last name")
        self.fields["groups"].label = _("Groups")
        self.fields["is_staff"].label = _("Staff status")
        self.fields["email"].label = _("Email")
        self.fields["personal_rooms_max_number"].label = _("Maximum number of personal rooms")

        self.helper.layout.fields = [
            Field("username"),
            Field("first_name"),
            Field("last_name"),
            Field("email"),
            Field("groups"),
            Field("is_staff"),
            Field("personal_rooms_max_number"),
            *self.helper.layout.fields,
        ]

    def _get_validation_exclusions(self):
        exclude = super()._get_validation_exclusions()

        # exclude the username validation if it has no usable password (e.i. the user is from OIDC)
        if not self.instance.has_usable_password():
            exclude.append("username")

        return exclude


class UserFormWithPassword(UserForm):
    class Meta(UserForm.Meta):
        fields = [
            "tenant",
            "password",
            *UserForm.Meta.fields,
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["tenant"].disabled = True

        self.fields["password"].label = _("Password")

        self.helper.layout.fields.insert(1, Field("password"))

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
            self._save_m2m()
        return user


class SetPasswordForm(DjangoSetPasswordForm):
    username = forms.CharField(label=_("Username"), disabled=True)

    def __init__(self, *args, **kwargs):
        super(SetPasswordForm, self).__init__(*args, **kwargs)
        self.initial["username"] = self.user.username

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("username"),
            Field("new_password1"),
            Field("new_password2"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PasswordChangeForm(DjangoPasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("old_password"),
            Field("new_password1"),
            Field("new_password2"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PasswordResetForm(forms.Form):
    username = forms.CharField(label=_("Username"), max_length=511)

    def __init__(self, *args, tenant: Site, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)

        self.tenant = tenant

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("username", autocomplete="username"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def save(self, build_password_reset_url: Callable[[int, str], str], from_email: str) -> None:
        username = self.cleaned_data["username"]
        user = User.objects.filter(username=username, tenant=self.tenant, is_active=True).first()

        if not user:
            logger.debug(f"Attempt to reset password for non-existing user {username}.")
            return
        if not user.has_usable_password():
            logger.info(
                f"Attempt to reset password for user {username} without password. "
                "The user is probably managed by an external service."
            )
            return
        if not user.email:
            logger.debug(f"Attempt to reset password for user {username} without an email address.")
            return

        subject = _("Password reset on %s") % self.tenant.domain
        body = loader.render_to_string(
            "password_reset_email_body.txt",
            {
                "display_name": str(user),
                "domain": self.tenant.domain,
                "reset_url": build_password_reset_url(user.id, default_token_generator.make_token(user)),
            },
        ).strip()

        send_mail(subject, body, from_email, [user.email])


class TenantForm(forms.ModelForm):
    class Meta:
        model = Site
        fields = ["domain", "name"]

    def __init__(self, *args, **kwargs):
        super(TenantForm, self).__init__(*args, **kwargs)

        self.fields["domain"].label = _("Domain")
        self.fields["name"].label = _("Name")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("domain"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class ApiTokenForm(forms.ModelForm):
    class Meta:
        model = ApiToken
        fields = ["name", "tenants", "scheduling_strategy"]

    tenants = forms.ModelMultipleChoiceField(queryset=Site.objects.all(), required=True)

    def __init__(self, *args, general_parameter: GeneralParameter, **kwargs):
        super(ApiTokenForm, self).__init__(*args, **kwargs)
        self.fields["name"].label = _("Name")
        self.fields["tenants"].label = _("Tenants")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        scheduling_strategy_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.scheduling_strategy_id:
            scheduling_strategy_filter |= Q(id=self.instance.scheduling_strategy_id)
        self.fields["scheduling_strategy"].queryset = SchedulingStrategy.objects.filter(
            scheduling_strategy_filter
        ).distinct()

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("tenants"),
            Field("scheduling_strategy"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class FileMaxSizeValidator:
    message = _("Ensure this file does not exceed the size of %(max_size)s (it has %(size)s).")
    code = "max_size"
    max_size = 10

    def __init__(self, max_size=None, message=None, code=None):
        self.max_size = max_size
        if message is not None:
            self.message = message
        if code is not None:
            self.code = code

    def __call__(self, value):
        if self.max_size is not None and value.size > self.max_size:
            raise ValidationError(
                self.message,
                code=self.code,
                params={
                    "size": str(IntRepresentedAsByteSize(value.size)),
                    "max_size": str(IntRepresentedAsByteSize(self.max_size)),
                    "value": value,
                },
            )


class SlidesForm(forms.ModelForm):
    class Meta:
        model = Slides
        fields = [
            "tenant",
            "owner",
            "name",
            "file",
        ]

    def __init__(self, *args, slides_max_upload_size: int, **kwargs):
        super().__init__(*args, **kwargs)

        self.initial_file_path = self.instance.file.path if self.instance.file else None

        self.fields["tenant"].disabled = True
        self.fields["owner"].disabled = True

        self.fields["name"].label = _("Name")

        allowed_extensions = ["doc", "docx", "jpeg", "jpg", "pdf", "png", "pptx", "txt"]
        self.fields["file"].label = _("File")
        self.fields["file"].widget.attrs["accept"] = ",".join(f".{extension}" for extension in allowed_extensions)
        self.fields["file"].validators.append(FileExtensionValidator(allowed_extensions=allowed_extensions))
        self.fields["file"].validators.append(FileMaxSizeValidator(slides_max_upload_size))

        self.helper = FormHelper()
        self.helper.form_id = "id-slides-form"
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("file", template="clearable_file_input.html"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def save(self, commit=True):
        super().save(commit)

        if commit and self.initial_file_path and "file" in self.changed_data:
            mark_slides_file_for_removal(path=self.initial_file_path)

        return self.instance


class SlidesAdminForm(SlidesForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["owner"].label = _("Owner")
        self.fields["owner"].disabled = False

        self.helper.layout.fields.insert(0, Field("owner"))
