import json
import logging
from typing import Any, Mapping, Optional, Union

import requests
import six
from core.utils import get_permission, get_tenant, load_moderator_group
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.contrib.sites.models import Site
from django.core.exceptions import SuspiciousOperation
from django.db.models import QuerySet
from django.http import HttpRequest
from django.urls import reverse
from django.utils.encoding import force_bytes, smart_bytes, smart_text
from josepy.b64 import b64decode
from josepy.jwk import JWK
from josepy.jws import JWS, Header
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.utils import absolutify
from oidc_authentication.models import AuthParameter
from requests.auth import HTTPBasicAuth

LOGGER = logging.getLogger(__name__)


class TenantOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    def __init__(self, *_: Any, **__: Any) -> None:
        self.request = None
        self.UserModel = get_user_model()

        self._cached_support_users_permission = get_permission("support_chat.support_users")

    @staticmethod
    def get_settings(attr: str, *args: Any) -> Any:
        LOGGER.debug("Retrieving the non-tenant-based %s.", attr)
        return OIDCAuthenticationBackend.get_settings(attr, *args)

    def filter_users_by_claims(self, claims: dict) -> QuerySet[AbstractUser]:
        """
        Return all users matching the specified username
        If no user is found create_user will be called
        If more than one user (that is two or more user with the
        same username) are found, an error will occur
        If exactly one user is found, update_user will be called
        """
        username = claims.get("username")
        if not username:
            return self.UserModel.objects.none()
        return self.UserModel.objects.filter(username=username)

    def verify_claims(self, claims: dict, necessary_claim: Optional[str] = None) -> bool:
        return not necessary_claim or necessary_claim in claims

    def create_user(self, claims: dict, tenant: Optional[Site] = None) -> AbstractUser:
        return self.update_user(
            self.UserModel.objects.create_user(claims.get("username", ""), tenant=tenant), claims, tenant
        )

    def update_user(self, user: AbstractUser, claims: dict, tenant: Optional[Site] = None) -> AbstractUser:
        groups = claims.get("groups", [])

        user.is_superuser = "superuser" in groups
        user.is_staff = "staff" in groups
        user.first_name = claims.get("given_name", "")
        user.email = claims.get("email", "")
        user.last_name = claims.get("family_name", "")
        user.username = claims.get("username", "")
        user.display_name = claims.get("name", "")
        user.save()

        mod_group = load_moderator_group(tenant)

        if "moderator" in groups:
            mod_group.user_set.add(user)
        else:
            mod_group.user_set.remove(user)
        if "supporter" in groups:
            user.user_permissions.add(self._cached_support_users_permission)
        else:
            user.user_permissions.remove(self._cached_support_users_permission)
        return user

    def _verify_jws(
        self, payload: bytes, key: Union[str, Mapping[str, Any]], auth_parameter: Optional[AuthParameter] = None
    ) -> bytes:
        """Verify the given JWS payload with the given key and return the payload"""
        jws = JWS.from_compact(payload)

        if not auth_parameter:
            msg = "No authentication parameters where provided."
            raise SuspiciousOperation(msg)
        try:
            alg = jws.signature.combined.alg.name
        except KeyError:
            msg = "No alg value found in header"
            raise SuspiciousOperation(msg)

        if alg != auth_parameter.sign_algo:
            msg = "The provider algorithm {!r} does not match the client's OIDC_RP_SIGN_ALGO.".format(alg)
            raise SuspiciousOperation(msg)

        if isinstance(key, six.string_types):
            # Use smart_bytes here since the key string comes from settings.
            jwk = JWK.load(smart_bytes(key))
        else:
            # The key is a json returned from the IDP JWKS endpoint.
            jwk = JWK.from_json(key)

        if not jws.verify(jwk):
            msg = "JWS token verification failed."
            raise SuspiciousOperation(msg)

        return jws.payload

    def retrieve_matching_jwk(self, token: bytes, auth_parameter: Optional[AuthParameter] = None) -> Optional[str]:
        """Get the signing key by exploring the JWKS endpoint of the OP."""
        if not auth_parameter:
            msg = "No authentication parameters where provided."
            raise SuspiciousOperation(msg)
        response_jwks = requests.get(
            auth_parameter.jwks_endpoint,
            verify=auth_parameter.verify_ssl,
            timeout=auth_parameter.timeout,
            proxies=auth_parameter.proxy,
        )
        response_jwks.raise_for_status()
        jwks = response_jwks.json()

        # Compute the current header from the given token to find a match
        jws = JWS.from_compact(token)
        json_header = jws.signature.protected
        header = Header.json_loads(json_header)

        key = None
        for jwk in jwks["keys"]:
            if jwk["kid"] != smart_text(header.kid):
                continue
            if "alg" in jwk and jwk["alg"] != smart_text(header.alg):
                continue
            key = jwk
        if key is None:
            raise SuspiciousOperation("Could not find a valid JWKS.")
        return key

    def get_payload_data(self, token: bytes, key: str, auth_parameter: Optional[AuthParameter] = None) -> bytes:
        """Helper method to get the payload of the JWT token."""
        if not auth_parameter:
            msg = "No authentication parameters where provided."
            raise SuspiciousOperation(msg)
        if auth_parameter.allow_unsecured_jwt:
            header, payload_data, signature = token.split(b".")
            header = json.loads(smart_text(b64decode(header)))

            # If config allows unsecured JWTs check the header and return the decoded payload
            if "alg" in header and header["alg"] == "none":
                return b64decode(payload_data)

        # By default fallback to verify JWT signatures
        return self._verify_jws(token, key, auth_parameter)

    def verify_token(
        self, token: Union[str, bytes], auth_parameter: Optional[AuthParameter] = None, **kwargs: Any
    ) -> dict:
        """Validate the token signature."""
        if not auth_parameter:
            msg = "No authentication parameters where provided."
            raise SuspiciousOperation(msg)
        nonce = kwargs.get("nonce")

        token = force_bytes(token)
        if auth_parameter.sign_algo.startswith("RS"):
            if auth_parameter.idp_sign_key is not None:
                key = auth_parameter.idp_sign_key
            else:
                key = self.retrieve_matching_jwk(token, auth_parameter)
        else:
            key = auth_parameter.client_secret

        payload_data = self.get_payload_data(token, key, auth_parameter)

        # The 'token' will always be a byte string since it's
        # the result of base64.urlsafe_b64decode().
        # The payload is always the result of base64.urlsafe_b64decode().
        # In Python 3 and 2, that's always a byte string.
        # In Python3.6, the json.loads() function can accept a byte string
        # as it will automagically decode it to a unicode string before
        # deserializing https://bugs.python.org/issue17909
        payload = json.loads(payload_data.decode("utf-8"))
        token_nonce = payload.get("nonce")

        if auth_parameter.use_nonce and nonce != token_nonce:
            msg = "JWT Nonce verification failed."
            raise SuspiciousOperation(msg)
        return payload

    def get_token(self, payload: dict, auth_parameter: Optional[AuthParameter] = None) -> dict:
        """Return token object as a dictionary."""
        if not auth_parameter:
            msg = "No authentication parameters where provided."
            raise SuspiciousOperation(msg)
        auth = None
        if auth_parameter.token_use_basic_auth:
            # When Basic auth is defined, create the Auth Header and remove secret from payload.
            user = payload.get("client_id")
            pw = payload.get("client_secret")

            auth = HTTPBasicAuth(user, pw)
            del payload["client_secret"]

        response = requests.post(
            auth_parameter.token_endpoint,
            data=payload,
            auth=auth,
            verify=auth_parameter.verify_ssl,
            timeout=auth_parameter.timeout,
            proxies=auth_parameter.proxy,
        )
        response.raise_for_status()
        return response.json()

    def get_userinfo(
        self, access_token: str, id_token: str, payload: dict, auth_parameter: Optional[AuthParameter] = None
    ) -> dict:
        """Return user details dictionary. The id_token and payload are not used in
        the default implementation, but may be used when overriding this method"""
        if not auth_parameter:
            msg = "No authentication parameters where provided."
            raise SuspiciousOperation(msg)
        user_response = requests.get(
            auth_parameter.user_endpoint,
            headers={"Authorization": "Bearer {0}".format(access_token)},
            verify=auth_parameter.verify_ssl,
            timeout=auth_parameter.timeout,
            proxies=auth_parameter.proxy,
        )
        user_response.raise_for_status()
        return user_response.json()

    def authenticate(self, request: HttpRequest, **kwargs: Any) -> Optional[AbstractUser]:
        """Authenticates a user based on the OIDC code flow."""
        self.request = request
        if not self.request:
            return None

        tenant = get_tenant(self.request)
        if not tenant:
            return None

        try:
            auth_parameter = tenant.auth_parameter
        except Site.auth_parameter.RelatedObjectDoesNotExist:
            return None

        state = self.request.GET.get("state")
        code = self.request.GET.get("code")
        nonce = kwargs.pop("nonce", None)

        if not code or not state:
            return None

        token_payload = {
            "client_id": auth_parameter.client_id,
            "client_secret": auth_parameter.client_secret,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": absolutify(self.request, reverse("oidc_authentication_callback")),
        }

        # Get the token
        token_info = self.get_token(token_payload, auth_parameter)
        id_token = token_info.get("id_token")
        access_token = token_info.get("access_token")

        # Validate the token
        payload = self.verify_token(id_token, auth_parameter, nonce=nonce)

        if payload:
            self.store_tokens(access_token, id_token)
            try:
                return self.get_or_create_user(access_token, id_token, payload, tenant, auth_parameter)
            except SuspiciousOperation as exc:
                LOGGER.warning("failed to get or create user: %s", exc)
                return None

        return None

    def get_or_create_user(
        self,
        access_token: str,
        id_token: str,
        payload: dict,
        tenant: Optional[Site] = None,
        auth_parameter: Optional[AuthParameter] = None,
    ) -> Optional[AbstractUser]:
        """Returns a User instance if 1 user is found. Creates a user if not found
        and configured to do so. Returns nothing if multiple users are matched."""

        user_info = self.get_userinfo(access_token, id_token, payload, auth_parameter)

        claims_verified = self.verify_claims(
            user_info,
            necessary_claim=auth_parameter.claim_unique_username_field
            if not auth_parameter.oidc_username_standard
            else None,
        )
        if not claims_verified:
            msg = "Claims verification failed"
            raise SuspiciousOperation(msg)

        user_info["username"] = (
            "{}@{}".format(payload["sub"], payload["iss"])
            if auth_parameter.oidc_username_standard
            else "{}@{}".format(user_info[auth_parameter.claim_unique_username_field], tenant.domain)
        )
        users = self.filter_users_by_claims(user_info)
        if "preferred_username" in user_info and len(users) == 0:
            migrated_user = self.migrate_user(user_info["preferred_username"], user_info["username"])
            if migrated_user is not None:
                users = [migrated_user]

        if len(users) == 1:
            return self.update_user(users[0], user_info, tenant)
        elif len(users) > 1:
            # In the rare case that two user accounts have the same email address,
            # bail. Randomly selecting one seems really wrong.
            msg = "Multiple users returned"
            raise SuspiciousOperation(msg)
        elif auth_parameter.create_user:
            user = self.create_user(user_info, tenant)
            return user
        else:
            LOGGER.debug(
                "Login failed: No user with the id %s found, and OIDC_CREATE_USER is False", user_info["username"]
            )
            return None

    def migrate_user(self, old_username: str, new_username: str) -> Optional[AbstractUser]:
        user = self.UserModel.objects.filter(username=old_username).first()
        if user:
            user.username = new_username
            user.save()
            return user
        return None


def provider_logout(request: HttpRequest) -> str:
    logout_endpoint = get_tenant(request).auth_parameter.end_session_endpoint
    return logout_endpoint + "?redirect_uri=" + request.build_absolute_uri("/")
