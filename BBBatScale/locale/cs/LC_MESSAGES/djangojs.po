# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-25 13:40+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n "
"<= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: core/static/home/js/updateRooms.js:206
msgid "active"
msgstr ""

#: core/static/home/js/updateRooms.js:209
msgid "inactive"
msgstr ""

#: core/static/home/js/updateRooms.js:212
msgid "creating"
msgstr ""

#: static/utilities/js/misc.js:160
msgid "The URL has successfully been copied to the clipboard."
msgstr ""

#: static/utilities/js/misc.js:161
msgid "Your Browser does not support copying to the clipboard."
msgstr ""

#: static/utilities/js/pagination.js:153
msgid "No results have been found"
msgstr ""

#: static/utilities/js/pagination.js:167
msgid "Previous"
msgstr ""

#: static/utilities/js/pagination.js:180
msgid "Page number"
msgstr ""

#: static/utilities/js/pagination.js:192
msgid "Number of pages"
msgstr ""

#: static/utilities/js/pagination.js:197
msgid "Next"
msgstr ""

#: static/utilities/js/slides.js:10 static/utilities/js/slides.js:16
msgid "Search"
msgstr ""

#: static/utilities/js/slides.js:20
msgid "Selectable"
msgstr ""

#: static/utilities/js/slides.js:24
msgid "Selected"
msgstr ""

#: static/utilities/js/slides.js:74
msgid "Show"
msgstr ""

#: static/utilities/js/slides.js:88
msgid "Set as initial slides"
msgstr ""
