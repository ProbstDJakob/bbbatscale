"""
ASGI config for BBBatScale project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os

import django
from channels.routing import get_default_application

from BBBatScale.settings import default_settings_module

os.environ.setdefault("DJANGO_SETTINGS_MODULE", default_settings_module)
django.setup()

application = get_default_application()
