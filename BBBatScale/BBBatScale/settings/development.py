import os

os.environ.setdefault("DJANGO_SECRET_KEY", "DUMMY_DJANGO_SECRET_KEY")
os.environ.setdefault("RECORDINGS_SECRET", "DUMMY_RECORDINGS_SECRET")
os.environ.setdefault("BBBATSCALE_MEDIA_SECRET_KEY", "DUMMY_BBBATSCALE_MEDIA_SECRET_KEY")
os.environ.setdefault("POSTGRES_DB", "bbbatscale")
os.environ.setdefault("POSTGRES_USER", "bbbatscale")
os.environ.setdefault("POSTGRES_PASSWORD", "DUMMY_POSTGRES_PASSWORD")
os.environ.setdefault("POSTGRES_HOST", "localhost")
os.environ.setdefault("REDIS_HOST", "localhost")

from BBBatScale.settings.base import *  # noqa: F401,F403,E402

DEBUG = True
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
