/**
 * @typedef {"ASCENDING"|"DESCENDING"|null} SortingState
 */

/**
 * @typedef {Function} ChangeSortingFunction
 * @param {boolean} [reload=true]
 * @return {void}
 */

/**
 * @typedef {Object} _SortableTableHead
 * @property {HTMLTableCellElement} element
 * @property {SortingState} state
 * @property {ChangeSortingFunction} clearSorting
 * @property {ChangeSortingFunction} sortAscending
 * @property {ChangeSortingFunction} sortDescending
 */

/**
 * @typedef {Object} SortableTableHead
 * @property {HTMLTableCellElement} element
 * @property {SortingState} state
 * @property {ChangeSortingFunction} clearSorting
 * @property {ChangeSortingFunction} sortAscending
 * @property {ChangeSortingFunction} sortDescending
 */

/**
 * @callback SortingChangedCallback
 * @this Paginator
 * @param {SortingState} state
 * @return {void}
 */

/**
 * @callback GetUrlCallback
 * @this Paginator
 * @param {number} page
 * @return {string}
 */

/**
 * @callback RenderHeadCallback
 * @this Paginator
 * @return {HTMLTableRowElement}
 */

/**
 * @callback RenderEntryCallback
 * @this Paginator
 * @param {unknown} entry
 * @return {HTMLTableRowElement}
 */

/**
 * @callback PageLoadedCallback
 * @this Paginator
 * @param {number} page
 * @param {number} pageCount
 * @param {number} totalCount
 * @param {Array<unknown>} entries
 * @return {void}
 */

/**
 * @param {GetUrlCallback} getUrl The callback to generate a URL based on the selected page.
 * @param {RenderHeadCallback} renderHead The callback to render the table head.
 * @param {RenderEntryCallback} renderEntry The callback to render an entry.
 * @param {?PageLoadedCallback} pageLoadedCallback The callback to be called after a page
 * load has successfully been processed.
 * @returns {Paginator}
 * @constructor
 */
function Paginator({ getUrl, renderHead, renderEntry, pageLoadedCallback = undefined }) {
    if (!(this instanceof Paginator)) {
        return new Paginator({ getUrl, renderHead, renderEntry, pageLoadedCallback });
    }
    /**
     * @type {Paginator}
     */
    const self = this;

    /**
     * @readonly
     * @type {HTMLDivElement}
     */
    this.container = document.createElement("div");

    /**
     * @type {number}
     */
    let page = 1;

    /**
     * @type {?AbortController}
     */
    let loadPageController = null;
    /**
     * @type {Array<_SortableTableHead>}
     */
    let exclusiveSortableTableHeads = [];
    /**
     * @type {HTMLTableElement}
     */
    const table = document.createElement("table");
    /**
     * @type {HTMLTableSectionElement}
     */
    const tableHead = document.createElement("thead");
    /**
     * @type {HTMLTableSectionElement}
     */
    const tableBody = document.createElement("tbody");
    /**
     * @type {HTMLSpanElement}
     */
    const noResultsLabel = document.createElement("span");
    /**
     * @type {HTMLButtonElement}
     */
    const paginationPrevious = document.createElement("button");
    /**
     * @param {HTMLInputElement}
     */
    const paginationPage = document.createElement("input");
    /**
     * @param {HTMLSpanElement}
     */
    const paginationCount = document.createElement("span");
    /**
     * @param {HTMLButtonElement}
     */
    const paginationNext = document.createElement("button");

    /**
     * Constructor
     */
    {
        const tableContainer = document.createElement("div");
        tableContainer.classList.add("table-responsive");
        this.container.append(tableContainer);

        table.classList.add("table", "table-sm", "table-hover", "table-borderless", "mb-0");
        table.hidden = true;
        tableContainer.append(table);

        table.append(tableHead);
        exclusiveSortableTableHeads = [];

        table.append(tableBody);

        noResultsLabel.innerText = gettext("No results have been found");
        noResultsLabel.classList.add("text-center", "d-inline-block", "w-100");
        tableContainer.append(noResultsLabel);

        const paginationBar = document.createElement("div");
        paginationBar.classList.add("input-group", "justify-content-center", "mt-3");
        this.container.append(paginationBar);

        const paginationPrepend = document.createElement("div");
        paginationPrepend.classList.add("input-group-prepend");
        paginationBar.append(paginationPrepend);

        paginationPrevious.classList.add("btn", "btn-outline-secondary");
        paginationPrevious.type = "button";
        paginationPrevious.title = gettext("Previous");
        paginationPrevious.addEventListener("click", () => self.loadPage(page - 1));
        paginationPrepend.append(paginationPrevious);

        const paginationPreviousIcon = document.createElement("div");
        paginationPreviousIcon.classList.add("fas", "fa-backward");
        paginationPrevious.append(paginationPreviousIcon);

        paginationPage.classList.add("form-control", "flex-grow-0");
        paginationPage.type = "number";
        paginationPage.min = "1";
        paginationPage.style.minWidth = "5rem";
        paginationPage.placeholder = "";
        paginationPage.title = gettext("Page number");
        paginationPage.addEventListener("change", () =>
            self.loadPage(Math.floor(Number(paginationPage.value)) || 1)
        );
        paginationBar.append(paginationPage);

        const paginationAppend = document.createElement("div");
        paginationAppend.classList.add("input-group-append");
        paginationBar.append(paginationAppend);

        paginationCount.classList.add("input-group-text");
        paginationCount.style.cursor = "default";
        paginationCount.title = gettext("Number of pages");
        paginationAppend.append(paginationCount);

        paginationNext.classList.add("btn", "btn-outline-secondary");
        paginationNext.type = "button";
        paginationNext.title = gettext("Next");
        paginationNext.addEventListener("click", () => self.loadPage(page + 1));
        paginationAppend.append(paginationNext);

        const paginationNextIcon = document.createElement("div");
        paginationNextIcon.classList.add("fas", "fa-forward");
        paginationNext.append(paginationNextIcon);
    }

    /**
     * @param {number} pageNumber
     * @param {number} pageCount
     */
    function updatePagination(pageNumber, pageCount) {
        page = pageNumber;

        paginationPrevious.disabled = pageNumber <= 1;
        paginationCount.innerText = "/ " + pageCount.toFixed(0);
        paginationPage.value = pageNumber.toFixed(0);
        paginationPage.max = pageCount.toFixed(0);
        paginationNext.disabled = pageNumber >= pageCount;
    }

    /**
     * @param {string} name
     * @param {SortingChangedCallback?} sortingChangedCallback
     * @return {HTMLTableCellElement|_SortableTableHead}
     */
    function createTableHead(name, sortingChangedCallback) {
        const head = document.createElement("th");
        head.scope = "col";
        head.classList.add("align-middle");


        if (!sortingChangedCallback) {
            head.innerText = name;
            return head;
        }

        head.style.cursor = "pointer";

        const container = document.createElement("div");
        container.classList.add("d-flex", "flex-nowrap", "align-items-center");
        head.append(container);

        container.append(document.createTextNode(name));

        const sortIcon = document.createElement("em");
        sortIcon.classList.add("ml-1", "fas", "fa-sort");
        container.append(sortIcon);

        function getState() {
            if (sortIcon.classList.contains("fa-sort-up")) {
                return "ASCENDING";
            } else if (sortIcon.classList.contains("fa-sort-down")) {
                return "DESCENDING";
            } else {
                return null;
            }
        }

        return {
            element: head,
            get state() {
                return getState();
            },
            clearSorting(reload = true) {
                sortIcon.classList.remove("fa-sort-up", "fa-sort-down");
                sortIcon.classList.add("fa-sort");

                if (reload) {
                    self.loadPage().then(() => sortingChangedCallback.call(self, getState()));
                }
            },
            sortAscending(reload = true) {
                sortIcon.classList.remove("fa-sort", "fa-sort-down");
                sortIcon.classList.add("fa-sort-up");

                if (reload) {
                    self.loadPage().then(() => sortingChangedCallback.call(self, getState()));
                }
            },
            sortDescending(reload = true) {
                sortIcon.classList.remove("fa-sort", "fa-sort-up");
                sortIcon.classList.add("fa-sort-down");

                if (reload) {
                    self.loadPage().then(() => sortingChangedCallback.call(self, getState()));
                }
            }
        };
    }

    /**
     * @param {string} name
     * @return {HTMLTableCellElement}
     */
    this.createTableHead = function(name) {
        return createTableHead(name);
    };

    /**
     *
     * @param {string} name
     * @param {SortingChangedCallback?} sortingChangedCallback
     * @return {SortableTableHead}
     */
    this.createSortableTableHead = function(name, sortingChangedCallback) {
        const sortableTableHead = createTableHead(name, sortingChangedCallback || (() => undefined));

        return {
            element: sortableTableHead.element,
            get state() {
                return sortableTableHead.state;
            },
            clearSorting(reload = true) {
                sortableTableHead.clearSorting(reload);
            },
            sortAscending(reload = true) {
                sortableTableHead.sortAscending(reload);
            },
            sortDescending(reload = true) {
                sortableTableHead.sortDescending(reload);
            }
        };
    };

    /**
     *
     * @param {string} name
     * @param {SortingChangedCallback?} sortingChangedCallback
     * @return {SortableTableHead}
     */
    this.createExclusiveSortableTableHead = function(name, sortingChangedCallback) {
        const sortableTableHead = createTableHead(name, sortingChangedCallback || (() => undefined));

        const exclusiveSortableTableHead = {
            element: sortableTableHead.element,
            get state() {
                return sortableTableHead.state;
            },
            clearSorting(reload = true) {
                sortableTableHead.clearSorting(reload);
            },
            sortAscending(reload = true) {
                exclusiveSortableTableHeads.forEach(element => element.clearSorting(false));
                sortableTableHead.sortAscending(reload);
            },
            sortDescending(reload = true) {
                exclusiveSortableTableHeads.forEach(element => element.clearSorting(false));
                sortableTableHead.sortDescending(reload);
            }
        };

        exclusiveSortableTableHeads.push(sortableTableHead);

        exclusiveSortableTableHead.element.addEventListener("click", function() {
            if (exclusiveSortableTableHead.state === "ASCENDING") {
                exclusiveSortableTableHead.sortDescending();
            } else {
                exclusiveSortableTableHead.sortAscending();
            }
        });

        return exclusiveSortableTableHead;
    };

    /**
     * @return {number}
     */
    this.getPageNumber = function() {
        return page;
    };

    /**
     * @param {number?} page
     * @param {boolean} [ignoreAbort=true]
     */
    this.loadPage = async function(page, ignoreAbort = true) {
        try {
            loadPageController?.abort();
            loadPageController = new AbortController();

            const response = await fetch(getUrl.call(self, page || self.getPageNumber()), { signal: loadPageController.signal });
            /**
             * @type {{page: number, pageCount: number, totalCount: number, entries: Array<unknown>}}
             */
            const data = await response.json();
            const hasResults = data.pageCount !== 1 || data.entries.length !== 0;

            table.hidden = !hasResults;
            tableBody.innerText = "";
            if (hasResults) {
                noResultsLabel.classList.replace("d-inline-block", "d-none");
                tableBody.append(...data.entries.map((entry) => renderEntry.call(self, entry)));
            } else {
                noResultsLabel.classList.replace("d-none", "d-inline-block");
            }

            updatePagination(data.page, data.pageCount);

            pageLoadedCallback?.call(self, data.page, data.pageCount, data.totalCount, data.entries);
        } catch (e) {
            if (!ignoreAbort || !(e instanceof DOMException) || e.name !== "AbortError") {
                throw e;
            }
        }
    };

    /**
     * Constructor - post init
     */
    {
        tableHead.append(renderHead.call(self));
    }
}
