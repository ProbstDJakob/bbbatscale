# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-25 13:40+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n "
"<= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: support_chat/forms.py:33
#: support_chat/templates/support_chat/prepared_answers_overview.html:67
#: support_chat/templates/support_chat/prepared_answers_overview.html:115
msgid "Save"
msgstr ""

#: support_chat/models.py:22
msgid "Enable Support Chat"
msgstr ""

#: support_chat/models.py:23
msgid "Disable Support Chat if offline"
msgstr ""

#: support_chat/models.py:25
msgid "Allow supporter to inactivate all supporters"
msgstr ""

#: support_chat/models.py:27
msgid "Maximum message length"
msgstr ""

#: support_chat/models.py:120
msgid "Can support users"
msgstr ""

#: support_chat/models.py:126
msgid "Status active"
msgstr ""

#: support_chat/models.py:128
msgid "If enabled, the user could support other users in the support chat."
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:10
msgid "Prepared Answers"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:14
msgid "Create"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:39
msgid "Create Prepared Answer"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:50
#: support_chat/templates/support_chat/prepared_answers_overview.html:92
msgid "Title"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:58
#: support_chat/templates/support_chat/prepared_answers_overview.html:101
msgid "Text"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:80
msgid "Edit Prepared Answer"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:112
msgid "Delete"
msgstr ""

#: support_chat/templates/support_chat/settings.html:22
msgid "Settings"
msgstr ""

# TODO
#: support_chat/templates/support_chat/supporters_overview.html:13
msgid "Active Supporters"
msgstr ""

# TODO
#: support_chat/templates/support_chat/supporters_overview.html:36
msgid "Inactive Supporters"
msgstr ""
