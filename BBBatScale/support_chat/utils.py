from typing import TYPE_CHECKING, List, Union

from asgiref.sync import sync_to_async
from channels.layers import BaseChannelLayer
from django.contrib.auth.models import AbstractUser, AnonymousUser
from django.contrib.sites.models import Site
from django.db import transaction
from django.http import HttpRequest
from utils.websockets import group_send_json

if TYPE_CHECKING:
    from support_chat.models import SupportChatParameter


# TODO: check with has_tenant_based_perm?
def can_inactivate_all_supporters(tenant: Site, user: Union[AnonymousUser, AbstractUser]) -> bool:
    from support_chat.models import SupportChatParameter

    if user.is_superuser:
        return True

    if getattr(user, "tenant", None) != tenant:
        return False

    if user.is_staff:
        return True

    if SupportChatParameter.objects.load(tenant).allow_supporter_to_inactivate_all:
        return user.has_perm("support_chat.support_users")

    return False


async def inactivate_all_supporters(channel_layer: BaseChannelLayer, tenant: Site) -> List[str]:
    from support_chat.models import Supporter
    from support_chat.websockets import SupporterStatusListener, SupportStatusListener

    @transaction.atomic
    def _inactivate_all_supporters() -> List[str]:
        supporters = list(Supporter.objects.filter_status_active(tenant, True).values_list("user__username", flat=True))
        Supporter.objects.filter(tenant=tenant).update(is_status_active=False)
        return supporters

    # Do not use `database_sync_to_async`, since this closes the connection inside the management command
    # prior to using it.
    inactivated_supporters = await sync_to_async(_inactivate_all_supporters)()

    if inactivated_supporters:
        await group_send_json(
            channel_layer,
            SupportStatusListener.group(tenant),
            SupportStatusListener.support_status_changed,
            args=(False,),
        )

        for username in inactivated_supporters:
            await group_send_json(
                channel_layer,
                SupporterStatusListener.group(tenant),
                SupporterStatusListener.supporter_status_changed,
                args=(False, username),
            )

    return inactivated_supporters


def get_support_chat_parameter(request: HttpRequest) -> "SupportChatParameter":
    return request.support_chat_parameter
