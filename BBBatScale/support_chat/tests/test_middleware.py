from django.contrib.sites.models import Site
from django.http import HttpRequest, HttpResponse
from support_chat.middleware import CurrentSupportChatParameterMiddleware
from support_chat.models import SupportChatParameter


def test_current_support_chat_parameter_middleware(testserver_tenant: Site) -> None:
    support_chat_parameter = SupportChatParameter.objects.load(testserver_tenant)
    request = HttpRequest()
    request.tenant = testserver_tenant

    assert not hasattr(request, "support_chat_parameter")

    CurrentSupportChatParameterMiddleware(lambda _: HttpResponse())(request)

    assert getattr(request, "support_chat_parameter", None) == support_chat_parameter
