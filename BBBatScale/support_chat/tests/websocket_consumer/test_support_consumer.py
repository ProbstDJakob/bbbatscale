import pytest
from channels.db import database_sync_to_async
from core.models import User
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from support_chat.models import SupportChatParameter, Supporter
from support_chat.tests.conftest import create_supporter_user
from support_chat.tests.websocket_consumer.conftest import (
    WebsocketCommunicator,
    assert_connection_refuse,
    assert_error,
    assert_illegal_request,
    chat_consumer,
    create_chat,
    fill_chat,
    support_consumer,
)
from utils.websockets import Error


async def assert_get_chats(support_communicator: WebsocketCommunicator, async_user: User, moderator: User) -> None:
    await support_communicator.send_json_to({"type": "getChats"})
    assert await support_communicator.receive_nothing(timeout=1)

    async with (
        create_chat(async_user, async_user.tenant) as user_chat,
        create_chat(moderator, moderator.tenant) as moderator_chat,
        fill_chat(user_chat, async_user, support_communicator.user, (1, 0)) as user_messages,
        fill_chat(moderator_chat, moderator, support_communicator.user, (1, 0)) as moderator_messages,
    ):
        await support_communicator.send_json_to({"type": "getChats"})

        expectations = {
            async_user.username: {
                "type": "chat",
                "chatOwner": async_user.username,
                "chatOwnerRealName": str(async_user),
                "chatOwnerIsModerator": False,
                "unreadMessages": 1,
                "isSupportActive": False,
                "message": user_messages[0].message,
                "timestamp": user_messages[0].timestamp.isoformat(),
            },
            moderator.username: {
                "type": "chat",
                "chatOwner": moderator.username,
                "chatOwnerRealName": str(moderator),
                "chatOwnerIsModerator": True,
                "unreadMessages": 1,
                "isSupportActive": False,
                "message": moderator_messages[0].message,
                "timestamp": moderator_messages[0].timestamp.isoformat(),
            },
        }

        while expectations:
            response = await support_communicator.receive_json_from()
            assert response == expectations.pop(response["chatOwner"])


@pytest.mark.asyncio
async def test_access_support_without_login(async_example_tenant: Site) -> None:
    support_communicator_anonymous_user = support_consumer(AnonymousUser(), async_example_tenant)

    await assert_connection_refuse(support_communicator_anonymous_user, Error.LOGIN_REQUIRED)


@pytest.mark.asyncio
async def test_access_support_without_permission(async_user: User) -> None:
    support_communicator_user = support_consumer(async_user, async_user.tenant)

    await assert_connection_refuse(support_communicator_user, Error.SUPPORTER_REQUIRED)


@pytest.mark.asyncio
async def test_access_different_tenant_without_permission(
    async_inactive_supporter_user: User, async_other_example_tenant: Site
) -> None:
    support_communicator_active_supporter_user = support_consumer(
        async_inactive_supporter_user, async_other_example_tenant
    )

    await assert_connection_refuse(support_communicator_active_supporter_user, Error.SUPPORTER_REQUIRED)


@pytest.mark.asyncio
async def test_access_support_with_login(async_inactive_supporter_user: User) -> None:
    async with support_consumer(async_inactive_supporter_user, async_inactive_supporter_user.tenant):
        pass


@pytest.mark.asyncio
async def test_illegal_requests_inactive_supporter_user(async_inactive_supporter_user: User) -> None:
    async with support_consumer(
        async_inactive_supporter_user, async_inactive_supporter_user.tenant
    ) as support_communicator_inactive_supporter_user:
        await assert_illegal_request(support_communicator_inactive_supporter_user)


@pytest.mark.asyncio
async def test_illegal_requests_supporter(async_active_supporter_user: User) -> None:
    async with support_consumer(
        async_active_supporter_user, async_active_supporter_user.tenant
    ) as support_communicator_supporter:
        await assert_illegal_request(support_communicator_supporter)


@pytest.mark.asyncio
async def test_support_inactive_get_chats(
    async_inactive_supporter_user: User,
    async_user: User,
    async_moderator: User,
    async_inactive_supporter_user_other_tenant: User,
) -> None:
    async with (
        support_consumer(async_inactive_supporter_user_other_tenant, async_inactive_supporter_user_other_tenant.tenant),
        support_consumer(
            async_inactive_supporter_user, async_inactive_supporter_user.tenant
        ) as support_communicator_inactive_supporter_user,
    ):
        await assert_get_chats(support_communicator_inactive_supporter_user, async_user, async_moderator)


@pytest.mark.asyncio
async def test_support_active_get_chats(
    async_active_supporter_user: User,
    async_user: User,
    async_moderator: User,
    async_active_supporter_user_other_tenant: User,
) -> None:
    async with (
        support_consumer(async_active_supporter_user_other_tenant, async_active_supporter_user_other_tenant.tenant),
        support_consumer(
            async_active_supporter_user, async_active_supporter_user.tenant
        ) as support_communicator_supporter,
    ):
        await assert_get_chats(support_communicator_supporter, async_user, async_moderator)


@pytest.mark.asyncio
async def test_support_inactive_get_status(
    async_inactive_supporter_user: User, async_inactive_supporter_user_other_tenant: User
) -> None:
    async with (
        support_consumer(async_inactive_supporter_user_other_tenant, async_inactive_supporter_user_other_tenant.tenant),
        support_consumer(
            async_inactive_supporter_user, async_inactive_supporter_user.tenant
        ) as support_communicator_inactive_supporter_user,
    ):
        await support_communicator_inactive_supporter_user.send_json_to({"type": "getStatus"})
        assert await support_communicator_inactive_supporter_user.receive_json_from() == {
            "type": "status",
            "status": "inactive",
        }


@pytest.mark.asyncio
async def test_support_active_get_status(
    async_active_supporter_user: User, async_active_supporter_user_other_tenant: User
) -> None:
    async with (
        support_consumer(async_active_supporter_user_other_tenant, async_active_supporter_user_other_tenant.tenant),
        support_consumer(
            async_active_supporter_user, async_active_supporter_user.tenant
        ) as support_communicator_supporter,
    ):
        await support_communicator_supporter.send_json_to({"type": "getStatus"})
        assert await support_communicator_supporter.receive_json_from() == {"type": "status", "status": "active"}


@pytest.mark.asyncio
async def test_set_status(
    async_user: User,
    async_inactive_supporter_user: User,
    async_example_tenant: Site,
    async_inactive_supporter_user_other_tenant: User,
) -> None:
    async def assert_set_status(is_active: bool, support_communicator: WebsocketCommunicator) -> None:
        status = "active" if is_active else "inactive"
        await support_communicator.send_json_to({"type": "setStatus", "status": status})
        assert await support_communicator.receive_json_from() == {"type": "status", "status": status}
        _supporter = await database_sync_to_async(Supporter.objects.load)(
            support_communicator.user, support_communicator.user.tenant
        )
        assert _supporter is not None
        assert _supporter.is_status_active == is_active

        await support_communicator.send_json_to({"type": "setStatus", "status": "I am neither active nor inactive!"})
        await assert_error(support_communicator, Error.MALFORMED_REQUEST)
        await database_sync_to_async(_supporter.refresh_from_db)()
        assert _supporter.is_status_active == is_active

        await support_communicator.send_json_to({"type": "setStatus", "status": 42})
        await assert_error(support_communicator, Error.MALFORMED_REQUEST)
        await database_sync_to_async(_supporter.refresh_from_db)()
        assert _supporter.is_status_active == is_active

        await support_communicator.send_json_to({"type": "setStatus"})
        await assert_error(support_communicator, Error.MALFORMED_REQUEST)
        await database_sync_to_async(_supporter.refresh_from_db)()
        assert _supporter.is_status_active == is_active

    async with (
        support_consumer(async_inactive_supporter_user_other_tenant, async_inactive_supporter_user_other_tenant.tenant),
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        support_consumer(
            async_inactive_supporter_user, async_inactive_supporter_user.tenant
        ) as support_communicator_inactive_supporter_user,
    ):
        await assert_set_status(True, support_communicator_inactive_supporter_user)
        assert await chat_communicator_user.receive_json_from() == {"type": "supportOnline"}

        async with (
            create_supporter_user(True, async_example_tenant) as active_supporter_user,
            support_consumer(
                active_supporter_user, active_supporter_user.tenant
            ) as support_communicator_active_supporter_user,
        ):
            await assert_set_status(False, support_communicator_active_supporter_user)
            await assert_set_status(True, support_communicator_active_supporter_user)

        assert await chat_communicator_user.receive_nothing(timeout=1)

        await assert_set_status(False, support_communicator_inactive_supporter_user)
        assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}

        await assert_set_status(True, support_communicator_inactive_supporter_user)
        assert await chat_communicator_user.receive_json_from() == {"type": "supportOnline"}


@pytest.mark.asyncio
async def test_inactivate_all_supporters(
    async_user: User, async_staff_user: User, async_example_tenant: Site, async_active_supporter_user_other_tenant: User
) -> None:
    async def supporter_count(tenant: Site) -> int:
        return await database_sync_to_async(Supporter.objects.filter_status_active(tenant, True).count)()

    async with (
        support_consumer(async_active_supporter_user_other_tenant, async_active_supporter_user_other_tenant.tenant),
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        support_consumer(async_staff_user, async_staff_user.tenant) as support_communicator_staff_user,
    ):
        async with (
            create_supporter_user(True, async_example_tenant) as active_supporter_user,
            support_consumer(
                active_supporter_user, active_supporter_user.tenant
            ) as support_communicator_active_supporter_user,
        ):
            assert await supporter_count(async_user.tenant) == 1

            await support_communicator_staff_user.send_json_to({"type": "inactivateAllSupporters"})
            assert await support_communicator_active_supporter_user.receive_json_from() == {
                "type": "status",
                "status": "inactive",
            }
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}

            assert await supporter_count(async_user.tenant) == 0

        async with (
            create_supporter_user(True, async_example_tenant) as active_supporter_user,
            support_consumer(
                active_supporter_user, active_supporter_user.tenant
            ) as support_communicator_active_supporter_user,
        ):
            await support_communicator_staff_user.send_json_to({"type": "setStatus", "status": "active"})
            assert await support_communicator_staff_user.receive_json_from() == {
                "type": "status",
                "status": "active",
            }

            assert await supporter_count(async_user.tenant) == 2

            await support_communicator_staff_user.send_json_to({"type": "inactivateAllSupporters"})
            assert await support_communicator_staff_user.receive_json_from() == {
                "type": "status",
                "status": "inactive",
            }
            assert await support_communicator_active_supporter_user.receive_json_from() == {
                "type": "status",
                "status": "inactive",
            }
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}

            assert await supporter_count(async_user.tenant) == 0

        async with (
            create_supporter_user(True, async_example_tenant) as active_supporter_user,
            support_consumer(
                active_supporter_user, active_supporter_user.tenant
            ) as support_communicator_active_supporter_user,
        ):
            assert await supporter_count(async_user.tenant) == 1

            assert (
                await database_sync_to_async(SupportChatParameter.objects.load)(async_example_tenant)
            ).allow_supporter_to_inactivate_all is False
            await support_communicator_active_supporter_user.send_json_to({"type": "inactivateAllSupporters"})
            await assert_error(support_communicator_active_supporter_user, Error.PERMISSION_DENIED)

            assert await supporter_count(async_user.tenant) == 1

            await database_sync_to_async(SupportChatParameter.objects.filter(tenant=async_example_tenant).update)(
                allow_supporter_to_inactivate_all=True
            )
            await support_communicator_active_supporter_user.send_json_to({"type": "inactivateAllSupporters"})
            assert await support_communicator_active_supporter_user.receive_json_from() == {
                "type": "status",
                "status": "inactive",
            }

            assert await supporter_count(async_user.tenant) == 0
