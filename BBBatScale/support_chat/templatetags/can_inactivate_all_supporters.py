from core.models import User
from core.utils import get_tenant
from django import template
from django.template import RequestContext
from support_chat.utils import can_inactivate_all_supporters

register = template.Library()


@register.simple_tag(name="can_inactivate_all_supporters", takes_context=True)
def do_can_inactivate_all_supporters(context: RequestContext, user: User) -> bool:
    return can_inactivate_all_supporters(get_tenant(context.request), user)
