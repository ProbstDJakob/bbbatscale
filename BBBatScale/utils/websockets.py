from __future__ import annotations

import json
from asyncio import iscoroutinefunction
from enum import Enum, unique
from functools import wraps
from json import JSONEncoder
from types import FunctionType, MethodType
from typing import Any, Callable, Coroutine, Dict, Optional, Tuple, TypeVar, Union, cast

from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.layers import BaseChannelLayer
from core.models import User
from django.contrib.sites.models import Site

T = TypeVar("T")


@unique
class Error(Enum):
    def __init__(self, code: int, message: str) -> None:
        self._value_ = code
        self._message_ = message

    @property
    def code(self) -> int:
        return self.value

    @property
    def message(self) -> str:
        return self._message_

    def to_dict(self) -> Dict[str, Any]:
        return dict(type="error", code=self.code, message=self.message)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}.{self.name}"

    def __str__(self) -> str:
        return f"{self.__class__.__name__}.{self.name}: {self.message}"

    def __eq__(self, other) -> bool:
        if isinstance(other, int):
            return self.code == other
        return super().__eq__(other)

    @classmethod
    def _missing_(cls, value) -> Optional[Error]:
        for error in cls.__members__.values():
            if error == value:
                return error
        return None

    INTERNAL_ERROR = (1, "An unexpected internal error occurred.")
    LOGIN_REQUIRED = (2, "Login required.")
    STAFF_REQUIRED = (3, "You have to be staff to access this content.")
    SUPPORTER_STATUS_INACTIVE = (4, "Your support status is inactive. Set it to active to access this feature.")
    CHAT_NOT_FOUND = (5, "The chat could not be found.")
    MESSAGE_TOO_LONG = (6, "The message is too long.")
    MALFORMED_REQUEST = (7, "The request is malformed.")
    UNKNOWN_REQUEST = (8, "The request is unknown.")
    PERMISSION_DENIED = (9, "The request is not allowed in this context.")
    SUPPORTER_NOT_JOINED_CHAT = (10, "You have not joined the chat yet.")
    ANONYMOUS_REQUIRED = (11, "Anonymous required. You should not be logged in.")
    SUPPORTER_REQUIRED = (12, "You have to be supporter to access this content.")


class ExtendedJsonEncoder(JSONEncoder):
    def default(self, obj: Any) -> Any:
        if isinstance(obj, Error):
            return obj.to_dict()

        return super().default(obj)


class WebsocketConsumerError(RuntimeError):
    pass


class WebsocketConsumerNotOpenError(WebsocketConsumerError):
    def __init__(self) -> None:
        super().__init__("The WebSocket is not open")


class WebsocketConsumerAlreadyAcceptedError(WebsocketConsumerError):
    def __init__(self) -> None:
        super().__init__("The WebSocket has already been accepted")


class WebsocketConsumerNotAcceptedError(WebsocketConsumerError):
    def __init__(self) -> None:
        super().__init__("The WebSocket has not been accepted yet")


class WebsocketConsumerAlreadyClosedError(WebsocketConsumerError):
    def __init__(self) -> None:
        super().__init__("The WebSocket has already been closed")


class WebsocketConsumerError(WebsocketConsumerError):
    def __init__(self, error: Error) -> None:
        super().__init__(error.message)
        self.error = error


def websocket_consumer_decorator(init_func: Callable[[T, dict, ...], None]) -> Callable[[T, dict], None]:
    @wraps(init_func)
    def wrapper(self, scope: dict) -> None:
        args = scope["url_route"]["args"]
        kwargs = scope["url_route"]["kwargs"]

        init_func(self, scope, *args, **kwargs)

    return wrapper


class WebsocketConsumer(AsyncJsonWebsocketConsumer):
    def __init__(self, scope: dict, *_: Any, **__: Any) -> None:
        super().__init__(scope=scope)
        self.tenant = cast(Site, scope["tenant"])
        self.user = cast(User, scope["user"])
        self._accepted = False
        self._closed = False

    async def __call__(self, *args, **kwargs):
        await self.resolve_scope()
        await super().__call__(*args, **kwargs)

    async def resolve_scope(self):
        pass

    async def join_group(self, group):
        self.groups.append(group)
        await self.channel_layer.group_add(group, self.channel_name)

    async def group_send_json(
        self, group: str, handler: Union[Callable[[Any], Coroutine[Any, Any, None]], str], *args: Any, **kwargs: Any
    ) -> None:
        await group_send_json(self.channel_layer, group, handler, args=args, kwargs=kwargs)

    async def send_json_from_group(self, event: dict) -> None:
        await getattr(self, event["handler"])(*event["args"], **event["kwargs"])

    async def accept(self, subprotocol=None) -> None:
        if self._accepted:
            raise WebsocketConsumerAlreadyAcceptedError
        self._accepted = True
        await super().accept(subprotocol)

    @property
    def accepted(self) -> bool:
        return self._accepted

    async def close(self, code=None) -> None:
        if not self._accepted:
            raise WebsocketConsumerNotAcceptedError
        if self._closed:
            raise WebsocketConsumerAlreadyClosedError
        self._closed = True
        await super().close(code)

    @property
    def closed(self) -> bool:
        return self._closed

    @property
    def opened(self) -> bool:
        return self.accepted and not self.closed

    @classmethod
    async def encode_json(cls, content) -> Any:
        return json.dumps(content, cls=ExtendedJsonEncoder)

    @classmethod
    def if_opened(
        cls, method: MethodType = None, raise_exception: bool = False
    ) -> Union[Callable[[MethodType], Callable[..., Any]], Callable[..., Any]]:
        def decorator(_method: MethodType) -> Callable[..., Any]:
            @wraps(_method)
            async def wrapper(self: WebsocketConsumer, *args: Any, **kwargs: Any) -> Any:
                if self.opened:
                    return await _method(self, *args, **kwargs)
                if raise_exception:
                    raise WebsocketConsumerNotOpenError

            return wrapper

        if method:
            return decorator(method)
        return decorator

    @classmethod
    def passes_test(
        cls,
        test: Callable[[WebsocketConsumer], Coroutine[Any, Any, Optional[Error]]],
        accept: bool = False,
        close: bool = False,
        raise_exception: bool = False,
    ) -> Callable[[MethodType], Callable[..., Any]]:
        def decorator(method: MethodType) -> Callable[..., Any]:
            @wraps(method)
            async def wrapper(self: WebsocketConsumer, *args: Any, **kwargs: Any) -> Any:
                if accept and not self.accepted:
                    await self.accept()
                error = await test(self)

                if not error:
                    return await method(self, *args, **kwargs)
                else:
                    await self.send_json(error)
                    if close:
                        await self.close(1000)
                    if raise_exception:
                        raise WebsocketConsumerError(error)

            return wrapper

        return decorator

    @classmethod
    def login_required(
        cls, method: MethodType = None, accept: bool = False, close: bool = False, raise_exception: bool = False
    ) -> Union[Callable[[MethodType], Callable[..., Any]], Callable[..., Any]]:
        async def test(self: WebsocketConsumer) -> Optional[Error]:
            is_authenticated = self.user.is_active and self.user.is_authenticated
            return None if is_authenticated else Error.LOGIN_REQUIRED

        decorator = cls.passes_test(test, accept=accept, close=close, raise_exception=raise_exception)
        if method:
            return decorator(method)
        return decorator


async def group_send_json(
    channel_layer: BaseChannelLayer,
    group: str,
    handler: Union[Callable[[Any], Coroutine[Any, Any, None]], str],
    *,
    args: Optional[Tuple[Any, ...]] = None,
    kwargs: Optional[Dict[str, Any]] = None,
) -> None:
    if (isinstance(handler, MethodType) or isinstance(handler, FunctionType)) and iscoroutinefunction(handler):
        handler = handler.__name__
    elif not isinstance(handler, str):
        raise ValueError("'handler' is neither a string nor a async method/function")

    if args is None:
        args = tuple()

    if kwargs is None:
        kwargs = dict()

    await channel_layer.group_send(
        group, {"type": "send_json_from_group", "handler": handler, "args": args, "kwargs": kwargs}
    )
