# Migration Path from 2.X to 3.0

In the 3.0 release no major database change occurs.
This version introduces multi-tenancy support with django's sites framework and an oidc authentication provider for
tenant based authentication. It is tested with keycloak. For further instructions on oidc please read the 
[openIDconnect.md](openIdConnect.md).

To migrate from a running BBB@Scale v2.X version to v3.0.0, you have to upgrade your project, run migrations and create/overwrite your tenant.

### Updating your project
For updating please read [GettingStarted.md update section](../GettingStarted.md#update).

### Migrations
In case you deploy via Helm, migrations happen to run automatically.
For a local docker deployment, you have to run migrations manually. Please customize following command for your deployment:
```
docker-compose -f /bbbatscale/docker-compose.yml exec bbbatscale bbbatscale migrate
```

### Create/Overwrite Tenant
Since we use django's sites framework to identify our tenants, you'll have to create and tenant based on your domain.
```
# For new tenant via cli
docker-compose -f /bbbatscale/docker-compose.yml exec bbbatscale bbbatscale tenant create <domain>

# If you already have data that will be associated with the default tenant (example.org)
# use this command to change the tenants domain to your desired domain
docker-compose -f /bbbatscale/docker-compose.yml exec bbbatscale bbbatscale tenant rename example.org <domain>
```

### Authentication
If you used the old oidc, ldap or shibboleth authentication, please have a look at [openIDconnect.md](openIdConnect.md),
since version 3 does not support them anymore. We switched to a central IAM to provide multi-tenancy authentication.


After you migrated your database you are finished.
Happy working and carry on!
